const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

elixir(function(mix) {
    mix.scripts(
        [
            //'./resources/assets/js/jquery-1.9.1.min.js',
            './resources/assets/js/sticky-kit.min.js',
            './resources/assets/js/skripte.js',
            './resources/assets/js/datetimepicker.full.js',
            './resources/assets/js/scripts-call.js',
            './node_modules/jquery.payment/lib'
        ],
        './public/js/all.js'
    );

    mix.webpack('vue/app.js');
});
