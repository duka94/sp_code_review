

//$(".sidebar-inner").stick_in_parent();
$("#sidebar-nav").stick_in_parent();

$(document).ready(function(){

});


$(window).scroll(function() {

    var visina = $('.header-100').innerHeight();


    if ($(this).scrollTop() > 350){
        $('.header-100').addClass("fixed");
        $('body').css({ 'padding-top': visina + 'px' });
    }
    else{
        $('.header-100').removeClass("fixed");
        $('body').css({ 'padding-top': 0 + 'px' });
    }


});




//    if (window.innerWidth > 768) {
//
//            $(window).scroll(function() {
//
//                var visinafilter = $('.left-filters').innerHeight();
//
//             if ($(this).scrollTop() > 350){
//                 $('.left-filters').addClass("fixed");
//                 $('body').css({ 'padding-top': visina + visinafilter + 'px' });
//               }
//             else {
//                 $('.left-filters').removeClass("fixed");
//                 $('body').css({ 'padding-top': 0 + 'px' });
//               }
//     });
// }



//<!--SMOOTH SCROLL-->

$(function() {
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});

//<!--/SMOOTH SCROLL-->




//<!-- SCROLL TO TOP -->

$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});




if($(window).width() < 992){
    $('.heading h1').appendTo('.header .mobile-header');
    $('.heading .hboxes').appendTo('.header .mobile-header');
    $('.heading').css({
        "max-height":"0",
        "overflow":"hidden",
        "padding":"0"
    });
}




//<!-- CUSTOM SELECT BUTTON -->

// $('select').each(function(){
//     var $this = $(this), numberOfOptions = $(this).children('option').length;

//     $this.addClass('select-hidden');
//     $this.wrap('<div class="select"></div>');
//     $this.after('<div class="select-styled"></div>');

//     var $styledSelect = $this.next('div.select-styled');
//     //$styledSelect.text($this.children('option').eq(0).text());
//     $styledSelect.text($this.children('option:selected').eq(0).text());

//     var $list = $('<ul />', {
//         'class': 'select-options'
//     }).insertAfter($styledSelect);

//     for (var i = 0; i < numberOfOptions; i++) {
//         $('<li />', {
//             text: $this.children('option').eq(i).text(),
//             rel: $this.children('option').eq(i).val()
//         }).appendTo($list);
//     }

//     var $listItems = $list.children('li');

//     $styledSelect.click(function(e) {
//         e.stopPropagation();
//         $('div.select-styled.active').not(this).each(function(){
//             $(this).removeClass('active').next('ul.select-options').hide();
//         });
//         $(this).toggleClass('active').next('ul.select-options').toggle();
//     });

//     $listItems.click(function(e) {
//         //companyList.updateAdditionalServices()
//         e.stopPropagation();
//         $styledSelect.text($(this).text()).removeClass('active');
//         $this.val($(this).attr('rel'));
//         $list.hide();
//         //console.log($this.val());
//     });

//     $(document).click(function() {
//         $styledSelect.removeClass('active');
//         $list.hide();
//     });

// });

//<!-- /CUSTOM SELECT BUTTON -->


jQuery('#datetimepicker').datetimepicker({
    timepicker:false,
    format:'d/m/Y',
    minDate: true
});



//<!-- LEFT SIDEBAR SCROLL NAV + INDICATOR -->

// Cache selectors
var lastId,
    topMenu = $("#sidebar-nav"),
    topMenuHeight = topMenu.outerHeight()+1,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
    });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    $('html, body').stop().animate({
        scrollTop: offsetTop
    }, 850);
    e.preventDefault();
});

// Bind to scroll
$(window).scroll(function(){
    // Get container scroll position
    var fromTop = $(this).scrollTop()+topMenuHeight;

    // Get id of current scroll item
    var cur = scrollItems.map(function(){
        if ($(this).offset().top < fromTop)
            return this;
    });
    // Get the id of the current element
    cur = cur[cur.length-1];
    var id = cur && cur.length ? cur[0].id : "";

    if (lastId !== id) {
        lastId = id;
        // Set/remove active class
        menuItems
            .parent().removeClass("active")
            .end().filter("[href=#"+id+"]").parent().addClass("active");
    }
});


//<!-- LEFT SIDEBAR SCROLL NAV + INDICATOR -->





//<!-- FAQ TABS -->

$(document).ready(function() {

    (function ($) {
        $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');

        $('.tab ul.tabs li a').click(function (g) {
            var tab = $(this).closest('.tab'),
                index = $(this).closest('li').index();

            tab.find('ul.tabs > li').removeClass('current');
            $(this).closest('li').addClass('current');

            tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
            tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();

            g.preventDefault();
        } );
    })(jQuery);
});

//<!-- /FAQ TABS -->

//<!-- TAB BUTTONS MIN HEIGHT _ DUE TO THEIR ABSOLUTE POSITION -->

//resize
function resizeAll() {

    var tabsheight = $('.faq-container .tab ul.tabs').innerHeight();

    $('.faq-container .tab').css({
        'min-height': tabsheight + 30 + 'px'
    });

}

$(window).resize(function() {
    resizeAll();
});
resizeAll();



//<!-- EXPANDABLE DIVS ON CLICK -->


$(document).on('click', '.expand-btn', function() {
    $( ".expand-container.active" ).removeClass( "active" );
    $(this).toggleClass('active');
    $('.expand-btn.active + .expand-container').toggleClass('active');
});

//<!-- EXPANDABLE DIVS ON CLICK -->

//<!-- INPUT COUNTER -->

// $(document).ready(function() {
//     $('.minus').click(function () {
//         var $input = $(this).parent().parent().find('input');
//         var count = parseInt($input.val()) - 1;
//         count = count < 0 ? 0 : count;
//         $input.val(count);
//         $input.change();
//         companyList.clickMinus($input.val(), $input.data('id'));
//         return false;
//     });
//     $('.plus').click(function () {
//         var $input = $(this).parent().parent().find('input');
//         $input.val(parseInt($input.val()) + 1);
//         $input.change();
//         companyList.clickPlus($input.val(), $input.data('id'));
//         return false;
//     });
// });

//<!-- INPUT COUNTER -->


//COMPARE COMPANIES SIDEBAR MOBILE CANCEL/APPLY FILTERS
$(document).on('click', '.mobile-cogwheel-btn', function() {
    $( ".sidebar" ).toggleClass( "active" );
    $(this).toggleClass('active');
});

$(document).on('click', '.cancel-apply-filters .btn.cancel', function() {
    $( ".sidebar" ).toggleClass( "active" );
    $(this).toggleClass('active');
});
//COMPARE COMPANIES SIDEBAR MOBILE CANCEL/APPLY FILTERS

