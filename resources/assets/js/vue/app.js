/**
 * This was added later, bacause someone removed all default content vrom assets/js folder
 * that is why it is in subfolder vue
 */


/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('datepicker', require('vuejs-datepicker'));
Vue.component('popper', require('vue-popperjs'));
Vue.component('search', require('./components/Search.vue'));
//Vue.component('modal', require('./components/Modal.vue'));
//Vue.component('modal-box', require('./components/Modal_box.vue'));
Vue.component('pagination', require('./components/Pagination.vue'));

const app = new Vue({
    el: '#app'
});

