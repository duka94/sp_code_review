@extends('layouts.admin.admin')

@section( 'content' )

    <style type="text/css">
        td {
            border: 1px solid #aaaaaa;
            padding: 5px;
        }
    </style>

    <section class="content-header">
        <h1>
            Company dashboard
            <small>Company summary</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="app" v-cloak>

            <div class="row">
                <?php if($data['is_approved']): ?>
                <div class="col-xs-12 form-inline">
                    <vf-form
                            action="/admin"
                            method="POST"
                            v-ref:statistics
                    >
                        {{ csrf_field() }}

                        <vf-date
                                name="services_date"
                                v-ref:services_date
                                :rules="{services_date:true}"
                                :options="{ locale:{firstDay:1}}"
                                class="pull-left"
                                required
                        ></vf-date>

                        <vf-submit text="Show" class="pull-left"></vf-submit>

                    </vf-form>
                </div>

                <div class="row"><div class="col-xs-12">&nbsp;</div></div>
                <div class="row"><div class="col-xs-12">&nbsp;</div></div>

                <div class="col-xs-12">
                    <h4>Booking summary for @{{ stats.date }}</h4>

                    <table>
                        <tr>
                            <td>Total new bookings today: </td>
                            <td><a href="/admin/bookings">@{{ stats.jobs_today  }}</a></td>
                        </tr>
                        
                        <tr>
                            <td>Jobs for today: </td>
                            <td>@{{ stats.today_cnt  }}</td>
                        </tr>
                        <tr>
                            <td>Total booking revenue today: </td>
                            <td>£@{{ stats.total_revenue }}</td>
                        </tr>
                        <!--<tr v-for="count in stats.grouped">
                            <td>@{{ $key }}: </td>
                            <td>@{{ count }}</td>
                        </tr>-->
                        
                        
                    </table>


                    <h4>Subscription summary</h4>
                    <p>You are on the <b>@{{ plan.name }}</b> package</p>
                    <p><b>Monthly bookings limit:</b> @{{ plan.jobs }}</p>
                    <p><b>Bookings remaining:</b> @{{ plan.jobs - stats.month_cnt }}</p>
                </div>

                <?php else: ?>
                <div class="alert alert-warning">
                    Your subscription is pending approval. If you would like to discuss this further please call 0000 000 0000
                </div>
                <?php endif; ?>
            </div>



        </div>

        <!-- Your Page Content Here -->

    </section>
    <!-- /.content -->

@endsection

@section('js')
    <script>
        var app = new Vue({
            el: '#app',

            data: function () {
                return {
                    today: moment(),
                    stats: <?= json_encode($data) ?>,
                    company: <?=json_encode($company) ?>,
                    plan: <?= json_encode($plan) ?>
                }
            },

        })
    </script>

@endsection