@extends('layouts.admin.admin')

@section('content')

    <section class="content-header">
        <h1>
            Your bookings
            <small>View your bookings here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Orders</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div id="app">

            <div class="row">
                <div class="col-xs-12">

                    <div class="table">
                        <v-client-table :data="{{json_encode($orders)}}" :columns="headers" :options="options"></v-client-table>
                    </div>

                    <div class="spinner" v-if="loading">
                        <!--<div class="spinner" v-if="isSending">-->
                        <loader :loading="loading" :color="color" :size="loaderSize" class="text-center"></loader>
                    </div>
                </div>

            </div>
        </div>

    </section>

@endsection
@section('js')
    <script>

        var app = new Vue({

            el: '#app',
            data: {
                headers: ['from', 'booking_date', 'service_date',  '_viewmore'],
                options: {
                    //dateColumns: ['service_date'],
                    headings: {
                        _viewmore: 'View more',
                        from: 'Client name',
                        booking_date: 'Booking date'
                    },
                    filterable: ['from','email','service_date'],
                    orderBy: { 'column': 'booking_date', 'descending': false},
                    templates: {
                        _viewmore: function (row) {
                            return '<a class="btn btn-primary" href="/admin/bookings/' + row.id + '">View more</a>';
                        },
                        booking_date: function (row) {
                            return moment(String(row.booking_date)).format('D.M.YY');
                        },
                    },
                    perPage: 100
                }
            },
            computed: {
                _orders: function () {
                    console.log(this._orders);
                    if( _.isString(this.orders) ) {
                        try {
                            return JSON.parse(this.orders)
                        }
                        catch(e) {
                            return this.orders
                        }
                    }

                    return this.orders;
                }
            }

        });

    </script>
@endsection