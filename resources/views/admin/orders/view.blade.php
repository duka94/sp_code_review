@extends('layouts.admin.admin')

@section('content')

<section class="content-header">
    <h1>
        View booking
        <small>Detailed view for your booking</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Orders</a></li>
        <li class="active">View order</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <div id="app">
        <div class="row">
            <div class="col-xs-12">

             <span class="text-bold">{{ $order['category_name']  }}</span><br/>
             <!-- <span class="text-bold">Service date:</span>--> {{ \Carbon\Carbon::parse($order['service_date'])->format('d.m.Y') }}
             <br/>
             <!--<span class="text-bold">Timeslot:</span>--> <?php
             $exp = explode( '|', $order['timeslots'] );
             echo date( 'H:i', strtotime( $exp[0] ) ) . ' - ' . date( 'H:i', strtotime( $exp[1] ) );?>

                @if( !empty($order['order_total']) )

                       <?php $totalAmt = number_format($order['order_total'],2); ?>

                @else

                       <?php $totalAmt = number_format($order['company']['price'],2); ?>

                @endif


             <br/>
             @foreach($order['company']['services'] as $service)

                  @if ( $service['name'] == 'Bathroom')

                             @if ( $service['count'] > 1 )

                               <?php $cnt = $service['count'] - 1;?>

                             <p> <b>{{ $service['category'] }} </b> - {{ $cnt.' '.$service['name'] }}</p>

                             @endif

                    @else

                        <p> <b>{{ $service['category'] }} </b> - {{ $service['count'].' '.$service['name'] }}</p>

                   @endif

             @endforeach
             <?php 
             $order['order_price'] = $totalAmt;
             $order['site_charge'] = $order['site_charge'] / 100;
             $order['revenue'] = $totalAmt - $order['site_charge']; 
             ?>

             Total Booking: &pound;{{$order['order_price']}} <br>

             Commission: &pound;{{$order['site_charge']}} <br>

             Remaining balance: &pound;{{  $order['revenue'] }} <br>


                 @if(isset($order['coupon']))

                    Coupon Used: {{$order['coupon']}}

                 @endif

             <br/><br>

             <hr/>

             <h4>Customer info:</h4>
             {{ $order['client']['firstname'] or '' }} {{ $order['client']['lastname'] or '' }}

             <br/>
             {{ $order['client']['email'] or '' }}<br/>
             {{ $order['client']['clientphone'] or '' }}<br/>
             {{ $order['client']['address'] or '' }}<br/>
             @if(!empty($order['client']['address_2'])) {{$order['client']['address_2']}} <br/> @endif
             @if(!empty($order['client']['flat'])) {{$order['client']['flat']}} <br/> @endif
             {{ $order['client']['town'] or '' }}<br/>
             <!--<span class="text-bold">County:</span> {{ $order['client']['county'] or '' }}<br/>-->
             {{ $order['postcode'] or '' }}<br/>
             @if(isset($order['client']['instructions']) && $order['client']['instructions'] != '')Instructions for access: {{ $order['client']['instructions'] }}<br/> @endif

             <hr/>

           
                    
                </div>
            </div>
        <!--<div class="row">
             <hr />
            <vf-form ajax action="{{ $url }}" 
                     method="POST"
                     >
                <vf-submit text="Resend booking email" class="pull-left"></vf-submit>
            </vf-form>
         </div>-->
            <div class="spinner" v-if="loading">
                <loader :loading="loading" :color="color" :size="loaderSize" class="text-center"></loader>
            </div>
        </div>


    </section>

@endsection

@section('js')
    <script>

        var app = new Vue({

            el: '#app',
            data: {
                loading: false,
                formurl: 'testaddr',
                order: <?= json_encode( $order ) ?>,
            },
            mixins: [
                events
            ],
            events: {
                'vue-formular.sending': function () {
                    this.loading = true;
                },
                'vue-formular.sent': function () {
                    //TODO: Success message
                    this.loading = false;
                },
                'vue-formular.invalid.server': function (response) {
                    console.log(response);
                    this.$emit('sent.error', response)
                },
            }


        });

    </script>
@endsection
