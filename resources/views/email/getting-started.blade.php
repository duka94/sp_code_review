Hello from <?= env('SITE_NAME');?>,<br/>
<br/>
Congratulations! You've successfully signed up for <?= env('SITE_NAME');?>!<br/>
<br/>Your subscriber ID is: <?=$company->name;?><br/>
<b>Your password is: <?=trim($password);?></b><br/>
<br/>
You are on the <?=$plan->name;?> and you can receive up to <?=$plan->jobs_limit;?> bookings per month.<br/>
<br/>
Now it's time to activate your account so that customers can start comparing your quotes online:<br/>
<br/>
1. Sign in to your account at <a href="<?=env('APP_URL');?>login"><?=env('APP_URL');?>login</a><br/>
<br>
2. Follow the on-screen instructions. Setting up services just takes a few minutes and involves four simple steps:<br/>
- Selecting the services you offer<br/>
- Entering prices for each service and extras<br/>
- Entering the postcode areas you cover<br/>
- Entering other required information<br/>
<br/>
Once you complete these steps, your account will be sent for approval. If you need any help setting up your account, just call us on {{ env('SITE_PHONE') }}.
<br/>
<br/>
Ready to get started? Check out our <a href="https://www.serviceoctopus.com/subscriber-help/">resources for new
    subscribers.</a>
<br/>
<br/>
We're delighted to welcome you to <?= env('SITE_NAME');?>.<br/>
<br/>
<br/>
<br/>
See you online,<br/>
The <?= env('SITE_NAME');?> Team
