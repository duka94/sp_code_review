<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
        table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        img { -ms-interpolation-mode: bicubic; }

        /* RESET STYLES */
        img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
        table { border-collapse: collapse !important; }
        body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MEDIA QUERIES */
        @media screen and (max-width: 480px) {
            .mobile-hide {
                display: none !important;
            }
            .mobile-center {
                text-align: center !important;
            }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] { margin: 0 !important; }
    </style>
</head>
<body style="margin: 0 !important; padding: 0 !important; background-color: #eeeeee;" bgcolor="#eeeeee">


<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
           <!-- <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">-->
		<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;border: 1px solid #eeeeee">
                <tr>
                    <td align="center" valign="top" style="font-size:0; padding: 25px;padding-bottom:10px;" bgcolor="#ffffff">
                        <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                            <tr>
                                <td align="left" valign="top" width="300">
                        <![endif]-->
                        <div style="display:inline-block; max-width:50%; min-width:100px; vertical-align:top; width:100%;">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:300px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 36px; font-weight: 800; line-height: 48px;" class="mobile-center">
                                        <a href="https://www.serviceoctopus.com"><img src="<?=env('APP_URL');?>/images/email-logo.png" alt="<?= env('SITE_NAME');?> Logo" style="width:161px;height:70px;"></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        <td align="right" width="300">
                        <![endif]-->
                        <div style="display:inline-block; max-width:50%; min-width:100px; vertical-align:top; width:100%;" class="mobile-hide">
                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:300px;">
                                <tr>
                                    <td align="right" valign="top" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; line-height: 48px;">
                                        <table cellspacing="0" cellpadding="0" border="0" align="right">
                                            <tr>
                                                <td style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400;">

                                                </td>
                                                <td style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 24px;">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding: 10px 25px 20px 25px; background-color: #ffffff;" bgcolor="#ffffff">
                        <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                            <tr>
                                <td align="center" valign="top" width="600">
                        <![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                            <tr>
                                <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 0px;">
                                    <img src="<?=env('APP_URL');?>/img/communications/tick.png" width="125" height="120" style="display: block; border: 0px;" /><br>
                                    <h2 style="font-size: 26px; font-weight: 800; line-height: 36px; color: #333333; margin: 0;">

                                        Thanks for your booking, <?=$order_data->firstname.'!';?>

                                    </h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;">
                                    <p style="font-size: 14px; font-weight: 400; line-height: 24px; color: #777777;">
                                        <b><?=$order_data->company->name;?> <!--$SubscriberCompanyName$--></b> has received your booking and they’ll be arriving between <b><?=$timeslot_from;?>-<?=$timeslot_to;?></b><!--$ArrivalTimeSlot$--> on <b><?=$date;?><!--$BookingDate$-->.</b>
                                    </p>

                                    <p style="font-size: 14px; font-weight: 400; line-height: 24px; color: #777777;">


                                        If you have any questions about your booking or want to make a change, just give <?=$order_data->company->name;?> <!--$SubscriberCompanyName$--> a quick call on <?=$companyData['phone'];?><!--$SubscriberCompanyMainTelephone$--> or email <?=$company['email'];?><!--$SubscriberEmailAddress$--></b>.

                                    </p>
                             

<hr style="border-top: 1px solid #eeeeee;">

                                    <p style="font-size: 14px; font-weight: 400; line-height: 24px; color: #008000;"> Need anything else?  Visit our <a href="https://www.serviceoctopus.com/help/">FAQs page</a>.</p>

                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding-top: 20px;">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td width="75%" align="left" bgcolor="#eeeeee" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 800; line-height: 24px; padding: 10px;">
                                                Booking Confirmation
                                            </td>
                                            <td width="25%" align="left" bgcolor="#eeeeee" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 800; line-height: 24px; padding: 10px;">
                                                #<?=$order->id?><!--$BookingConfNumber$-->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="75%" align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 15px 10px 5px 10px;">
                                                <?=$category_name;?> <!--$ServiceName$-->
                                            </td>
                                            <td width="25%" align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 15px; font-weight: 400; line-height: 24px; padding: 15px 10px 5px 10px;">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="75%" align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 24px; padding: 5px 10px;">
                                                @foreach($order_data->company->services as $service)
                                                    @if($loop->first && $order_data->cleaning_type != 'Carpet Cleaning')
                                                        {{$order_data->propertyType}}, {{$service->count}} {{$service->name}},
                                                    @elseif($loop->first && $order_data->cleaning_type == 'Carpet Cleaning')
                                                        {{$service->count}} {{$service->name}},
                                                        @else
                                                        {{$service->count}} {{$service->name}}
                                                        @endif
                                                        @endforeach
                                                                <!--$ServiceDetailsAndExtras$-->
                                            </td>
                                            <td width="25%" align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 15px; font-weight: 400; line-height: 24px; padding: 5px 10px;">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="75%" align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 5px 10px;">
                                                Booking fee
                                            </td>
                                            <td width="25%" align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 5px 10px;">
                                                £{{number_format($booking_fee, 2)}}    <!-- $BookingFee$-->
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="75%" align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 5px 10px;">
                                                Remaining balance
                                            </td>
                                            <td width="25%" align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 5px 10px;">
                                                £{{ number_format($total_sum-$booking_fee, 2)}}<!-- $RemainingBalance$-->
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding-top: 20px;">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td width="75%" align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 800; line-height: 24px; padding: 10px; border-top: 3px solid #eeeeee; border-bottom: 3px solid #eeeeee;">
                                                TOTAL
                                            </td>
                                            <td width="25%" align="left" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 800; line-height: 24px; padding: 10px; border-top: 3px solid #eeeeee; border-bottom: 3px solid #eeeeee;">
                                                £{{ number_format($total_sum, 2)}}<!--$Total$-->
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
                <tr>
                    <td align="center" height="100%" valign="top" width="100%" style="padding: 0 35px 35px 35px; background-color: #ffffff;" bgcolor="#ffffff">
                        <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                            <tr>
                                <td align="center" valign="top" width="600">
                        <![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:660px;">
                            <tr>
                                <td align="center" valign="top" style="font-size:0;">
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                                        <tr>
                                            <td align="left" valign="top" width="300">
                                    <![endif]-->
                                    <div style="display:inline-block; max-width:50%; min-width:240px; vertical-align:top; width:100%;">

                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:300px;">
                                            <tr>
                                                <td align="left" valign="top" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                    <p style="font-weight: 800;">Address to be serviced:</p>

                                                    <?php if (!empty($order_data->client->flat) ) {

                                                        $flatNo = $order_data->client->flat;

                                                    }

                                                    else {

                                                        $flatNo ='';
                                                    }

                                                    ?>

                                                    <?= !empty($order_data->client->address)?$flatNo.' '.$order_data->client->address.'<br>':'' ?>

                                                            <!--$Address1$--><?=!empty($order_data->client->address_2)?$order_data->client->address_2.'<br>':'';?>
                                                    <?=!empty($order_data->client->town)? $order_data->client->town.'<br>':"";?>
                                                            <!--$County$--><?=!empty($order_data->postcode)?$order_data->postcode.'<br>':"";?><!--$PostCode-->
                                                            <!--$County$--><?=!empty($order_data->city)?$order_data->city:"";?><!--$PostCode--></p>

                                                    <?php if ( !empty($order_data->client->instructions)) {?>

                                                    <p>  Instructions for access: {{$order_data->client->instructions}} </p>

                                                    <?php } ?>


                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    <td align="left" valign="top" width="300">
                                    <![endif]-->
                                    <div style="display:inline-block; max-width:50%; min-width:240px; vertical-align:top; width:100%;">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:300px;">
                                            <tr>
                                                <td align="left" valign="top" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px;">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>





                <!-- Price promise -->

                <tr>
                    <td align="center" style=" padding: 35px; background-color: #044767;" bgcolor="#044767">
                        <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                            <tr>
                                <td align="center" valign="top" width="600">
                        <![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                            <tr>
                                <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;">
                                    <h2 style="font-size: 24px; font-weight: 800; line-height: 30px; color: #ffffff; margin: 0;">
                                        Our price promise to you!
                                    </h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" >
                                                <p style="font-size: 18px; font-weight: 500; line-height: 30px; color: #ffffff; margin: 0;">We guarantee you'll pay the same price for a service booked on <?= env('SITE_NAME');?> as you would going direct*.
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>




                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>

                {{--<tr>--}}
                {{--<td align="center" style=" padding: 35px; background-color: #1b9ba3;" bgcolor="#1b9ba3">--}}
                {{--<!--[if (gte mso 9)|(IE)]>--}}
                {{--<table align="center" border="0" cellspacing="0" cellpadding="0" width="600">--}}
                {{--<tr>--}}
                {{--<td align="center" valign="top" width="600">--}}
                {{--<![endif]-->--}}
                {{--<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">--}}
                {{--<tr>--}}
                {{--<td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 10px;">--}}
                {{--<h2 style="font-size: 24px; font-weight: 800; line-height: 30px; color: #ffffff; margin: 0;">--}}
                {{--Promo Text here--}}
                {{--</h2>--}}
                {{--</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td align="center" style="padding: 25px 0 15px 0;">--}}
                {{--<table border="0" cellspacing="0" cellpadding="0">--}}
                {{--<tr>--}}
                {{--<td align="center" style="border-radius: 5px;" bgcolor="#66b3b7">--}}
                {{--<a href="/" target="_blank" style="font-size: 18px; font-family: Open Sans, Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; border-radius: 5px; background-color: #66b3b7; padding: 15px 30px; border: 1px solid #66b3b7; display: block;">Yes, please!</a>--}}
                {{--</td>--}}
                {{--</tr>--}}
                {{--</table>--}}
                {{--</td>--}}
                {{--</tr>--}}
                {{--</table>--}}


                {{----}}

                {{--<!--[if (gte mso 9)|(IE)]>--}}
                {{--</td>--}}
                {{--</tr>--}}
                {{--</table>--}}
                {{--<![endif]-->--}}
                {{--</td>--}}
                {{--</tr>--}}










                <tr>
                    <td align="center" style="padding: 35px; background-color: #ffffff;" bgcolor="#ffffff">
                        <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                            <tr>
                                <td align="center" valign="top" width="600">
                        <![endif]-->
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                            <tr>
                                <td align="center" style="font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 24px;">

                                    <p style="font-size: 12px; font-weight: 400; line-height: 20px; color: #777777;">
                                        <?= env('SITE_NAME');?> Ltd, {{ env('HELLO_EMAIL_SITE') }} *Price Promise Terms and conditions apply<br/></br>


                                        Add {{ env('HELLO_EMAIL_SITE') }} to your contacts to make sure you'll always see our emails. <?= env('SITE_NAME');?> will never email you asking for bank or card details. It's wise to change your passwords regularly.</br></br>
                                        Registered in England & Wales No. 10350926 | 75 Park Lane, Croydon, CR9 1XS,  UK</p>

                                    <p style="font-size: 12px; font-weight: 400; line-height: 20px; color: #777777;">
                                        If you didn't make a booking using this email address, please ignore this email or forward it to {{ env('HELLO_EMAIL_SITE') }}.
                                    </p>





                                </td>
                            </tr>
                        </table>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
</table>

</body>
</html>



