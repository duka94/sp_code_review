<b>Congratulations, you've received a new booking!</b><br />
<br />
<?=$category_name;?><br />
<?=$date;?><br />
arrival between <?=$timeslot_from;?>-<?=$timeslot_to;?><br />
<br />
Next steps:<br />
<br />
1. Review this booking online <a href="<?=env('APP_URL');?>login">here</a><br />
2. Call the customer to confirm the booking and finalise any outstanding payment required <br />
<br />
Remember: If you need to amend the booking at any time, please contact the customer directly. <br />
 
<br />
<br />
See you online,
<br />
The  <?= env('SITE_NAME');?> Team
