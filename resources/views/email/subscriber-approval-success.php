Hi <?=$company_name;?>, <br />
 <br />
Congratulations, your registration approval has been successful.
 <br /><br />
Your company will now be visible on  <?= env('SITE_NAME');?>.
 <br /><br /><br />
See you online,
<br />
The  <?= env('SITE_NAME');?> Team
