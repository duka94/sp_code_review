@extends('layouts.admin.admin')

@section('sidebar')
    @include('layouts.admin.sidebar_master')
@endsection

@section( 'content' )

    <section class="content-header">
        <h1>
            Cities
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-cogs"></i> Master</a></li>
            <li class="active">Cities</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

            <div>
                <a href="/master/cities/create" class="btn btn-primary">New City</a>
            </div>
        <div id="app" :delete_url="{{json_encode(url('/'))}}">

            <div class="row">
                <div class="col-xs-12">

                    <div class="table">
                        <v-client-table :data="_orders" :columns="headers" :options="options"></v-client-table>
                    </div>

                    <div class="spinner" v-if="loading">
                        <!--<div class="spinner" v-if="isSending">-->
                        <loader :loading="loading" :color="color" :size="loaderSize" class="text-center"></loader>
                    </div>
                </div>

            </div>
        </div>

    </section>
@endsection

@section('js')
    <script>

        var app = new Vue({

            el: '#app',

            data: {
                loading: false,
                headers: ['name', 'postcode', 'active', 'edit'],
                cities: '{!! json_encode($cities)!!}',
                options: {
                    headings: {
                        postcode: "Postcode's",
                        name: 'City name'
                    },
                    filterable: ['name'],
                    templates: {
                        edit: function (row) {
                            return '<a class="btn btn-primary" href="/master/cities/edit/'+row.id+'"><i class="fa fa-edit"></i> </a>';
                        },
                        {{--delete: function (row) {--}}
                            {{--return '<a class="btn btn-danger fa fa-trash" @click="$parent.deleteCity('+row.id+')""></a>';--}}
                        {{--},--}}
                        active: function (row) {
                            {{--return `<a href='javascript:void(0);' @click='$parent.changeApproved(${row.id})'><i class='glyphicon glyphicon-erase'></i></a>`--}}

                            return `
                            <div class="onoffswitch">
                                    <input type="checkbox" class="onoffswitch-checkbox"
                            id="${row.id}"
                            name="${row.id}"
                                    @change="$parent.changeActive(${row.id})"
                                        ${ !row.deleted_at ? 'checked' : ''}
                                    >
                                    <label class="onoffswitch-label" for="${row.id}">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                    </label>
                                    </div>
                            `
                        },
                    },
                    perPage: 100
                },
            },
            props: ['delete_url'],
            methods: {

//                deleteCity:function(deleteId) {
//                    var delete_url = this.delete_url;
//
//                    var x = confirm("Are you sure you want to delete?");
//                    if (x) {
//                        window.location.href = delete_url + '/master/cities/delete/' + deleteId;
//                    }
//                },
                changeActive: function (id) {
                    this.loading = true;

                    this.$http.post('/master/city/active/'+id, [])
                            .then(
                                    function () {
                                        this.loading = false;
    //                                this.scheduled = this.cleanJson(data);
    //                                this.showMessage('success', 'Your timeslot has been deleted successfully');

                                    }.bind(this),

                                    function (error) {
                                        this.loading = false;
    //                                this.showMessage('danger', 'Error!');
                                        console.log(error, 'ERR');

                                    }.bind(this)
                            );
            },


            },
            computed: {
                _orders: function () {
                    if( _.isString(this.cities) ) {
                        try {
                            return JSON.parse(this.cities)
                        }
                        catch(e) {
                            return this.cities
                        }
                    }

                    return this.cities;
                }
            },
            mounted: {

            }

        });

    </script>
@endsection