@extends('layouts.admin.admin')

@section('sidebar')
    @include('layouts.admin.sidebar_master')
@endsection

@section( 'content' )

    <section class="content-header">
        <h1>
            Create city
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-cogs"></i> Master</a></li>
            <li class="active">Create city</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div id="form-message" class="city-create">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <form action="/master/cities/update/{{$city->id}}" method="post">
                        {{csrf_field()}}
                        <label for="name">City name</label>
                        <input type="text" name="name" id="name" class="form-control"
                               value="{{!old('name') ? $city->name : old('name')}}" required>
                        <label for="name">Postcodes:</label>
                        <button class="btn btn-default float-right" type="button" id="add-new-postcode">New postcode
                        </button>
                        <div class="city-postcodes">
                            @if(!empty($errors) && count($errors->all()) > 0)
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                            @endif
                            @foreach($postcodes as $postcode)
                                <div class="city-postcodes-input">
                                    <input type="text" name="postcodes[{{$postcode->id}}]"
                                           value="{!! $postcode->name !!}" required>
                                    <button type="button" class="remove-postcode">X</button>
                                </div>
                            @endforeach
                        </div>
                        <button class="btn btn-success float-right" type="submit">Submit</button>
                    </form>


                </div>

            </div>
        </div>

    </section>
@endsection

@section('js')
    <script>
        $('#add-new-postcode').on('click', function (e) {
            $('.city-postcodes').append('<div class="city-postcodes-input"> ' +
                    '<input type="text" name="postcodes[]" required>' +
                    '<span> <button type="button" class="remove-postcode">X</button> </span>' +
                    '</div>');
        });

        $(document).on('click', '.remove-postcode', function (e) {
            var _this = this;
            var postocodes = $('.city-postcodes-input').length;
            if (postocodes > 1) {
                $(_this).parents('div.city-postcodes-input').remove();
            }
        })
    </script>
@endsection