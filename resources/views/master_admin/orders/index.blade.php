@extends('layouts.admin.admin')

@section('sidebar')
    @include('layouts.admin.sidebar_master')
@endsection

@section( 'content' )

    <section class="content-header">
        <h1>
            Bookings
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-cogs"></i> Master</a></li>
            <li class="active">Bookings</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div id="app" :delete_url="{{json_encode(url('/'))}}">

            <div class="row">
                <div class="col-xs-12">

                    <div class="table">
                        <v-client-table :data="_orders" :columns="headers" :options="options"></v-client-table>
                    </div>

                    <div class="spinner" v-if="loading">
                        <!--<div class="spinner" v-if="isSending">-->
                        <loader :loading="loading" :color="color" :size="loaderSize" class="text-center"></loader>
                    </div>
                </div>

            </div>
        </div>

    </section>

@endsection

@section('js')
    <script>

        var app = new Vue({

            el: '#app',

            data: {
                headers: ['city','company_name','from',  'booking_date', 'service_date','_viewmore', 'delete'],
                orders: <?= json_encode( $orders ) ?>,
                options: {
                    headings: {
                        _viewmore: 'View more',
                        _delete: 'Delete',
                        from: 'Client name',
                        booking_date: 'Booking date',
                        city: 'City'
                    },
                    filterable: ['city','company_name','from','service_date'],
                    orderBy: { 'column': 'booking_date', 'descending': false},
                    templates: {
                        _viewmore: function (row) {
                            return '<a class="btn btn-primary" href="/master/bookings/' + row.id + '">View more</a>';
                        },
                        booking_date: function (row) {
                            return moment(String(row.booking_date)).format('D.M.YY');
                        },
                        delete: function (row) {
                            return '<button class="btn btn-danger fa fa-trash fa-6" @click="$parent.deleteBooking('+row.id+')" href="/master/bookings/' + row.id + '""></button>';
                        }
                    },
                    perPage: 100
                }
            },
            props: ['delete_url'],
            methods: {

                deleteBooking:function(deleteId) {
                    var delete_url = this.delete_url;

                    var x = confirm("Are you sure you want to delete?");
                    if (x) {
                        window.location.href = delete_url + '/master/bookings/delete/' + deleteId;
                    }
                },
                getOrderedDate: function () {

                }


            },
            computed: {
                _orders: function () {
                    if( _.isString(this.orders) ) {
                        try {
                            return JSON.parse(this.orders)
                        }
                        catch(e) {
                            return this.orders
                        }
                    }

                    return this.orders;
                }
            }

        });

    </script>
@endsection