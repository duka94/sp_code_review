@extends('layouts.admin.admin')

@section('sidebar')
    @include('layouts.admin.sidebar_master')
@endsection

@section( 'content' )

    <section class="content-header">
        <h1>
            {{$heading}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-cogs"></i> Master</a></li>
            <li class="active">Bookings</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div id="app">

            <div class="row">
                <div class="col-xs-12">

                    <div class="table">
                        <v-client-table :data="_tooltips" :columns="headers" :options="options"></v-client-table>
                    </div>

                    <div class="spinner" v-if="loading">
                        <!--<div class="spinner" v-if="isSending">-->
                        <loader :loading="loading" :color="color" :size="loaderSize" class="text-center"></loader>
                    </div>
                </div>

            </div>
        </div>

    </section>

@endsection

@section('js')
    <script>

        var app = new Vue({

            el: '#app',

            data: {
                headers: ['name','tooltip_text', 'tooltip_active','_edit'],
                tooltips: '{!! json_encode($services ) !!}',
                options: {
                    headings: {
                        _edit: 'Edit tooltip',
                        tooltip_text: 'Tooltip text',
                        tooltip_active: 'Is active',
                        name: 'Name'
                    },
                    filterable: ['name','tooltip_text'],
                    templates: {
                        tooltip_active: function (row) {
                                {{--return `<a href='javascript:void(0);' @click='$parent.changeApproved(${row.id})'><i class='glyphicon glyphicon-erase'></i></a>`--}}

                                return `
                            <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox"
                                id="${row.id}"
                                name="${row.id}"
                                        @change="$parent.changeApproved(${row.id})"
                                            ${row.tooltip_active ? 'checked' : ''}
                                        >
                                        <label class="onoffswitch-label" for="${row.id}">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                        </label>
                                        </div>
                            `
                            },
                        _edit: function (row) {
                            return '<a class="btn btn-primary fa fa-edit" href="/master/edit/tooltip/' + row.id + '"></a>';
                        }
                    },
                    perPage: 100
                }
            },
            methods: {
                changeApproved: function(id) {
                    this.loading = true;

                    this.$http.get('/master/changeApproved/tooltip/'+id, [])
                            .then(
                                    function () {
                                        this.loading = false;
                                    }.bind(this),
                                    function (error) {
                                        this.loading = false;
                                        console.log(error, 'ERR');

                                    }.bind(this)
                            );
                },
            },
            computed: {
                _tooltips: function () {
                    if( _.isString(this.tooltips) ) {
                        try {
                            return JSON.parse(this.tooltips)
                        }
                        catch(e) {
                            return this.tooltips
                        }
                    }

                    return this.tooltips;
                }
            }

        });

    </script>
@endsection