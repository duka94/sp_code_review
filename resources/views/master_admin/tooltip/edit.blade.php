@extends('layouts.admin.admin')

@section('sidebar')
    @include('layouts.admin.sidebar_master')
@endsection

@section( 'content' )

    <section class="content-header">
        <h1>
            Create city
        </h1>
        <ol class="breadcrumb">
            <li><a href="/master"><i class="fa fa-cogs"></i> Master</a></li>
            <li><a href="/master#tooltip_dd"> Tooltip</a></li>
            <li class="active">Update tooltip</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div id="form-message" class="city-create">
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <form action="/master/update/tooltip/{{$service->id}}" method="post">
                        {{csrf_field()}}
                        <h2 for="name">{{$service->name}}</h2>

                        <label for="name">Tooltip text:</label>
                        <input type="text" id="name" class="form-control" name="tooltip_text" value="@if(isset($error) && $error->has('tooltip_text')){{old('tooltip_text')}}@else{{ $service->tooltip_text }} @endif" required>

                        <button class="btn btn-success float-right" type="submit">Save</button>
                    </form>


                </div>

            </div>
        </div>

    </section>
@endsection
