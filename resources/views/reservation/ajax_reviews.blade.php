@foreach ($reviews as $review)
    <div class="review">
        <div class="inline-block review-circle-wrapp" data-letters="{{$review->name[0]}}"></div>
        <div class="review-content-wrapp inline-block">
            <div class="reviewer">
                <div class="name verified">
                    @if($review->name != 'No Name')
                        {{$review->name}}
                    @else
                        VERIFIED CUSTOMER
                    @endif
                </div>
            </div>
            <div class="date">{{ $review->created_at }}</div>
            <div class="review-rating">
                @for($i=1; $i<$review->rating; $i++)
                    <div class="star"></div>
                @endfor
                @if ($i - $review->rating == 0.5)
                    <div class="star half"></div>
                @else
                    <div class="star"></div>
                @endif
                @if(5 - $review->rating >= 1)
                    @for($i=1; $i <= 5 - $review->rating; $i++)
                        <div class="star star-grey"></div>
                    @endfor
                @endif
            </div>
            <span class="more-review">{{ $review->comment }}</span>
        </div>
    </div>
@endforeach