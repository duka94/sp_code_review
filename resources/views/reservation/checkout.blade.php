@extends('reservation.layout.layout')

@section('css')
    <style>
        .sidebar{
            width:25%;
        }
        .main-content{
            width:73%;
        }
        .sidebar ul.service-list {
            overflow-y: scroll;
        }
        
          .main-content h3 {  padding-bottom: 0px;}
        hr {margin-top: 0px;}

        .StripeElement {
            background-color: white;
            padding: 8px 12px;
            border-radius: 4px;
            border: 1px solid transparent;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }

        
input[type=text],[type="email"]{
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}

/*.input.card-input:before {top: calc(0% + 0px);}*/
    </style>
@endsection


@section('content')
    <div id="service-octopus">
        <section class="top-bar">
            <div class="container">
                <h2>Complete your booking with {{ $company->name }}</h2>
            </div>
        </section>

        <section>
            <form method="post" id="payment-form">
                {{ csrf_field() }}
                <input type="hidden" name="companyId" value="{{ $company->id }}">
                <input type="hidden" name="coupon" value="@{{ couponCode }}">
            <div class="container flex">
                <!-- MAIN CONTENT -->
                <div class="main-content">





                    {{--@if (count($errors) > 0)--}}
                        {{--<div class="alert alert-danger">--}}
                            {{--<ul>--}}
                                {{--@foreach ($errors->all() as $error)--}}
                                    {{--<li>{{ $error }}</li>--}}
                                {{--@endforeach--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--@endif--}}

                    <input type="hidden" name="date" value="{{ request('date') }}">
                    <input type="hidden" name="date" value="{{ request('time') }}">
                    <input type="hidden" name="postCode" value="{{ request('postCode') }}">
                    <input type="hidden" name="serviceCategoryId" value="{{ request('serviceCategoryId') }}">
                    @foreach(request('services') as $service => $quantity)
                        <input type="hidden" name="service[{{ $service }}]" value="{{ $quantity }}">
                    @endforeach


      		    <h3>Contact details</h3>
                    <hr>
                    <div class="row">
                        @if(!empty($errors))
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger">{{ $error }}</div>
                            @endforeach
                        @endif
                     <div class="input column half">
                            <label for="">First name</label>
                            <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" required>
                        </div>
                        <div class="input column half">
                            <label for="">Last name</label>
                            <input type="text" name="last_name" id="last_name" value="{{ old('last_name') }}" required>
                        </div>


                  <div class="input column half">
                            <label for="">Email</label>
                            <input type="email" name="email" id="email" value="{{ old('email') }}" required>
                        </div>
                        <div class="input column half">
                            <label for="">Confirm Email</label>
                            <input type="email" name="email_confirm" id="email_confirm" value="{{ old('email_confirm') }}" required>

                        </div>
                        <div class="input column half">
                            <label for="">Mobile</label>
                            <input type="text" name="mobile" id="mobile" value="{{ old('mobile') }}" required>

                        </div>

                    </div>

                    <h3>Property Address</h3>
                    <hr>
                    <div class="row">
                        <div class="input column half">
                            <label for="">House/Flat number</label>
                            <input type="text" name="house_number" id="house_number" value="{{ old('house_number') }}" required>
                        </div>
                        <div class="input column half">
                            <label for="">Address Line 1</label>
                            <input type="text" name="address_line_1" id="address_line_1" value="{{ old('address_line_1') }}" required>
                        </div>
                        <div class="input column half">
                            <label for="">Address Line 2 (optional)</label>
                            <input type="text" name="address_line_2" id="address_line_2">
                        </div>
                        <div class="input column half">
                            <label for="">City/Town</label>
                            <input type="text" name="city_town" id="city_town" value="{{ old('city_town') }}" required>
                        </div>
                    </div>

                    <h3>Special Instructions</h3>
                    <hr>
                    <div class="row">
                        <div class="input column full">
                            <textarea name="instructions" id="instructions" cols="30" rows="3"></textarea>
                        </div>
                    </div>



                    <h3>Payment</h3>
                    <hr>
                    <div class="row">

			  <divl class="col-md-12" style="padding-bottom: 30px;">
                            <div id="card-element"></div>
                            <div id="card-errors"></div>
                        </divl>

                        <div class="column half">
                            <div class="input">
                                <div class="card-badges">
                                    <img src="/images/visa-logo.svg" width="50px;" alt="Visa logo">
                                    <img src="/images/mastercard-logo.svg" width="50px;" alt="MasterCard logo">
                                    <img src="/images/american-express-logo.svg" width="36px;" alt="American Express logo">
                                </div>
                            </div>
                            {{--<div class="input card-input">--}}
                               {{--<label for="">Card number</label>--}}
                                {{--<input type="text" class="cc-number" name="card_number" id="card_number" value="{{ old('card_number') }}" required>--}}
                            {{--</div>--}}
                            {{--<div class="input">--}}
                                {{--<div class="exp-date">--}}
                              {{--<label for="">Expiry date</label><br/>--}}
                                    {{--<div class="input mm">--}}
                                        {{--<input type="text" class="cc-numeric" placeholder="MM" name="expiration_month" id="expiration_month" value="{{ old('expiration_month') }}" required>--}}
                                    {{--</div>--}}
                                    {{--<div class="input yyyy">--}}
                                        {{--<input type="text" class="cc-numeric" placeholder="YYYY" name="expiration_year" id="expiration_year" value="{{ old('expiration_year') }}" required>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="cvc">--}}
                                    {{--<label for="">CVC</label><br/>--}}
                                    {{--<div class="input">--}}
                                        {{--<input type="text" class="cc-cvc" placeholder="CVC" name="cvc" id="cvc" value="{{ old('cvc') }}" required>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>

                        <style>
                            /*overriding styled checkobx to show html5 validation messages*/
                            #terms {
                                position: static;
                                display: inline-block;
                                width: 30px;
                            }
                            .input.checkbox-input:before{
                                display: none;
                            }
                        </style>
                        <div class="column half">
                            <div class="input checkbox-input column full @if($errors->has('terms')) has-error-border @endif" style="margin-top:-10px;">
                                <label for="terms">
                                <input type="checkbox" id="terms" name="terms" required >

                                I have read and agree to the <a href="/customer-terms-and-conditions" target="_blank">terms and conditions</a> and <a href="/privacy-policy" target="_blank">privacy policy</a>.</label>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!-- /MAIN CONTENT -->
                <!-- SIDEBAR -->
                <div class="sidebar" id="coupon">
                    <div class="sidebar-inner">
                        <!-- SIDEBAR SECTION  FORM FIELDS-->
                        <div class="sidebar-section">
                           <div class="top-sidebar-quote">
                                <p>Total</p>
                                <div class="price">
                                    &pound; @{{ priceTotal }}.<span>00</span>
                                </div>
                           </div>
                       </div>
                        <div class="sidebar-section">
                            <div class="checkout-info-fields">
                                <div class="field">
                                    <div class="column full services">
                                        @if (request('serviceCategoryId') === 'tenancy')
                                            End of Tenancy Cleaning
                                        @elseif (request('serviceCategoryId') === 'deep')
                                            Deep Cleaning
                                        @elseif (request('serviceCategoryId') === 'carpet')
                                            Carpet Cleaning
                                        @endif
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="column full date">
                                        {{ \Carbon\Carbon::parse(request('date'))->toFormattedDateString() }}
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="column full time">
                                        {{ request('time') }}:00 - {{(int)request('time') + 1}}:00
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="column full zip-code">
                                        {{ request('postCode') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- SIDEBAR SECTION -->
                        <!-- SIDEBAR SECTION LIST-->
                        <div class="sidebar-section">
                            <ul class="service-list default-skin">
                                @foreach ($services as $service)
                                    <li><div class="ico"><img src="/images/icons/{{ $service->icon }}.png" alt="{{ $service->name }}"></div>{{ $selectedServices[$service->id] }} {{ $service->name }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- SIDEBAR SECTION -->
                        <!-- SIDEBAR SECTION PRICE-->








                        <div class="price-paynow-mobile">

                                     <!-- SIDEBAR SECTION PAY NOW-->
                            <div class="sidebar-section pay">
                                <div class="column-full">
                                    <p class="info">
                                        Due on {{ \Carbon\Carbon::parse(request('date'))->toFormattedDateString() }}:
                                        £{{ $duePrice }}
                                    <p>
                                        <b>Please note:</b> parking and congestion charging, if applicable, will be
                                        added
                                        on the day.
                                    </p>
                                    </p>

                                </div>
                            </div>

                           <!-- SIDEBAR SECTION PAY NOW-->
                           <div class="sidebar-section pay">
                               <div class="column-full">
                                  <!--  <p class="info">
                                        Due on {{ \Carbon\Carbon::parse(request('date'))->toFormattedDateString() }}: £{{ $duePrice }}
                                    </p>-->
                                   <!-- <button class="btn">Pay now(&pound;35)<span class="ico"></span></button> -->
                                   <button class="btn" :disabled="clicked">Pay now (&pound;<span>@{{ price }}</span>)<span class="ico"></span></button>


                                      <div class="coupon-wrapper" v-if="price > 5 || couponApplied == true">
                                      <div id="coupon-btn" class="coupon-btn">Coupon</div>
                                      <div id="coupon-box" class="coupon-box">
                                          <div class="row">
                                              <div class="input column full">
                                                  <input type="text" name="coupon" placeholder="Coupon code" v-model="couponCode">
                                              </div>
                                              <div class="input column full">
                                                  <a class="" v-on:click="applyCoupon()" v-if="couponApplied == false">Apply Coupon</a>
                                                  <div v-if="!couponApplied" id="coupon-btn-close" class="coupon-btn close">Cancel</div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>


                                   <div class="info" style="margin-top:15px;">
                                       {{--<p>You will only need to pay <strong>25% of the total fee right now</strong> - we will reauire the rest once the job is completed and confirmed</p>--}}
                                        <p>Pay a booking fee today to secure your timeslot. The fee is deducted from the total cleaning cost and helps us run our secure platform.</p>

                                   </div>

                              <div class="sidebar-section" id="couponMessage" v-html="couponMessage" style="border-bottom: none;"></div>

                              </div>
                           </div>


                             <!-- SIDEBAR SECTION PRICE-->
                    <!--       <div class="sidebar-section price-section">
                              <div class="column full">

                                  <!-- PRICE BOX 1
                                   <div class="price-box">
                                       <label style="display: inline;float: left;width: auto;top: 11px;margin-right: 10px;"
                                       >Total:</label>
                                       <span class="price">
                                           &pound;@{{ priceTotal }}.<span>00</span>
                                       </span>



                                   </div>-->





                                   <!-- PRICE BOX 2 -->
                                   <!-- <div class="price-box price-booking">
                                       <label>Total:</label>
                                       <div class="price">
                                           &pound;365.<span>00</span>
                                       </div>
                                   </div>  -->

                              </div>
                           </div>
                           <!-- SIDEBAR SECTION -->


                       </div>





                    </div>
                </div>
            </form>
        </section>
@endsection
@section('scripts')
    <script>

        var companyList = new Vue({
            el: '#coupon',
            data: {
                price: '{{ $price->getBookingFee() }}',
                priceTotal: '{{ $price->getCleaningPrice() }}',
                oldPrice: '{{ $price->getBookingFee() }}',
                couponCode: '{{ old('coupon') }}',
                couponMessage: '',
                couponApplied: false,
                stripe_pk_key: '{{$stripe_pk_key}}',
                clicked: false
            },
            methods: {
                applyCoupon: function () {
                    this.$http.post('/callback/applyCoupon', {coupon: this.couponCode}).then(function (response) {
                        var discount = (this.price - response.data.discount) < 1 ? (this.price - 1) : response.data.discount;
                        this.price -= discount;
                        this.priceTotal -= discount;
                        this.couponMessage = 'Coupon applied.';
                        this.couponApplied = true;
                    },function (response) {
                        // Log error
                        this.couponMessage = 'Invalid Coupon provided.';
                    });
                }
            },
            mounted: function() {
                if (this.couponCode) {
                    this.applyCoupon();
                }
            }
        });


        $(document).ready(function() {
            jQuery('#datetimepicker').datetimepicker({
                value:'{{  request('date') }}'
            });
            $('.sidebar-inner').stick_in_parent();

            $('.cc-number').payment('formatCardNumber');
            $('.cc-exp').payment('formatCardExpiry');
            $('.cc-numeric').payment('restrictNumeric');
            $('.cc-cvc').payment('formatCardCVC');


        });
        //CHECKOUT APPLY COUPON CODE
        $('#coupon-btn').on('click', function() {
            $(this).toggleClass('hide');
            $('#coupon-box').toggleClass('active');
        });

        $('#coupon-btn-close').on('click', function() {
            $(this).toggleClass('active');
            $('#coupon-box').toggleClass('active');
            $('#coupon-btn').toggleClass('hide');
            //$('#couponMessage').html('');
            companyList.couponMessage = '';
            companyList.couponCode = '';
            companyList.price = companyList.oldPrice;
        });

        var stripe = Stripe(companyList.stripe_pk_key);
        var elements = stripe.elements();

        var style = {
            base: {
                color: '#32325d',
                lineHeight: '24px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };
        var card = elements.create('card', {style: style});
        card.mount('#card-element');

        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Create a token or display an error when the form is submitted.
        var form = document.getElementById('payment-form');

        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            // Submit the form
            form.submit();
        }

        form.addEventListener('submit', function(event) {
            companyList.clicked = true;
            event.preventDefault();

            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    companyList.clicked = false;
                    // Inform the user if there was an error
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server
                    stripeTokenHandler(result.token);
                }
            });
        });
        //CHECKOUT APPLY COUPON CODE
    </script>
@endsection
