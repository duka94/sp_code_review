@extends('reservation.layout.layout')
@section('css')
    <link href="{{ asset('/css/animate.min.css') }}" rel="stylesheet" type="text/css"
          xmlns:v-on="http://www.w3.org/1999/xhtml">
@endsection
@section('content')
    <style>

        .star-grey{
            position: relative;
            display: inline-block;
            vertical-align: top;
            width: 14px;
            height: 14px;
            background-repeat: no-repeat;
            background-size: contain;
            margin-right: 4px;
        }
        .compare-page .top-filters .mobile-cogwheel-btn {
            padding: 12px;
        }

        .cancel-apply-filters a.btn:first-child, .cancel-apply-filters button.btn:first-child {
            background: #F7038E;
            color: white;
        }

        .select-styled {
            position: relative;
        }

        @media (min-width: 750px) and (max-width: 970px) {

            #service-octopus {
                margin-top: 44px
            }
        }

    </style>

    <div id="service-octopus" class="compare-page">
        <section>


            <div class="container flex" id="companyList" style="display: none">

                <!-- MAIN CONTENT -->
                <div class="main-content">

                    <div class="top-filters">
                        <div class="left-filters">
                            <span v-html="serviceCategoryName"></span><span class="for">for</span><span
                                    v-html="serviceText"></span>

                            <div class="mobile-cogwheel-btn"><img src="/images/cog-wheel.svg" alt=""></div>
                        </div>
                        <div class="right-arrange">
                            <p>Arrange by</p>

                            <div class="input">
                                <div class="sele img{{asset('images/tooltip_img.png')}} ct">
                                    <div class="select-styled" :class="{active: showSort}" @click="showSort = true"
                                    v-text="sorting[selectedOrder]">Sort
                                </div>
                                <ul class="select-options" :class="{visible: showSort}" @click="showSort = false">
                                <li v-for="(order, key) in sorting" v-text="order" @click="
                                selectedOrder = key; getCompanies()"></li>
                                </ul>
                            </div>
                        </div>
                        {{--<div class="input">--}}
                        {{--<select name="order" id="order" v-model="selectedOrder" v-on:change="getCompanies()">--}}
                        {{--<option value="price">Price</option>--}}
                        {{--<option value="reviews">Reviews</option>--}}
                        {{--<!--option value="rating">Rating</option-->--}}
                        {{--</select>--}}
                        {{--</div>--}}
                    </div>
                </div>
                <div class="companies-list">
                    <div v-if="noCompanies == true">I'm afraid there are no companies available to service your
                        request.
                    </div>

		            <a href="https://www.serviceoctopus.com/price-promise/" target="_blank"><img src="/images/Never-Cheaper-Direct.png" alt="Never Cheaper Direct!" style="padding-bottom: 26px;"></a>
                    <!-- COMPANY -->
                    <div class="company" v-for="company in companies">
                        <div class="c-name">
                            <div class="img-holder">
                                <a :id="'details_'+company.id" :href="companyDetails(company)"><img :src="company.image" :alt="company.name"></a>
                            </div>
                            <a :id="'details_'+company.id" :href="companyDetails(company)"><h3>@{{ company.name }}</h3></a>
                            <div class="rating-modal" v-if="company.review_rate != 0" :data-id="company.id">
                            <div type="button" class="review-rating">
                                <div class="star" v-for="n in company.review_rate">
                                </div><div class="star half" v-if="company.is_half_rate == 1">
                                </div><div class="star star-grey" v-for="n in 5-company.is_half_rate-company.review_rate">
                                </div></div>
                            <p v-if="company.number_of_reviews > 0">(@{{ company.number_of_reviews }} Reviews)</p>
                            </div>
                        </div>
                        <div class="c-info">

                            <div><p>Liability<br>Insurance</p>
                                <span v-if="company.liability_insurance">&pound;@{{ company.liability_insurance }}
                                    mill.</span>
                                <span v-else>None</span>
                            </div>
                            <div><p>Cleaning<br>Guarantee</p>
                                <span>@{{ getGuarantee(company.cleaning_guarantee) }}</span></div>
                            <div><p>Servicing<br>since</p><span>@{{ company.servicing_since }}</span></div>

                        </div>
                        <div class="c-price" v-bind:class="{'shake animated': shake }">
                            <p>Total price</p>
                            <span>&pound;@{{ company.total_price }}</span>
                        </div>
                        <div class="c-book">
                            <a class="btn" :id="'book_'+company.id" :href="bookNow(company)">
                                Book Now (&pound;@{{ company.booking_fee }})
                            </a>
                            <a class="btn see-more" :id="'details_'+company.id"
                               :href="companyDetails(company)">
                                See more
                            </a>
                        </div>

                    </div>
                    <!-- /COMPANY -->
                </div>
            </div>
            <!-- /MAIN CONTENT -->


            <!-- SIDEBAR -->
            <div class="sidebar">
                <!-- Mobile button for showing filters -->
                <div class="mobile-filters"></div>
                <!-- Mobile button for showing filters -->

                <div class="sidebar-inner">
                    <!-- SIDEBAR SECTION  FORM FIELDS-->
                    <div class="sidebar-section">


                        <div class="input column services" style="display:none;">
                            <div class="select">
                                <div class="select-styled" @click="showServices = !showServices"
                                v-text="services[mainServiceCategory] || 'Select Service'"
                                :class="{active: showServices}"
                                >Select Service
                            </div>
                            <ul class="select-options" :class="{visible: showServices}" @click="showServices = false">
                            <li rel="hide" @click="mainServiceCategory = ''">Select Service</li>
                            <li v-for="(service, serviceId) in services" :rel="serviceId" @click="
                                mainServiceCategory = serviceId; updateAdditionalServices()" v-text="service"></li>
                            </ul>
                        </div>
                    </div>

                    <div class="input column full date" style="display:none;">
                        <datepicker v-model="registerData.date" :disabled="disabledTime" @input="
                                updateAdditionalServices"></datepicker>
                        {{--<input type="text" id="datetimepicker" placeholder="Select Date">--}}
                    </div>

                    <div class="input column time" style="display:none;">
                        <div class="select">
                            <div class="select-styled" @click="showTime = !showTime"
                            v-text="times[registerData.time] || 'Estimated Arrival Time'"
                            :class="{active: showTime}"
                            > Estimated Arrival Time
                        </div>
                        <ul class="select-options" :class="{visible: showTime}" @click="showTime = false">
                        <li rel="hide" @click="registerData.time = ''">Estimated Arrival Time</li>
                        <li @click="registerData.time = timeId" v-for="(time, timeId) in times" v-text="time"></li>
                        </ul>
                    </div>
                </div>

                <div class="input column full zipcode" style="display:none;">
                    <input type="text" placeholder="Enter Postcode" value="{{ $postCode }}"
                           v-model="registerData.postCode">
                </div>

                <!-- EXPANDABLE BOX -->
                <div class="expandable" v-if="'carpet' != mainServiceCategory">
                    <div class="expand-btn active">
                        <p>Property type</p>
                    </div>
                    <div class="expand-container active">
                        <div class="input">
                            <div class="input-acti img{{asset('images/tooltip_img.png')}} on">
                                <div class="switch">
                                    <input type="radio" v-on:change="changePropertyType" v-model="registerData.propertyType"
                                           class="switch-input" name="property_type" value="flat" id="flat" checked>
                                    <label for="flat" class="switch-label switch-label-off">Flat</label>
                                    <input type="radio" v-on:change="changePropertyType" v-model="registerData.propertyType"
                                           class="switch-input" name="property_type" value="house" id="house">
                                    <label for="house" class="switch-label switch-label-on">House</label>
                                    <span class="switch-selection"></span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="expandable" v-if="'carpet' != registerData.serviceCategoryId">
                    <div class="expand-btn active">
                        <p>Property Size</p>
                    </div>
                    <div class="expand-container active">
                        <div class="input">
                            <span class="add_categories">Bedrooms</span>
                            @if(isset($bedroomTooltip) && $bedroomTooltip->tooltip_active)
                                <popper trigger="click" :options="{placement: 'right'}" class="tooltip-div">
                                    <div class="tooltip_toggle">
                                        {{$bedroomTooltip->tooltip_text}}
                                    </div>

                                    <button slot="reference"></button>
                                </popper>
                            @endif
                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" :value="bedroomCount" id="13">

                                    <div class="ico"><img src="/images/bed.svg" alt="Bedrooms"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="setMinus($event, 13)"></div>
                                        <div class="plus" v-on:click="setPlus($event, 13)"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input">
                            <span class="add_categories">Bathrooms</span>
                            @if(isset($bathroomTooltip) && $bathroomTooltip->tooltip_active)
                                <popper trigger="click" :options="{placement: 'right'}" class="tooltip-div">
                                    <div class="tooltip_toggle">
                                        {{$bathroomTooltip->tooltip_text}}
                                    </div>

                                    <button slot="reference"></button>
                                </popper>
                            @endif
                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" :value="bathroomCount" id="14">

                                    <div class="ico"><img src="/images/bathroom.svg" alt="Bathrooms"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="setMinus($event, 14)"></div>
                                        <div class="plus" v-on:click="setPlus($event, 14)"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="expandable" v-if="'carpet' != registerData.serviceCategoryId">
                    <div class="expand-btn active">
                        <p>Additional Services</p>
                    </div>

                    <div class="expand-container active">
                        <div class="input">
                            <span>Key Collection </span>
                            @if(isset($keyCollection) && $keyCollection->tooltip_active)
                            <popper trigger="click" :options="{placement: 'right'}" class="tooltip-div">
                                <div class="tooltip_toggle">
                                 {{$keyCollection->tooltip_text}}
                                </div>

                                <button slot="reference"></button>
                            </popper>
                            @endif
                            <div class="input-action">
                                <div class="switch">
                                    <input type="radio" class="switch-input" name="key_collection" :value="false"
                                           v-model="keyCollection" v-on:click="setKeyCollection" id="na" data-id="108"
                                           checked>
                                    <label for="na" class="switch-label switch-label-off">N/A</label>

                                    <input type="radio" class="switch-input" name="key_collection"
                                           v-model="keyCollection" v-on:click="setKeyCollection" :value="true"
                                           id="required">
                                    <label for="required" class="switch-label switch-label-on">Required</label>
                                    <span class="switch-selection"></span>
                                </div>
                            </div>
                        </div>

                        <span class="input" v-for="additionalCategory in additionalCategories">
                          <div>  <span class="additional-category-span">@{{ additionalCategory.name }}
                            </span>
                                <popper trigger="click" :options="{placement: 'right'}" class="tooltip-div" v-if="additionalCategory.tooltip_active">
                                    <div class="tooltip_toggle">
                                        @{{ additionalCategory.tooltip_text }}
                                    </div>

                                    <button slot="reference"></button>
                                </popper>
                          </div>


                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" :value="additionalCategory.quantity" :id="additionalCategory.id">

                                    <div class="ico"><img :src="'/images/icons/'+additionalCategory.icon+'.png'"
                                                          :id="additionalCategory.name"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, additionalCategory.id)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, additionalCategory.id)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        </div>
                    </div>
                </div>
                <!-- EXPANDABLE BOX -->

                <!-- CARPET CLEANING -->
                <div class="expandable" v-if="'carpet' == registerData.serviceCategoryId">
                    <div class="expand-btn active">
                        <span class="additional-category-span">Carpet and rug cleaning</span>
                    </div>
                    <div class="expand-container active">
                        <span class="input">
                            <span class="additional-category-span">Lounge</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="1" id="3">

                                    <div class="ico"><img src="/images/icons/lounge.png" alt="Lounge"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 3)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 3)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Single Bedroom</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="1" id="1">

                                    <div class="ico"><img src="/images/icons/bed.png" alt="Single Bedroom"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 1)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 1)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Dining Room</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="4">

                                    <div class="ico"><img src="/images/icons/diningroom.png" alt="Dining Room"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 4)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 4)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Double Bedroom</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="2">

                                    <div class="ico"><img src="/images/icons/mattress_bedroom.png" alt="Double Bedroom">
                                    </div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 2)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 2)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Hallway</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="5">

                                    <div class="ico"><img src="/images/icons/hallway.png" alt="Hallway"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 5)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 5)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Stairs/Landing</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="67">

                                    <div class="ico"><img src="/images/icons/stairs.png" alt="Stairs/Landing"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 67)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 67)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Kitchen</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="6">

                                    <div class="ico"><img src="/images/icons/kitchen.png" alt="Kitchen"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 6)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 6)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Bathroom</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="68">

                                    <div class="ico"><img src="/images/icons/bathroom.png" alt="Bathroom"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 68)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 68)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Study</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="69">

                                    <div class="ico"><img src="/images/icons/study.png" alt="Study"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 69)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 69)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Rug (Small/Medium)</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="70">

                                    <div class="ico"><img src="/images/icons/rug.png" alt="Rug (Small/Medium)"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 70)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 70)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Rug (Large 10ft+)</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="71">

                                    <div class="ico"><img src="/images/icons/rug.png" alt="Rug (Large 10ft+)"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 71)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 71)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </span>
                </div>

                <div class="expandable" v-if="'carpet' == registerData.serviceCategoryId">
                    <div class="expand-btn">
                        <span class="additional-category-span">Fabric sofa cleaning</span>
                    </div>
                    <div class="expand-container">
                        <span class="input">
                            <span class="additional-category-span">Single Seater / Armchair</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="15">

                                    <div class="ico"><img src="/images/icons/sofa.png" alt="Single Seater / Armchair">
                                    </div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 15)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 15)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">2 Seater Sofa</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="9">

                                    <div class="ico"><img src="/images/icons/sofa.png" alt="2 Seater Sofa"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 9)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 9)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">3 Seater Sofa</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="10">

                                    <div class="ico"><img src="/images/icons/sofa.png" alt="3 Seater Sofa"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 10)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 10)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">4 Seater/Corner Sofa</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="11">

                                    <div class="ico"><img src="/images/icons/sofa.png" alt="4 Seater/Corner Sofa"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 11)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 11)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Foot Stool</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="45">

                                    <div class="ico"><img src="/images/icons/lounge.png" alt="Foot Stool"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 45)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 45)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Dining Chair</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="46">

                                    <div class="ico"><img src="/images/icons/lounge.png" alt="Dining Chair"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 46)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 46)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                    <!-- END CARPET CLEANING -->
                </div>

                <div class="expandable" v-if="'carpet' == registerData.serviceCategoryId">
                    <div class="expand-btn">
                        <span class="additional-category-span">Curtains and blinds</span>
                    </div>
                    <div class="expand-container">
                        <span class="input">
                            <span class="additional-category-span">Pelmets</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="47">

                                    <div class="ico"><img src="/images/icons/carpet.png" alt="Pelmets"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 47)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 47)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Curtains (Half Length Pair)</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="48">

                                    <div class="ico"><img src="/images/icons/carpet.png"
                                                          alt="Curtains (Half Length Pair)"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 48)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 48)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Curtains (¾ Length Pair)</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="49">

                                    <div class="ico"><img src="/images/icons/carpet.png" alt="Curtains (¾ Length Pair)">
                                    </div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 49)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 49)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Curtains (Full Length Pair)</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="50">

                                    <div class="ico"><img src="/images/icons/carpet.png"
                                                          alt="Curtains (Full Length Pair)"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 50)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 50)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Blind</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="51">

                                    <div class="ico"><img src="/images/icons/blinds.png" alt="Blind"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 51)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 51)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>

                <div class="expandable" v-if="'carpet' == registerData.serviceCategoryId">
                    <div class="expand-btn">
                        <span class="additional-category-span">Mattresses</span>
                    </div>
                    <div class="expand-container">
                        <span class="input">
                            <span class="additional-category-span">Mattress (Single)</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="72">

                                    <div class="ico"><img src="/images/icons/mattress_bedroom.png"
                                                          alt="Mattress (Single)"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 72)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 72)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Mattress (Double)</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="73">

                                    <div class="ico"><img src="/images/icons/mattress_bedroom.png"
                                                          alt="Mattress (Double)"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 73)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 73)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Mattress (King Size)</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="74">

                                    <div class="ico"><img src="/images/icons/mattress_bedroom.png"
                                                          alt="Mattress (King Size)"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 74)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 74)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Mattress (Queen Size)</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="75">

                                    <div class="ico"><img src="/images/icons/carpet.png" alt="Mattress (Queen Size)">
                                    </div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 75)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 75)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>

                <div class="expandable" v-if="'carpet' == registerData.serviceCategoryId">
                    <div class="expand-btn">
                        <span class="additional-category-span">Treatments</span>
                    </div>
                    <div class="expand-container">
                        <span class="input">
                            <span class="additional-category-span">Scotguard stain protection</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="76">

                                    <div class="ico"><img src="/images/icons/carpet.png"
                                                          alt="Scotguard stain protection"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 76)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 76)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Pet treatment</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="77">

                                    <div class="ico"><img src="/images/icons/carpet.png" alt="Pet treatment"></div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 77)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 77)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Alergy/pest treatment</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="78">

                                    <div class="ico"><img src="/images/icons/carpet.png" alt="Alergy/pest treatment">
                                    </div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 78)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 78)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span class="input">
                            <span class="additional-category-span">Deodorized treatment</span>

                            <div class="input-action">
                                <div class="counter">
                                    <input type="text" value="0" id="79">

                                    <div class="ico"><img src="/images/icons/carpet.png" alt="Deodorized treatment">
                                    </div>
                                    <div class="plus-minus">
                                        <div class="minus" v-on:click="clickMinus($event, 79)"></div>
                                        <div class="plus" v-on:click="clickPlus($event, 79)"></div>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>
                <!-- SIDEBAR SECTION END -->
            </div>
    </div>
    <!-- mobile cancel and apply filters -->
    <div class="cancel-apply-filters">
        <a class="btn cancel">View changes</a>
    </div>
    <!-- /mobile cancel and apply filters -->

    </div>

    </section>
        <div class="modal" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                    <div class="modal-body">
                        <div class="company-modal-wrapper">
                            <div>
                                <img src="" alt="modal image">

                                <div class="inline-block modal-company-wrapp">
                                    <div class="modal-company-name"></div>
                                    <div class="modal-company-city"></div>
                                </div>
                            </div>
                            <div class="modal-reviews-wrapper">
                                <div class="modal-reviews">

                                </div>
                                <div class="modal-total-review">

                                </div>
                            </div>
                        </div>
                        <div class="content">

                        </div>
                        <div class="modal-pagination">
                            <ul class="pagination">

                            </ul>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>





@endsection


@section('scripts');
<script>
    var companyList = new Vue({
        el: '#companyList',
        cache: false,
        data: {
            invisible: true,
            disabledTime: {
                to: new Date()
            },
            registerData: {
                serviceCategoryId: '{{ $serviceCategoryId }}',
                postCode: '{{ $postCode }}',
                propertyType: '{{@$services['propertyType']}}' || 'flat',
                date: '{{ $serviceDate->format('Y-m-d') }}',
                time: '{{ $serviceTime }}',
                services: '{{ json_encode($registeredServices) }}' || {},
            },
            shake: false,
            companies: [],
            serviceText: '<span>'+'{{@$services['bedrooms']}}' || 1 + ' bedroom</span>,<span>'+'{{@$services['bathrooms']}}' || 1+' bathroom</span>',
            selection: null,
            additionalCategories: {},
            initialized: true,
            selectedOrder: 'price',
            mainServiceCategory: '{{ $serviceCategoryId }}',
            keyCollectionServiceId: 0,
            keyCollection: false,
            noCompanies: false,
            serviceCategoryName: '{{ $serviceCategoryName }}',
            bedroomCount: '{{@$services['bedrooms']}}' || 1,
            bathroomCount: '{{@$services['bathrooms']}}' ||1,
            showTime: false,
            showServices: false,
            showSort: false,
            sorting: {
                price: 'Price',
                reviews: 'Reviews'
            },
            services: {
                tenancy: 'End of Tenancy Cleaning',
                carpet: 'Carpet Cleaning',
                deep: 'Deep Cleaning'
            }
        },
        methods: {
            getCompanies: function() {
                // assigne formatted date
                var data = Object.assign({}, this.registerData, { date: moment(this.registerData.date).format('Y-MM-DD') });
                setTimeout(
                        this.$http.post('/callback/getCompanies', {data: data, order: this.selectedOrder})
                                .then(function(response) {

                                    var data = response.data;
                                    this.companies = JSON.parse(data.companies);
                                    this.noCompanies = this.companies.length == 0;
                                    this.serviceText = data.serviceText;

                                    if(this.initialized == false)
                                        this.shake = true;

                                    this.initialized = false;
                                })
                                .catch(function (e) {
                                    console.log(e);
                                })
                        , 500);

                this.buildSelectedText();
                this.shake = false;
            },
            getAdditionalServices: function(serviceCategoryId) {

                this.$http.post('/callback/getAdditionalServices', {data: serviceCategoryId})
                        .then(function (response) {
                            this.additionalCategories = response.data;
                        })
                        .catch(function (e) {
                            console.log(e);
                        })
            },
            updateTime: function() {
                this.registerData = Object.assign({}, this.registerData, { time: 22 });
            },
            resetBedroomBathroom: function () {
                this.bedroomCount = 1;
                this.bathroomCount = 1;
                this.keyCollectionNA = 1;
                this.keyCollectionSet = 0;
            },
            updateAdditionalServices: function () {
                this.keyCollection = false;
                Vue.delete(this.registerData.services, this.keyCollectionServiceId, 1);

                if (this.mainServiceCategory == 'carpet') {
                    this.serviceCategoryName = 'Carpet Cleaning';
                } else if (this.mainServiceCategory == 'deep') {
                    this.serviceCategoryName = 'Deep Cleaning';
                    this.resetBedroomBathroom();
                } else if (this.mainServiceCategory == 'tenancy') {
                    this.serviceCategoryName = 'End of tenancy cleaning';
                    this.resetBedroomBathroom();
                }
                this.registerData.serviceCategoryId = this.mainServiceCategory;

                this.getAdditionalServices(this.mainServiceCategory);
                this.setSelectedServices(this.mainServiceCategory);
                this.getCompanies();
                if (this.mainServiceCategory == 'tenancy') {
                    Vue.set(this.additionalCategories, 13, 1);
                    Vue.set(this.additionalCategories, 14, 1);
                }
            },
            setSelectedServices: function (serviceCategoryId) {
                if (serviceCategoryId == 'tenancy') {
                    this.registerData.services = {13: this.bedroomCount, 14: this.bathroomCount};
                } else if(serviceCategoryId == 'deep') {
                    this.registerData.services = {13: this.bedroomCount, 14: this.bathroomCount};
                } else if(serviceCategoryId == 'carpet') {
                    this.registerData.services = {3: this.bathroomCount, 1: this.bedroomCount};
                }

                this.postSelectedServices();
            },
            postSelectedServices: function () {

            },
            bookNow: function (company) {
                var data = $.extend({}, this.registerData, {
                    companyId: company.id
                });
                return '/checkout?'+$.param(data);

            },
            companyDetails: function (company) {
                var data = $.extend({}, this.registerData, {
                    companyId: company.id
                });
                return '/company-details/'+company.id+'?'+$.param(data);
            },
            clickPlus: function(event, categoryId) {
                var input = $(event.target).parent().parent().find('input'),
                count = input.val(),
                id = input.attr('id');
                var cat_name = input.parents('span.input').find('span.additional-category-span').text().toLowerCase();

                cat_name = cat_name.trim().replace(/ /g, '-');
                input.val(parseInt(input.val()) + 1);
                input.change();
                window.history.replaceState('', '', this.updateURLParameter(window.location.href, cat_name, input.val()));
                // Set service count
                this.additionalCategories[categoryId]['quantity'] = parseInt(input.val());

                if(count >= 0) {
                    Vue.set(this.registerData.services, id, parseInt(count) + 1);
                    this.getCompanies();
                }
            },
            clickMinus: function(event, categoryId) {
                var input = $(event.target).parent().parent().find('input'),
                count = parseInt(input.val()) - 1;
                id = input.attr('id');
                var cat_name = input.parents('span.input').find('span.additional-category-span').text().toLowerCase();
                cat_name = cat_name.trim().replace(/ /g, '-');
                count = count < 0 ? 0 : count;
                input.val(count);
                input.change();

                // Set service count
                this.additionalCategories[categoryId]['quantity'] = parseInt(input.val());
                if(count > 0) {
                    window.history.replaceState('', '', this.updateURLParameter(window.location.href, cat_name, count));
                    Vue.set(this.registerData.services, id, count);
                    this.getCompanies();
                } else if (count == 0) {
                    window.history.replaceState('', '', this.removeUrlParameter(window.location.href, cat_name) );
                    Vue.delete(this.registerData.services, id, count);
                    this.getCompanies();
                }
            },
            setKeyCollection:function () {
                /*
                 * Id pulled from `services` table hardcoded for now
                 * based on service category (should load dynamically)
                 */
                var keyCollectionId = 98;
                switch(this.mainServiceCategory){
                    case 'tenancy':
                        keyCollectionId = 98;
                        break;
                    case 'carpet':
                        keyCollectionId = 108;
                        break;
                    case 'deep':
                        keyCollectionId = 107;
                        break;
                }

                // Remove previous collection key service id
                Vue.delete(this.registerData.services, this.keyCollectionServiceId, 1);

                if (this.keyCollection) {
                    this.keyCollectionServiceId = keyCollectionId;
                    Vue.set(this.registerData.services, keyCollectionId, 1);
                    this.getCompanies();
                } else {
                    Vue.delete(this.registerData.services, keyCollectionId, 1);
                    this.getCompanies();
                }
            },
            setPlus:function (event, serviceId) {
                var input = $(event.target).parents('div.input').find('span.add_categories').text().toLowerCase();

                if (serviceId == 13) {
                    if ( +this.bedroomCount + 1 > 6){
                        return;
                    }
                    this.bedroomCount++;
                    window.history.replaceState('', '', this.updateURLParameter(window.location.href, input, this.bedroomCount));
                    Vue.set(this.registerData.services, 13, this.bedroomCount);
                } else if (serviceId == 14) {
                    if ( +this.bathroomCount + 1 > 6){
                        return;
                    }
                    this.bathroomCount++;
                    window.history.replaceState('', '', this.updateURLParameter(window.location.href, input, this.bathroomCount));
                    Vue.set(this.registerData.services, 14, this.bathroomCount);
                }

                this.getCompanies();
            },
            setMinus:function (event, serviceId) {
                var input = $(event.target).parents('div.input').find('span.add_categories').text().toLowerCase();

                if (serviceId == 13) {
                    if (this.bedroomCount - 1 <= 0){
                        return;
                    }
                    this.bedroomCount--;
                    window.history.replaceState('', '', this.updateURLParameter(window.location.href, input, this.bedroomCount));
                    if (this.bedroomCount > 0) {
                        Vue.set(this.registerData.services, 13, this.bedroomCount);
                    }  else if (this.bedroomCount == 0) {
                        Vue.delete(this.registerData.services, 13, this.bedroomCount);
                        this.getCompanies();
                    } else {
                        this.bedroomCount = 0;
                    }
                } else if (serviceId == 14) {

                    if (this.bathroomCount - 1 <= 0){
                        return;
                    }
                    this.bathroomCount--;
                    window.history.replaceState('', '', this.updateURLParameter(window.location.href, input, this.bathroomCount));
                    if (this.bathroomCount > 0) {
                        Vue.set(this.registerData.services, 14, this.bathroomCount);
                    }  else if (this.bathroomCount == 0) {
                        Vue.delete(this.registerData.services, 14, this.bathroomCount);
                        this.getCompanies();
                    } else {
                        this.bathroomCount = 0;
                    }
                }
                this.getCompanies();
            },
            buildSelectedText: function () {
                this.$http.post('/callback/getSelectedText', {services: this.registerData.services})
                        .then(function (response) {
//                            console.log(response);
                            this.selectedText = response.data.text;
                        })
                        .catch(function (e) {
                            console.log(e);
                        });
            },
            getGuarantee: function(id) {
                switch (parseInt(id)) {
                    case 1:
                        return '24 hrs';
                    case 2:
                        return '48 hrs';
                    case 3:
                        return '1 week';
                    case 4:
                        return '2 weeks';
                    case 5:
                        return '3 weeks';
                    case 6:
                        return '4 weeks';
                    case 7:
                        return 'None';
                    default:
                        return 'None';

                }
            },
            updateURLParameter: function(url, param, paramVal) {
                var newAdditionalURL = "";
                var tempArray = window.location.href.split("?");
                var baseURL = tempArray[0];
                var additionalURL = tempArray[1];
                var temp = "";
                if (additionalURL) {
                    tempArray = additionalURL.split("&");
                    for (var i = 0; i < tempArray.length; i++) {
                        if (tempArray[i].split('=')[0] != param) {
                            newAdditionalURL += temp + tempArray[i];
                            temp = "&";
                        }
                    }
                }

                var rows_txt = temp + "" + param + "=" + paramVal;
                return baseURL + "?" + newAdditionalURL + rows_txt;
            },
            removeUrlParameter: function (url, parameter) {
                var urlParts = url.split('?');

                if (urlParts.length >= 2) {
                    // Get first part, and remove from array
                    var urlBase = urlParts.shift();

                    // Join it back up
                    var queryString = urlParts.join('?');
                    var prefix = encodeURIComponent(parameter) + '=';
                    var parts = queryString.split(/[&;]/g);
                    // Reverse iteration as may be destructive
                    for (var i = parts.length; i-- > 0;) {
                        // Idiom for string.startsWith
                        if (parts[i].lastIndexOf(prefix, 0) !== -1) {
                            parts.splice(i, 1);
                        }
                    }

                    url = urlBase + '?' + parts.join('&');
                }

                return url;
            },
            setAdditionalServices: function (additionalServices, registeredServices) {
                this.additionalCategories = JSON.parse(additionalServices.replace(/&quot;/g,'"'));
                this.registerData.services = JSON.parse(registeredServices.replace(/&quot;/g,'"'));
            },
            changePropertyType: function() {
                window.history.replaceState('', '', this.updateURLParameter(window.location.href, 'propertyType', event.target.value));
                this.getCompanies();
            }

        },
        computed: {
            times: function() {
                var out = {};
                for(var i = 8; i <= 17; i++) {
                    // creates format 08:00 - 09:00
                    out[i] = ('0' + i).substr(-2) + ':00 - ' + ('0' + (i + 1)).substr(-2) + ':00'
                }
                return out;
            }
        },
        mounted: function() {
            this.setAdditionalServices('{{json_encode($defaultAdditionalService)}}', '{{ json_encode($registeredServices) }}' );
            this.getCompanies();
            // Show data when is ready
            $("#companyList").show();
        }
    });

    var defered = $.Deferred();
    var moretext = "More";
    var lesstext = "Less";

    var showChar = 300;  // How many characters are shown by default
    var ellipsestext = "...";

    function getReviews(id, page, callback){
        $.ajax({
            url: '/ajax/reviews/'+id+'?page=' + page
        }).done(function(data){
            $('.content').html(data);
            calculateMoreText();
            if(callback){
                $("#myModal").modal("show");
            }
        });
    };

    function calculateMoreText(){
        $('.more-review').each(function() {
            var content = $(this).html();

            if(content.length > showChar) {
                var c = content.substr(0, showChar);
                var h = content.substr(showChar, content.length - showChar);
                var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
                $(this).html(html);
            }

        });
    };

    $(document).ready(function() {
        $("#company a").click(function(e) {
            e.preventDefault()
        });

        $(document).on("click", ".morelink", function(){
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });

        var url = {!! json_encode(url("/")) !!}

        $(document).on("click", ".rating-modal", function (e) {
            var _this = this;
            var id = $(_this).data("id");

            $.ajax( {
              method: "get",
              url: "/reviews/company/info/"+ id,
              data: { id: id }
            } )
              .done(function(res) {
                  res = JSON.parse(res);
                  $(".company-modal-wrapper").find("img").attr("src", "/img/logos/"+res.logo)
                  $(".modal-company-name").text(res.name);
                  $(".modal-company-city").html(res.city+", UK");
                  $(".modal-reviews").html(res.avg);
                  $(".modal-total-review").html("("+res.count+")");
                  var pag = res.paginate;
                  $(".modal-pagination .pagination").empty();
                  $(".modal-pagination .pagination").append(
                          "<li class='prev' data-id="+id+"><a class='pointer-disabled' href='"+url+"/reviews/company/info/"+id+"?page=1'>«</a></li>"
                  );
                 for(var i=1; i<(pag.last_page + 1);i++){
                     $(".modal-pagination .pagination").append(
                             "<li data-id="+id+" "+ (i == 1 ? " class='active'" : "" )+"><a href='"+url+"/reviews/company/info/"+id+"?page="+i+"'>"+i+"</a></li>"
                     );
                 };
                  $(".modal-pagination .pagination").append(
                          "<li class='next' data-id="+id+"><a href='"+pag.next_page_url+"'>»</a></li>"
                  );

                  getReviews(id, 1, true);
                  if(pag.last_page > 1){
                    $(".modal-pagination .pagination").css("display", "inline-block");
                  }
              })
              .fail(function() {
              })
              .always(function() {
              });
        });

        $(document).on('click','.pagination a', function(e){
            e.preventDefault();
            var count_a = $('.pagination a').length - 2;
            var company_id = $(this).parent().data("id");
            $('.pagination li').removeClass('active');
            $(this).parent().not(".prev,.next").addClass("active");

            var page = $(this).attr('href').split('page=')[1];

            if(page != 1){
                if(page == count_a){
                    $('li.next a').addClass("pointer-disabled");
                } else {
                    $('li.next a').removeClass("pointer-disabled");
                }
                $('li.prev a').removeClass("pointer-disabled");

            } else {
                $('li.next a').removeClass("pointer-disabled");
                $('li.prev a').addClass("pointer-disabled");
            }

            var _this_parent = $(this).parent();
            var li_selected = $('.pagination li')[page];
            if($(_this_parent).hasClass('next')){
                $(li_selected).addClass('active');
            } else if ($(_this_parent).hasClass('prev')) {
                $(li_selected).addClass('active');

            }

            $('li.prev a').attr("href", url +"/reviews/company/info/" + company_id + "?page="+ (page-1) );
            $('li.next a').attr("href", url +"/reviews/company/info/" + company_id + "?page="+ (+page+1) );
            getReviews(company_id, page);
        });

    });
</script>
@endsection
