@extends('reservation.layout.layout')
@section('content')
    {{--<link rel='stylesheet' id='ytplayer-css-css'  href='{{ asset('css/main.css') }}' type='text/css' media='all' />--}}
  <style>
   .select-styled{position: relative;    border: 1px solid #E8E8E8;}
   section.intro .main-form input {border-color:#E8E8E8;}
   .select-options {border-bottom: 1px solid #E8E8E8;border-left: 1px solid #E8E8E8;border-right: 1px solid #E8E8E8;}
input[type=text] {
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}

.vdp-datepicker__calendar .cell.selected, .vdp-datepicker__calendar .cell.selected.highlighted, .vdp-datepicker__calendar .cell.selected:hover{background: #F7038E;    color: white;}

.vdp-datepicker__calendar .cell:not(.blank):not(.disabled).day:hover, .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).month:hover, .vdp-datepicker__calendar .cell:not(.blank):not(.disabled).year:hover{border: 1px solid #F7038E;}

</style>
    <div id="app">
        <section class="intro">
            <div class="container" id="search">
                <h1>We compare cleaning services to bring you the best price</h1>

                @if(Session::has('status'))
                    <div class="alert alert-danger">
                        <strong>{{ Session::get('status') }}</strong>
                    </div>
                @endif
                <search @if( @$session_category ):session_category="{{json_encode($session_category)}}"
                        :session_date="{{json_encode($session_date)}}"
                        :session_time="{{json_encode($session_time)}}"
                        :session_postcode="{{json_encode($session_postcode)}}"
                        @endif
                ></search>

            </div>

   </section>
        

        <section class="about">
            <div class="container pink">
                <h3>Compare. Book. Relax</h3>

                <div class="row">
                    <div class="column third">
                        <div class="ico-holder"><img src="/images/compare.svg" alt=""></div>
                        <h4>Compare</h4>
                        <p>Simply pick the service you would like to compare, enter your date, time and location and click go.</p>
                    </div>
                    <div class="column third">
                        <div class="ico-holder"><img src="/images/book.svg" alt=""></div>
                        <h4>Book</h4>
                        <p>Select from 20+ companies to book the service that’s perfect for you; compare, book, save – just like that.</p>
                    </div>
                    <div class="column third">
                        <div class="ico-holder"><img src="/images/relax.svg" alt=""></div>
                        <h4>Relax</h4>
                        <p>Kick back and relax! With everything sorted in under 5 minutes, make yourself a cuppa – you deserve it.</p>
                    </div>
                </div>

            </div>

            <div class="blue-bg">
                <div class="container pink">
                    <h3>Why Service Octopus?</h3>

                    <div class="row">
                        <div class="column third">
                            <div class="ico-holder"><img src="/images/quick.svg" alt=""></div>
                            <h4>Quick &amp; Convenient</h4>
                            <p>The days of having to call three different companies for a quote are long gone. Compare 20+ companies and book instantly.</p>
                        </div>
                        <div class="column third">
                            <div class="ico-holder"><img src="/images/always-available.svg" alt=""></div>
                            <h4>Always Available</h4>
                            <p>We’re open 24/7. Compare quotes and book your preferred company whenever it’s convenient for you.</p>
                        </div>
                        <div class="column third">
                            <div class="ico-holder"><img src="/images/trustworthy.svg" alt=""></div>
                            <h4>Trustworthy</h4>
                            <p>We use SSL encryption which protects your details sent over the internet from third-party access and manipulation.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


@section('footer')
            <script>
                $(function(){
                    $('#menu').slicknav({
                        //prependTo:'.header nav',
                        prependTo:'.header .mobile-menu',
                        label:""
                    });
                    //var height = $(window).height();
                    //$('.slicknav_nav').css({'max-height': height, 'overflow-y':'scroll'});
                });
            </script>
@endsection
