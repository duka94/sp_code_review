@extends('reservation.layout.layout')

@section('css')
    <style>
        .sidebar {
            width: 25%;
        }
        .main-content {
            width: 73%;
            margin-top: -78px;
        }
        .review .reviewer .name.verified:after {
            margin-left: 9px;
        }
        .main-content h3 {
            padding-bottom: 0px;
        }
        hr {
            margin-top: 0px;
        }
        .sidebar-section.price-section {
            border-bottom: 1px solid #DEDEDE;
        }

    </style>
@endsection

@section('content')
    <div id="service-octopus" class="about-page">
        <!--ABOUT SECTION START -->
        <section class="top-bar">
            <div class="container">
                <h2><span>About {{ $company->name }}</span></h2>
            </div>
        </section>
        <!--ABOUT SECTION END -->
        <section style="">
            <div class="container flex" id="app">
                <!-- MAIN CONTENT -->
                <div class="main-content">
                    <!-- LEFT SIDEBAR -->
                    <div class="sidebar-nav">
                        <nav id="sidebar-nav">
                            <ul id="side-nav">
                                <li class="active"><a href="#about">About</a></li>
                                <li><a href="#services">Services</a></li>
                                <li><a href="#reviews">Reviews</a></li>
                                <li><a href="#faq">FAQs</a></li>
                            </ul>
                        </nav>
                    </div>
                    <!-- LEFT SIDEBAR -->
                    <section id="about">
                        <h3>About</h3>
                        <hr>
                        <p>{{ $companyData->description }}</p>
                        @if( !empty($companyData->youtube) )
                            <p>
                                <iframe width="420" height="315"
                                        src="{{$companyData->youtube}}" frameborder="0" allowfullscreen>
                                </iframe>
                            </p>
                        @endif
                    </section>

                    <section id="services">
                        <h3>Services</h3>
                        <hr>
                        <div class="about-services-box flex">
                            @if (in_array(1, $mainServices))
                                <div class="service">
                                    <div class="ico"><img src="/images/services-icons/tenancy-cleaning.svg" alt="End of Tenancy Cleaning"></div>
                                    <h4>End of<br>Tenancy<br>Cleaning</h4>
                                </div>
                            @endif

                            @if (in_array(4, $mainServices))
                                <div class="service">
                                    <div class="ico"><img src="/images/services-icons/carpet-cleaning.svg" alt="Capet Cleaning"></div>
                                    <h4>Carpet<br>Cleaning</h4>
                                </div>
                            @endif

                            @if (in_array(17, $mainServices))
                                <div class="service">
                                    <div class="ico"><img src="/images/services-icons/deep-cleaning.svg" alt="Deep Cleaning"></div>
                                    <h4>Deep<br>Cleaning</h4>
                                </div>
                            @endif
                        </div>
                    </section>
                    <!-- SECTION REVIEWS -->

                    <section id="reviews">
                        {{--{{dd( $averageRating )}}--}}
                        <h3>Reviews ({{ $reviewsCount }})
                            @if ($reviewsCount > 0)
                                <div class="review-rating">
                                    @for ($i = 1; $i < $averageRating; $i++)
                                        <div class="star"></div>
                                    @endfor
                                    @if ($i - $averageRating == 0.5)
                                        <div class="star half"></div>
                                    @else
                                        <div class="star"></div>
                                    @endif
                                    @if(5 - $averageRating >= 1)
                                        @for($i=1; $i <= 5 - $averageRating; $i++)
                                            <div class="star star-grey"></div>
                                        @endfor
                                    @endif
                                </div>
                            @endif
                        </h3>
                        <hr>
                        @if ($reviewsCount == 0)
                            <div><p>There are no reviews yet.</p></div>
                        @endif
                        <div class="content">
                            @foreach ($reviews as $review)
                                <div class="review">
                                    <div class="review-rating">
                                        @for($i=1; $i<$review->rating; $i++)
                                            <div class="star"></div>
                                        @endfor
                                        @if ($i - $review->rating == 0.5)
                                            <div class="star half"></div>
                                        @else
                                            <div class="star"></div>
                                        @endif
                                        @if(5 - $review->rating >= 1)
                                            @for($i=1; $i <= 5 - $review->rating; $i++)
                                                <div class="star star-grey"></div>
                                            @endfor
                                        @endif
                                    </div>
                                    <span class="more-review">{{ $review->comment }}</span>

                                    <div class="reviewer">
                                        <div class="name verified">
                                            @if($review->name != 'No Name')
                                                {{$review->name}}
                                            @else
                                                VERIFIED CUSTOMER
                                            @endif
                                        </div>
                                        <div class="date">{{ $review->created_at }}</div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        {{$reviews->links()}}
                    </section>

                    <!-- /SECTION REVIEWS -->
                    <!-- SECTION FAQ -->
                    <section id="faq">
                        <h3>FAQs</h3>
                        <hr>
                        <div class="faq-container">
                            <div class="tab">
                                <ul class="tabs">
                                    <li><a href="javascript:void(0);">Bookings and Payment</a></li>
                                    <li><a href="javascript:void(0);">About Services</a></li>
                                    <!--   <li><a href="javascript:void(0);">Extras</a></li>
                                       <li><a href="javascript:void(0);">Appointment Details</a></li>
                                       <li><a href="javascript:void(0);">Complaints</a></li>-->
                                </ul> <!-- / tabs -->

                                <div class="tab_content">

                                    <div class="tabs_item">
                                        <section>
                                            <h4>What is a booking fee?</h4>
                                            <p>Pay a booking fee today to secure your timeslot. The fee is deducted from the total cleaning cost and helps us run our secure platform.</p>
                                        </section>
                                        <section>
                                            <h4>Is your site secure?</h4>
                                            <p>We use SSL encryption which protects your details sent over the internet from third-party access and manipulation. </p>
                                        </section>
                                        <section>
                                            <h4>How do I amend my booking </h4>
                                            <p>It's easy, just call the company you booked with using the details in your booking confirmation email.</p>
                                        </section>


                                    </div> <!-- / tabs_item -->

                                    <div class="tabs_item">
                                        <section>
                                            <h4> Who provides materials, and are they safe? </h4>
                                            <p>Prices quotes include the use of the service companies equipment and materials. </p>
                                        </section>
                                        <section>
                                            <h4>The cleaning company hasn't arrived?</h4>
                                            <p>We would advice contacting the company directly in the first instance. If you are still having difficulty please contact us on 020 3880 7700.</p>
                                        </section>
                                        <section>
                                            <h4>Do I have to be present during the service?</h4>
                                            <p>No, we would advise vacating your property while the service is taking place.</p>
                                        </section>
                                    </div> <!-- / tabs_item -->

                                    <div class="tabs_item">
                                        <section>
                                            <h4>Test 3.</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus inventore nisi dolores. Illum ipsum nostrum at possimus laboriosam odit quia, ea, quae excepturi reprehenderit laudantium amet voluptate obcaecati magni a debitis, cupiditate optio enim quos iusto ducimus? Itaque eveniet exercitationem ratione recusandae assumenda illo quas, soluta dolor consequatur, eum facere.</p>
                                        </section>
                                        <section>
                                            <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis delectus, magni incidunt.</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus inventore nisi dolores. Illum ipsum nostrum at possimus laboriosam odit quia, ea, quae excepturi reprehenderit laudantium amet voluptate obcaecati magni a debitis, cupiditate optio enim quos iusto ducimus? Itaque eveniet exercitationem ratione recusandae assumenda illo quas, soluta dolor consequatur, eum facere.</p>
                                        </section>
                                        <section>
                                            <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis delectus, magni incidunt.</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus inventore nisi dolores. Illum ipsum nostrum at possimus laboriosam odit quia, ea, quae excepturi reprehenderit laudantium amet voluptate obcaecati magni a debitis, cupiditate optio enim quos iusto ducimus? Itaque eveniet exercitationem ratione recusandae assumenda illo quas, soluta dolor consequatur, eum facere.</p>
                                        </section>
                                    </div> <!-- / tabs_item -->
                                    <div class="tabs_item">
                                        <section>
                                            <h4>Test 4</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus inventore nisi dolores. Illum ipsum nostrum at possimus laboriosam odit quia, ea, quae excepturi reprehenderit laudantium amet voluptate obcaecati magni a debitis, cupiditate optio enim quos iusto ducimus? Itaque eveniet exercitationem ratione recusandae assumenda illo quas, soluta dolor consequatur, eum facere.</p>
                                        </section>
                                        <section>
                                            <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis delectus, magni incidunt.</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus inventore nisi dolores. Illum ipsum nostrum at possimus laboriosam odit quia, ea, quae excepturi reprehenderit laudantium amet voluptate obcaecati magni a debitis, cupiditate optio enim quos iusto ducimus? Itaque eveniet exercitationem ratione recusandae assumenda illo quas, soluta dolor consequatur, eum facere.</p>
                                        </section>
                                        <section>
                                            <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis delectus, magni incidunt.</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus inventore nisi dolores. Illum ipsum nostrum at possimus laboriosam odit quia, ea, quae excepturi reprehenderit laudantium amet voluptate obcaecati magni a debitis, cupiditate optio enim quos iusto ducimus? Itaque eveniet exercitationem ratione recusandae assumenda illo quas, soluta dolor consequatur, eum facere.</p>
                                        </section>
                                    </div> <!-- / tabs_item -->
                                    <div class="tabs_item">
                                        <section>
                                            <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis delectus, magni incidunt.</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus inventore nisi dolores. Illum ipsum nostrum at possimus laboriosam odit quia, ea, quae excepturi reprehenderit laudantium amet voluptate obcaecati magni a debitis, cupiditate optio enim quos iusto ducimus? Itaque eveniet exercitationem ratione recusandae assumenda illo quas, soluta dolor consequatur, eum facere.</p>
                                        </section>
                                    </div>


                                    <!-- / tabs_item -->
                                </div> <!-- / tab_content -->
                            </div> <!-- / tab -->
                        </div> <!-- faq-container -->
                    </section>
                    <!-- /SECTION FAQ -->
                </div>
                <!-- /MAIN CONTENT -->

                <!-- SIDEBAR -->
                <div class="sidebar">
                    <div class="sidebar-inner">
                        <!-- SIDEBAR SECTION  FORM FIELDS-->
                        <div class="sidebar-section">
                            <div class="top-sidebar-quote">
                                <p>Quote</p>
                                <div class="price">
                                    &pound;{{ $price->getCleaningPrice() }}.<span>00</span>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-section">



                            <div class="checkout-info-fields">
                                <div class="field">
                                    <div class="column full services">
                                        @if (request('serviceCategoryId') === 'tenancy')
                                            End of Tenancy Cleaning
                                        @elseif (request('serviceCategoryId') === 'deep')
                                            Deep Cleaning
                                        @elseif (request('serviceCategoryId') === 'carpet')
                                            Carpet Cleaning
                                        @endif
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="column full date">
                                        {{ \Carbon\Carbon::parse(request('date'))->toFormattedDateString() }}
                                    </div>
                                </div>
                                <!--    <div class="field">
                                    <div class="column full time">
                                        {{ request('time') }}:00
                                    </div>
                                </div>-->
                                <!--   <div class="field">
                                    <div class="column full zip-code">
                                        {{ request('postCode') }}
                                        </div>
                                    </div>-->
                            </div>
                        </div>
                        <!-- SIDEBAR SECTION -->
                        <!-- SIDEBAR SECTION LIST-->
                        <div class="sidebar-section">
                            <ul class="service-list default-skin scrollable">
                                @foreach ($services as $service)
                                    <li><div class="ico"><img src="/images/icons/{{ $service->icon }}.png" alt="{{ $service->name }}"></div>{{ $selectedServices[$service->id] }} {{ $service->name }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- SIDEBAR SECTION -->
                        <!-- SIDEBAR SECTION PRICE-->


                        <div class="price-paynow-mobile">

                            <!-- SIDEBAR SECTION PRICE-->
                            <div class="sidebar-section price-section">
                                <div class="column full">


                                    <!--      <div class="price-box">
                              <label style="display: inline;float: left;width: auto;top: 11px;margin-right: 10px;"
                              >Total:</label>
                                       <span class="price">
                                           &pound;{{ $price->getCleaningPrice() }}.<span>00</span>
                                       </span>

                          </div> -->

                                    <p class="info">
                                        Due on {{ \Carbon\Carbon::parse(request('date'))->toFormattedDateString() }}: £{{ $duePrice }}
                                    </p>

                                </div>
                            </div>
                            <!-- SIDEBAR SECTION -->

                            <!-- SIDEBAR SECTION PAY NOW-->
                            <div class="sidebar-section pay">
                                <div class="column-full">


                                    <a class="btn" href="{{ route('checkout', request()->all()) }}">Book now (&pound;<span>{{ $price->getBookingFee() }}</span>)<span class="ico"></span></a>


                                    <div class="info" style="margin-top:15px;">
                                        <p>Pay a booking fee today to secure your timeslot. The fee is deducted from the total cleaning cost and helps us run our secure platform and provide customer support.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- SIDEBAR SECTION -->
                        </div>



                        <!-- SIDEBAR SECTION -->
                        <!-- SIDEBAR -->
                    </div></div>
            </div>
        </section>
        @endsection



        @section('scripts')

            <script src="{{ asset('js/custom-scrollbar.js') }}"></script>

            <script>

                var company_id = '{!! json_encode($company->id) !!}';
                var moretext = "More";
                var lesstext = "Less";

                var showChar = 300;  // How many characters are shown by default
                var ellipsestext = "...";

                var url = '{!! json_encode(url('/')) !!}'.replace(/^"|"$/g, '');

                $(document).on('click','.pagination a', function(e){
                    e.preventDefault();
                    var count_a = $('.pagination a').length - 2;

                    $('.pagination li').removeClass('active');
                    $(this).parent().not(".prev,.next").addClass("active");

                    var page = $(this).attr('href').split('page=')[1];

                    if(page != 1){
                        if(page == count_a){
                            $('li.next a').addClass("pointer-disabled");
                        } else {
                            $('li.next a').removeClass("pointer-disabled");
                        }
                        $('li.prev a').removeClass("pointer-disabled");

                    } else {
                        $('li.next a').removeClass("pointer-disabled");
                        $('li.prev a').addClass("pointer-disabled");
                    }

                    var _this_parent = $(this).parent();
                    var li_selected = $('.pagination li')[page];
                    if($(_this_parent).hasClass('next')){
                        $(li_selected).addClass('active');
                    } else if ($(_this_parent).hasClass('prev')) {
                        $(li_selected).addClass('active');

                    }

                    $('li.prev a').attr("href", url +"/company-detail/" + company_id + "?page="+ (page-1) );
                    $('li.next a').attr("href", url +"/company-detail/" + company_id + "?page="+ (+page+1) );
                    getReviews(company_id, page);
                });

                $(document).on("click", ".morelink", function(){
                    if($(this).hasClass("less")) {
                        $(this).removeClass("less");
                        $(this).html(moretext);
                    } else {
                        $(this).addClass("less");
                        $(this).html(lesstext);
                    }
                    $(this).parent().prev().toggle();
                    $(this).prev().toggle();
                    return false;
                });


                function getReviews(id, page){
                    $.ajax({
                        url: '/ajax/reviews/'+id+'?page=' + page
                    }).done(function(data){
                        $('.content').html(data);
                        calculateMoreText();
                        $('html, body').animate({
                            scrollTop: $("#reviews").offset().top
                        }, 1000);
                    });
                };




                function calculateMoreText(){
                    $('.more-review').each(function() {
                        var content = $(this).html();

                        if(content.length > showChar) {

                            var c = content.substr(0, showChar);
                            var h = content.substr(showChar, content.length - showChar);

                            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                            $(this).html(html);
                        }

                    });
                };

                $(document).ready(function() {

                    calculateMoreText();

                    $('.pagination li').first().addClass('prev').removeClass('disabled');
                    $('.pagination li').last().addClass('next');
                    $('li.prev').find('span').replaceWith(function() {
                        return '<a class="pointer-disabled" href="' + url + '/company-detail/' + company_id + '?page=1">«</a>';
                    });
                    $('li.active').find('span').replaceWith(function() {
                        return '<a href="' + url + '/company-detail/' + company_id + '?page=1">1</a>';
                    });
                    $('.sidebar-inner').stick_in_parent();
                    $(".service-list").customScrollbar({
                        hScroll: false
                    });
                    $('ul.pagination').css('display', 'inline-block');
                });

            </script>
@endsection
