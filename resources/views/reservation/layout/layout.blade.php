<!doctype html>
<html>
<head>
    @if(env('APP_ENV', 'local') == 'live')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PMCS6JH');</script>
    <!-- End Google Tag Manager -->
    @endif
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@if( !empty($page_title) ){{$page_title}} @else Compare | Service Octopus @endif</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
    @if(env('APP_ENV', 'local') == 'live')
        <meta name="robots" content="index, follow"/>
    @else
        <meta name="robots" content="noindex, nofollow"/>
    @endif
    <meta name="keywords" content=""/>
    <meta name="description"
          content="Compare cleaning and removal quotes online. Book a manually approved company securely online in under 60s. Save time and money today with Service Octopus."/>
    <meta name="distribution" content="global"/>
    <meta name="googlebot" content="noodp"/>
    <meta name="geo.region" content="GB"/>
    <meta name="geo.placename" content="London"/>
    <meta name="geo.position" content="51.4976890;0.1014670"/>
    <meta name="ICBM" content="51.4976890, 0.1014670"/>

    <meta property="og:url" content="https://www.serviceoctopus.com"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Compare Cleaning Quotes and Book Online - Service Octopus"/>
    <meta property="og:description"
          content="Compare quotes for end of tenancy cleaning, carpet cleaning and deep cleaning in London. Book an approved company online. Save time and money today.n"/>
    <meta property="og:image" content="https://www.serviceoctopus.com/so_octopus_only.png"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <script src="/js/jquery-1.9.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    @yield('css')
</head>
    @if((strpos(Request::path(), 'checkout-confirm') !== false) && (env('APP_ENV', 'local') == 'live'))
        <script> fbq('track', 'Purchase', { value: 39.00, currency: 'GBP' }); </script>
    @endif
<body>
@if(env('APP_ENV', 'local') == 'live')
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PMCS6JH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@endif

@include('layouts.inc.header')

@yield('content')

@include('layouts.inc.footer')

<script src="{{ elixir('js/app.js') }}"></script>

<script src="{{ elixir('js/all.js') }}"></script>
<script src="https://js.stripe.com/v3/"></script>
<script>
    if($(window).width() < 768){
        $('.price-paynow-mobile').appendTo('.mobile-filters');
    }
</script>

@yield('scripts')
<script>

    $('.cmn-toggle-switch').on('click', function(e){
        $(this).toggleClass('active');
        $(this).parents('header').find('.toggle-block').slideToggle();
        e.preventDefault();
    });
    $('.main-nav .menu-toggle').on('click', function(e){
        $(this).closest('li').addClass('active').siblings('.active').removeClass('active');
        $(this).closest('li').siblings('.parent').find('ul').slideUp();
        $(this).parent().siblings('ul').slideToggle();
        e.preventDefault();
    });
    $('.main-nav .menu-toggle-inner').on('click', function(e){
        $(this).closest('li').addClass('active').siblings('.active').removeClass('active');
        $(this).closest('li').siblings('li').find('ul').slideUp();
        $(this).parent().siblings('ul').slideToggle();
        e.preventDefault();
    });
</script>

@yield('footer')

</body>
</html>
<!-- cPanel [11.64] (pro,attracta,vps,vps) Copyright (c) 2016 cPanel, Inc. Licensed on serviceoctopus.servers.prgn.misp.co.uk -->
