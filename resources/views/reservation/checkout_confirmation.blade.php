@extends('reservation.layout.layout')

@section('content')
 


<?php if (url('/') == 'https://www.serviceoctopus.com') { ?>

<script type="text/javascript">
history.pushState(null, null, '<?php echo $_SERVER["REQUEST_URI"]; ?>');
window.addEventListener('popstate', function(event) {
    window.location.assign("{{url('/')}}");
});
</script>

<?php  }?>

<div id="service-octopus" class="thankyou-page">
        <section class="top-bar">
            <div class="container">
                <h2>Your booking has been sent to {{ $company->name }}</h2>
            </div>
        </section>
        <section>
            <div class="container message">
             <h2>What happens next?</h2>
                <p>
                    If you have any questions regarding your booking please contact <strong>{{ $company->name }}</strong> using the contact details in your booking confirmation email.
                    <br>  <br>
                    If you can't see your booking confirmation email in your regular inbox, please check the spam folder; it might be there.
                     <br>  <br>


                     Please note, parking and congestion charging may be applied on the day. For more information please contact {{ $company->name }}. 
                </p>
                
               {{--     <p><strong>The Serviceoctopus.com Team</strong></p> --}}
                
       {{--          <h2>Sharing is caring</h2>
                <p>If you were happy with your experience on Service Octopus please share it on Facebook and help us spread the &#9829;</p>
                 --}}
                <p><div class="fb-share-button" data-href="https://www.serviceoctopus.com" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.serviceoctopus.com%2F&amp;src=sdkpreparse">Share</a></div></P>

             


            </div>
        </section>
@endsection







