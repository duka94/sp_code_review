<!doctype html>
<html class="no-js" lang="en-GB">
   <head>
      
     <?php if (url('/') == 'https://www.serviceoctopus.com') { ?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PMCS6JH');</script>
<!-- End Google Tag Manager -->

    <?php  }?> 
       
       
      <title>Compare End of Tenancy Cleaning Quotes | Serviceoctopus.com</title>
      <meta name="description" content="Compare up to 20 Professional End of Tenancy Cleaning Companies in London. Book online in under 60 seconds. Save time, hassle and money when you book with serviceoctopus.com."/>
      <link rel="canonical" href="https://www.serviceoctopus.com/end-of-tenancy-cleaning/" />
      <meta name="geo.region" content="GB" />
      <meta name="geo.placename" content="London" />
      <meta name="geo.position" content="51.4976890;0.1014670" />
      <meta name="ICBM" content="51.4976890, 0.1014670" />
      <meta charset="UTF-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/><!--320-->
      <meta property="og:url" content="https://www.serviceoctopus.com/" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Compare End of Tenancy Cleaning Quotes - Book Online" />
      <meta property="og:description" content="Compare up to 20 Professional End of Tenancy Cleaning Companies in London. Book online in under 60 seconds." />
      <meta property="og:image" content="https://www.serviceoctopus.com/so_octopus_only.png" />
      <meta property="og:image:width" content="500"/>
      <meta property="og:image:height" content="218"/>
      <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
      <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
      <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
      <link rel="manifest" href="/manifest.json">
      <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
      <meta name="theme-color" content="#ffffff">
      <meta name="robots" content="noodp"/>
      <link rel='stylesheet' id='ytplayer-css-css'  href='{{ asset('css/main.css') }}' type='text/css' media='all' />
      <link rel='stylesheet' id='marketing-fonts-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A400%2C700%2C300&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
      <link href="/css/style.css" rel="stylesheet" type="text/css">
      <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
      <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://serviceoctopus.com/blog/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
      <script type='text/javascript' src='/js/jquery.js'></script>
      <script type='text/javascript' src='/js/jquery-migrate.min.js'></script>
	
      <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1482112013609{background-color: #f0f0f0 !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
      <style>.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}.select-styled{position: relative;border: 1px solid #E8E8E8;}section.intro .main-form input {border-color:#E8E8E8;}.select-options {border-bottom: 1px solid #E8E8E8;;border-left: 1px solid #E8E8E8;border-right: 1px solid #E8E8E8;}</style>

</head>

<body class="page-template-default page page-id-2135 page-parent logged-in  default default-layout theme-default wpb-js-composer js-comp-ver-5.0.1 vc_responsive">
<?php if (url('/') == 'https://www.serviceoctopus.com') { ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PMCS6JH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php  }?>
@include('layouts.inc.header')

{{--<div class="tt-header-margin "></div>--}}


<div id="app">
    <section class="intro">
        <div class="container" id="search">
           <h1 style="padding-top: 0px;">Compare End of Tenancy Cleaning</h1>
            @if(Session::has('status'))
                <div class="alert alert-danger">
                    <strong>{{ Session::get('status') }}</strong>
                </div>
            @endif
            <search :session_category="{{json_encode($search_cat)}}"></search>
	
        </div>
    </section>
</div>




<div id="content-wrapper" class="content no-margin">
  
   <!-- <div  class="empty-space  marg-lg-b10 marg-sm-b10 marg-xs-b10"></div>-->
    <section class="about">
        <div class="container pink">




  <h3>Why Service Octopus?</h3>
                <div class="row2">
                    <div class="column third">
                        <div class="ico-holder"><img src="/img/icons/quick.png" alt=""></div>
                        <h4>Quick &amp; Convenient</h4>
                        <p>The days of having to call three different cleaning companies for an end of tenancy cleaning  quote are over. Compare now!</p></div>
                    <div class="column third">
                        <div class="ico-holder"><img src="/img/icons/always-available.png" alt=""></div>
                        <h4>Always Available</h4>
                        <p>We&#8217;re open 24/7. Compare quotes and book your preferred tenancy cleaning company whenever it&#8217;s convenient for you.</p></div>
                    <div class="column third">
                        <div class="ico-holder"><img src="/img/icons/trustworthy.png" alt=""></div>
                        <h4>Trustworthy</h4>
                        <p>We use SSL encryption which protects your details sent over the internet from third-party access and manipulation.</p></div>
                </div>
        </div>
        <div class="blue-bg">
            <div class="container pink">

            <h3>Book Tenancy Cleaning</h3>
            <div class="row2">
                <div class="column third">
                    <div class="ico-holder"><img src="/img/icons/compare.png" alt=""></div>
                    <h4>Compare Quotes</h4>
                    <p>Compare move out cleaning quotes in an instant. No more unnecessary phone calls and emails!</p></div>
                <div class="column third">
                    <div class="ico-holder"><img src="/img/icons/book.png" alt=""></div>
                    <h4>Book Online</h4>
                    <p>Select from 20+ tenancy cleaning companies to book the service that&#8217;s perfect for you; compare, book, save &#8211; just like that.</p></div>
                <div class="column third">
                    <div class="ico-holder"><img src="/img/icons/relax.png" alt=""></div>
                    <h4>Relax</h4>
                    <p>Kick back and relax! With everything sorted in under 5 minutes, make yourself a cuppa &#8211; you deserve it.</p></div>
            </div>
                          </div>
        </div>
    </section>
    <div id="content-wrapper" class="content no-margin"></div>
    <div class="container">
        <div class="simple-text">
            <div class="row">
                <div class="col-md-12" style="
    padding-left: 0;
    padding-right: 0;
">
                    <!--END PINK BOX HERE--><!--<section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"><div  class="empty-space  marg-lg-b10 marg-sm-b15 marg-xs-b15"></div></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"><div class="text-block " ><div class="simple-text "><p></p>
                                    </div></div></div></div></section>-->

<section  class="section no">
   <div class="row">
      <div class="wpb_column col-md-2 have-padding"></div>
      <div class="wpb_column col-md-8 have-padding" style="padding-left: 0px;padding-right: 0px;">
         <div  class="tt-title">
            <div class="tt-title-cat"></div>
            <h2 class="c-h2"><small>Frequently asked questions, answered.</small></h2>
         </div>
         <div  class="empty-space  marg-lg-b30 marg-sm-b30 marg-xs-b30"></div>
         <div class="tt-accordion">
            <div class="tt-accordion-panel">
               <div class="tt-accordion-title active ">
                  Isn&#039;t it cheaper if I book direct?
                  <div class="tt-accordion-icon"></div>
               </div>
               <div class="tt-accordion-body active ">
                  <div class="simple-text size-3">
                     <div class="text-block " >
                        <div class="simple-text ">
                           <p>Never pay less going direct. We promise you will never pay more by going direct to the professional move out cleaning company. If this is not the case email hello@serviceoctopus.com and let us know</a>. Our objective is to make sure you get the best deal possible.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tt-accordion-panel">
               <div class="tt-accordion-title">
                  What&#039;s included in end of tenancy cleaning?
                  <div class="tt-accordion-icon"></div>
               </div>
               <div class="tt-accordion-body">
                  <div class="simple-text size-3">
                     <div class="text-block " >
                        <div class="simple-text ">
                           <p>We advise cleaning companies to follow the following <a href="/END-OF-TENANCY-CLEANING-CHECKLIST-1.pdf">move out cleaining checklist</a>. If you have any additional requests please mention this on the checkout page of the booking process.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tt-accordion-panel">
               <div class="tt-accordion-title">
                  How do you approve companies?
                  <div class="tt-accordion-icon"></div>
               </div>
               <div class="tt-accordion-body">
                  <div class="simple-text size-3">
                     <div class="text-block " >
                        <div class="simple-text ">
                           <p>We have a strict <b>manual</b> approval process which we apply before a professional cleaning company can provide quotes on our site. Find out <a href="/help/how-do-you-approve-companies/">more about our approval process</a>.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tt-accordion-panel">
               <div class="tt-accordion-title">
                  Is your site secure?
                  <div class="tt-accordion-icon"></div>
               </div>
               <div class="tt-accordion-body">
                  <div class="simple-text size-3">
                     <div class="text-block " >
                        <div class="simple-text ">
                           <p>We use SSL encryption which protects your details sent over the internet from third-party access and manipulation.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div  class="empty-space  marg-lg-b50 marg-md-b70 marg-sm-b50 marg-xs-b50"></div>
         <div class="text-block " >
            <div class="simple-text ">
               <p style="text-align: center">Question still not answered? Visit our <a href="/help/">help section</a>.</p>
            </div>
         </div>
      </div>
      <div class="wpb_column col-md-2 have-padding"></div>
   </div>
   <!-- //close row -->
</section>
<!--<section  class="section stretch_row_only fullwidth  vc_custom_1482112013609">
   <div class="container">
   <div class="row">
   <div class="wpb_column col-md-12 have-padding">
   <section  class="section no">
   <div class="row">
   <div class="wpb_column col-md-2 have-padding"></div>
   <div class="wpb_column col-md-8 have-padding">
   <div  class="tt-title"
   ><div class="tt-title-cat"></div>
   <h2 class="c-h2"><small>Become a subscriber!</small></h2>
   </div>
   <div  class="empty-space  marg-lg-b20">
   </div>
   <div class="text-block " >
   <div class="simple-text size-2"><p style="text-align: center">Service Octopus is shaking the cleaning industry up! Do you run a professional cleaning company? Would you like to join Service Octopus as a subscriber company? Call +4420 3880 7700 today for more info.</p>
   </div>
   </div>
   <div  class="empty-space  marg-lg-b20"></div>
   <div  class="empty-space  marg-lg-b100 marg-sm-b100 marg-xs-b100"></div>
   </div>
   <div class="wpb_column col-md-2 have-padding"></div>
   </div>
   </section>
   </div>
   </div>
   </div>
   </section>-->
<section  class="section no">
   <div class="row">
      <div class="wpb_column col-md-2 align-left have-padding"></div>
      <div class="wpb_column col-md-8 have-padding" style="
         padding-left: 0;
         padding-right: 0;
         ">
         <div  class="tt-title">
            <div class="tt-title-cat"></div>
            <h2 class="c-h2"><small>More about End of tenancy cleaning</small></h2>
         </div>
         <div  class="empty-space  marg-lg-b30 marg-sm-b30 marg-xs-b30"></div>
         <div class="text-block " >
            <div class="simple-text ">
               <p>End of tenancy cleaning or post tenancy clean is done at end of your tenancy when you move out of the property. Most tenancy agreements will require you to carry out an end of tenancy clean before the property is handed back to the estate agent or landlord. To ensure the cleaning is done properly, most professional cleaning companies will have a checklist of what needs to be cleaned. Skirting boards, tops of cabinets, inside cupboards and light switches need to be cleaned thoroughly along with any appliances that come with the property. However, the general rule is that you must leave the property in the condition that it was given to you. Always read the inventory report to check if you do need to get your property professionally cleaned. Click here to read our end of tenancy cleaning guide.</p>
               <p>Many people find that they lose part of their deposit because the property hasn’t been cleaned thoroughly and they don't get the <strong>move out cleaning</strong> done throroughly enough. In fact, most deposit disputes relate to cleaning costs deducted from the deposit. Make sure you always retain a receipt when a professional end of tenancy clean has been done so you can show your landlord or estate agent evidence that cleaning has been taken place. Another tip is to always check the property at the end of the clean to ensure that nothing has been missed. Most companies will offer a re-clean within 48 hours. Taking photos of the property after it has been cleaned should help eliminate any problems should the estate agent or landlord try to deduct money from your deposit for cleaning.</p>
               <p>When choosing a company, it is important to check if they have insurance and that they offer a cleaning guarantee. Most companies will bring their own equipment and chemicals with them to your property, so you do not have to provide them.</p>
            </div>
         </div>
         <div  class="empty-space  marg-lg-b60 marg-sm-b75 marg-xs-b75"></div>
      </div>
      <div class="wpb_column col-md-2 have-padding"></div>
   </div>
</section>
<section  class="section no">
   <div class="row">
      <div class="wpb_column col-md-12 have-padding">
         <div class="text-block " >
            <div class="simple-text "></div>
         </div>
      </div>
   </div>
</section>
</div>
</div>
</div>
</div>
</div>

<!-- FOOTER -->
<footer class="tt-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div id="text-2" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">COMPANY</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                            <li ><a href="/about-us">About us</a></li>
                            <li ><a href="/help">Help</a></li>
                            <li ><a href="/blog">Blog</a></li>
                            <li ><a href="/press">Press</a></li>
                        </ul>
                    </div>
                </div>                <div class="empty-space marg-xs-b30"></div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div id="text-3" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">LEGAL</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                            <li ><a href="/customer-terms-and-conditions">Terms and Conditions</a></li>
                            <li ><a href="/privacy-policy">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>                <div class="empty-space marg-sm-b30"></div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div id="text-4" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">COMPARE</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                            <li ><a href="/end-of-tenancy-cleaning/">End of Tenancy Cleaning</a></li>
                            <li ><a href="/carpet-cleaning">Carpet Cleaning</a></li>
                            <li ><a href="/deep-cleaning">Deep Cleaning</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="empty-space marg-lg-b80 marg-sm-b50 marg-xs-b30"></div>
    </div>
    <div class="tt-footer-copy">
        <div class="container">
            <div class="simple-text size-5 color-4">
                <p>Copyright &copy; 2017 Serviceoctopus.com Limited. All rights reserved.</p>

            </div>
        </div>
    </div>
</footer>

<script type='text/javascript' src='/js/global.js'></script>

<script src="{{ elixir('js/app.js') }}"></script>

</body>
</html>
