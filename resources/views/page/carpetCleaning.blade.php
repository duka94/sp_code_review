<!doctype html>
<html class="no-js" lang="en-GB">

<head>
     <?php if (url('/') == 'https://www.serviceoctopus.com') { ?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PMCS6JH');</script>
<!-- End Google Tag Manager -->

    <?php  }?> 
    <meta name="geo.region" content="GB" />
    <meta name="geo.placename" content="London" />
    <meta name="geo.position" content="51.4976890;0.1014670" />
    <meta name="ICBM" content="51.4976890, 0.1014670" />
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->


    <meta property="og:url"                content="https://www.serviceoctopus.com/" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="Compare Cheap Cleaning Quotes - Book Online" />
    <meta property="og:description"        content="Compare quotes for end of tenancy cleaning, carpet cleaning and builders cleaning in London. Book an approved company online. Save time and money today." />
    <meta property="og:image"              content="https://www.serviceoctopus.com/logo.png" />
    <meta property="og:image:width"        content="500"/>
    <meta property="og:image:height"        content="218"/>


    <title>Compare Carpet Cleaning Quotes | Book Online | Serviceoctopus.com</title>

    <meta name="description" content="Compare quotes from professional and approved carpet cleaning companies instantly with serviceoctopus.com. Save time, hassle and money when you book online."/>
    <meta name="robots" content="noodp"/>
    <link rel="canonical" href="https://www.serviceoctopus.com/carpet-cleaning/" />


    <link rel='stylesheet' id='ytplayer-css-css'  href='https://www.serviceoctopus.com/css/main.css' type='text/css' media='all' />
    <link rel='stylesheet' id='marketing-fonts-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A400%2C700%2C300&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
    <!--<link rel='stylesheet' id='js_composer_front-css'  href='https://www.serviceoctopus.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css' type='text/css' media='all' />-->



    <script type='text/javascript' src='https://www.serviceoctopus.com/js/jquery.js'></script>
    <script type='text/javascript' src='https://www.serviceoctopus.com/js/jquery-migrate.min.js'></script>
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://www.serviceoctopus.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1482112013609{background-color: #f0f0f0 !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
   

    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
    <style>
  
        
      
        .select-styled{
            position: relative;    border: 1px solid #E8E8E8;
        }
        
        section.intro .main-form input {border-color:#E8E8E8;}
        
       .select-options { 
        border-bottom: 1px solid #E8E8E8;;
    border-left: 1px solid #E8E8E8;;
    border-right: 1px solid #E8E8E8;}
        
        
  
        
    </style>


</head>
<body class="page-template-default page page-id-2266 page-parent logged-in  default default-layout theme-default wpb-js-composer js-comp-ver-5.0.1 vc_responsive">
<?php if (url('/') == 'https://www.serviceoctopus.com') { ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PMCS6JH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php  }?>


@include('layouts.inc.header')

<div id="app">
    <section class="intro">
        <div class="container" id="search">
            <h2>Compare Carpet Cleaning London</h2>

            @if(Session::has('status'))
                <div class="alert alert-danger">
                    <strong>{{ Session::get('status') }}</strong>
                </div>
            @endif
            <search :session_category="{{json_encode($search_cat)}}"></search>

        </div>
    </section>
</div>

<div id="content-wrapper" class="content no-margin">
    {{--<div class="container">--}}
        {{--<div class="simple-text">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<section  class="section stretch_row_only fullwidth"><div class="container"><div class="row"><div class="wpb_column col-md-12 have-padding"><div  class="empty-space  marg-lg-b40 marg-md-b60 marg-sm-b60 marg-xs-b60"></div><div class="text-block " ><div class="simple-text "><h1>Compare and Book Carpet Cleaners.</h1>--}}
                                        {{--</div></div><div  class="empty-space  marg-lg-b30 marg-sm-b45 marg-xs-b45"></div><div class="text-block " ><div class="simple-text "><!-- TrustBox widget - Micro Review Count -->--}}
                                            {{--<div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="58d82de80000ff00059f420b" data-style-height="20px" data-style-width="100%" data-theme="light">--}}
                                                {{--<a href="https://uk.trustpilot.com/review/serviceoctopus.com" target="_blank">Trustpilot</a>--}}
                                            {{--</div>--}}
                                            {{--<!-- End TrustBox widget -->--}}
                                        {{--</div></div><div  class="empty-space  marg-lg-b30 marg-sm-b45 marg-xs-b45"></div></div></div></div></section><br />--}}
                    {{--<!--START PINK BOX HERE-->--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <br />
    <div  class="empty-space  marg-lg-b10 marg-sm-b10 marg-xs-b10"></div>
    <section class="about">
        <div class="container pink">
            <h3>Compare. Book. Relax</h3>
            <div class="row2">
                <div class="column third">
                    <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/compare.png" alt=""></div>
                    <h4>Compare</h4>
                    <p>Simply pick the service you would like to compare, enter your date, time and location and click go.</p></div>
                <div class="column third">
                    <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/book.png" alt=""></div>
                    <h4>Book</h4>
                    <p>Select from 20+ companies to book the service that&#8217;s perfect for you; compare, book, save &#8211; just like that.</p></div>
                <div class="column third">
                    <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/relax.png" alt=""></div>
                    <h4>Relax</h4>
                    <p>Kick back and relax! With everything sorted in under 5 minutes, make yourself a cuppa &#8211; you deserve it.</p></div>
            </div>
        </div>
        <div class="blue-bg">
            <div class="container pink">
                <h3>Why Service Octopus?</h3>
                <div class="row2">
                    <div class="column third">
                        <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/quick.png" alt=""></div>
                        <h4>Quick &amp; Convenient</h4>
                        <p>The days of having to call three different companies for a quote are long gone. Compare 20+ companies and book instantly.</p></div>
                    <div class="column third">
                        <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/always-available.png" alt=""></div>
                        <h4>Always Available</h4>
                        <p>We&#8217;re open 24/7. Compare quotes and book your preferred company whenever it&#8217;s convenient for you.</p></div>
                    <div class="column third">
                        <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/trustworthy.png" alt=""></div>
                        <h4>Trustworthy</h4>
                        <p>We use SSL encryption which protects your details sent over the internet from third-party access and manipulation.</p></div>
                </div>
            </div>
        </div>
    </section>
    <div id="content-wrapper" class="content no-margin"></div>
    <div class="container">
        <div class="simple-text">
            <div class="row">
                <div class="col-md-12">
                    <!--END PINK BOX HERE--><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"><div  class="empty-space  marg-lg-b10 marg-sm-b15 marg-xs-b15"></div></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"><div class="text-block " ><div class="simple-text "><p></p>
                                    </div></div></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-2 have-padding"></div><div class="wpb_column col-md-8 have-padding"><div  class="empty-space  marg-lg-b50"></div><div  class="tt-title"><div class="tt-title-cat"></div><h2 class="c-h2"><small>Frequently asked questions, answered.</small></h2></div><div  class="empty-space  marg-lg-b30 marg-sm-b30 marg-xs-b30"></div><div class="tt-accordion"><div class="tt-accordion-panel"><div class="tt-accordion-title active ">Isn&#039;t it cheaper if I book direct?<div class="tt-accordion-icon"></div></div><div class="tt-accordion-body active "><div class="simple-text size-3"><div class="text-block " ><div class="simple-text "><p>Booking with Service Octopus should always be the same price (if not cheaper) than booking the service directly. If this is not the case <a href="https://www.serviceoctopus.com/help/email-us/">get in touch and let us know</a>. Our objective is to make sure you get the best deal possible.</p>
                                                    </div></div></div></div></div><div class="tt-accordion-panel"><div class="tt-accordion-title">How do you approve companies?<div class="tt-accordion-icon"></div></div><div class="tt-accordion-body"><div class="simple-text size-3"><div class="text-block " ><div class="simple-text "><p>We have a strict <b>manual</b> approval process which we apply before a company can provide a quote on our site. Find out <a href="https://www.serviceoctopus.com/approval-process/">more about our approval process</a>.</p>
                                                    </div></div></div></div></div><div class="tt-accordion-panel"><div class="tt-accordion-title">Is your site secure?<div class="tt-accordion-icon"></div></div><div class="tt-accordion-body"><div class="simple-text size-3"><div class="text-block " ><div class="simple-text "><p>We use SSL encryption which protects your details sent over the internet from third-party access and manipulation.</p>
                                                    </div></div></div></div></div></div><div  class="empty-space  marg-lg-b50 marg-md-b70 marg-sm-b50 marg-xs-b50"></div><div class="text-block " ><div class="simple-text "><p style="text-align: center">Question still not answered? Visit our <a href="https://www.serviceoctopus.com/help/">help section</a>.</p>
                                    </div></div><div  class="empty-space  marg-lg-b75 marg-md-b70 marg-sm-b90 marg-xs-b90"></div></div><div class="wpb_column col-md-2 have-padding"></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"></div></div></section><section  class="section stretch_row_only fullwidth  vc_custom_1482112013609"><div class="container"><div class="row"><div class="wpb_column col-md-12 have-padding"><section  class="section no"><div class="row"><div class="wpb_column col-md-2 have-padding"></div><div class="wpb_column col-md-8 have-padding"><div  class="empty-space  marg-lg-b40 marg-sm-b40 marg-xs-b40"></div><div  class="tt-title"><div class="tt-title-cat"></div><h2 class="c-h2"><small>Become a subscriber!</small></h2></div><div  class="empty-space  marg-lg-b20"></div><div class="text-block " ><div class="simple-text size-2"><p style="text-align: center">Service Octopus is shaking the cleaning industry up! Do you run a professional cleaning company? Would you like to join Service Octopus as a subscriber company? Find out more today.</p>
                                                    </div></div><div  class="empty-space  marg-lg-b20"></div><div class="text-center"><a  class="c-btn type-1 size-2 color-1" target="_self" title="button" href="https://www.serviceoctopus.com/subscribe/"><span>Learn More</span></a></div><div  class="empty-space  marg-lg-b100 marg-sm-b100 marg-xs-b100"></div></div><div class="wpb_column col-md-2 have-padding"></div></div></section></div></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-2 align-left have-padding"></div><div class="wpb_column col-md-8 have-padding"><div  class="empty-space  marg-lg-b60 marg-sm-b45 marg-xs-b45"></div><div  class="tt-title"><div class="tt-title-cat"></div><h2 class="c-h2"><small>More about Carpet Cleaning</small></h2></div><div  class="empty-space  marg-lg-b30 marg-sm-b30 marg-xs-b30"></div><div class="text-block " ><div class="simple-text "><p><img class="aligncenter wp-image-2947 size-full" title="Man Cleaning a Carpet - Compare carpet cleaning quote with Service Octopus" src="https://www.serviceoctopus.com/img/compare-carpet-cleaning-quotes-service-octopus.jpg" alt="Man Cleaning a Carpet - Compare carpet cleaning quote with Service Octopus" width="700" height="217" /></p>
                                        <p>Service Octopus is a price comparison and booking platform which compares manually approved local cleaning companies, giving you the cheapest quote for <strong>Carpet Cleaning London</strong>.</p>
                                        <p>A professional carpet clean can be offered as a stand-alone service or an add-on to an end of tenancy clean or deep clean. There are two ways in which a carpet can be cleaned: steam cleaning (or water extraction) or shampooing. Both methods are commonly used in a professional carpet clean though it may have varying results. Although most companies will do their best to get stains and marks off your carpet, older stains are harder to remove. This is why you should always treat stains or marks on your carpets as soon as possible.</p>
                                        <p>As mentioned above, there are two ways to get your carpets professionally cleaned. Steam cleaning or hot water extraction is done via a machine. This machine blasts steam or hot water through the carpet to dislodge dirt. The machine then vacuums the dirty water removing marks and stains. With shampooing, a thick carpet cleaning foam is applied to the carpet and then manually scrubbed to get the dirt or stain off. A sticky residue is often the by-product left on the carpet so you will need to let it dry thoroughly to avoid getting any new dirt trapped on it.</p>
                                        <p>The price of carpet cleaning is dependent on the size of your carpet. You will also need to let the company know what kind of fabric or material your carpet is made of so they can use the correct chemicals to treat your carpet. As additional services you can add on, some companies offer carpet protection or pet/allergy treatments to your carpets.</p>
                                        <p>You should always aim to get your carpets cleaned at least once every 12 months. This is to ensure that you get germs and bacteria out of the carpet and to ensure that your indoor air quality is free of dusts. If you have allergies then a frequent carpet cleaning regime is suggested.</p>
                                    </div></div><div  class="empty-space  marg-lg-b60 marg-sm-b75 marg-xs-b75"></div></div><div class="wpb_column col-md-2 have-padding"></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"><div class="text-block " ><div class="simple-text "><!--<div class="dropdown">
<p><button class="dropbtn">Areas we cover</button></p>
<div id="myDropdown" class="dropdown-content"><a href="https://www.serviceoctopus.com/carpet-cleaning/Acton">Acton</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Balham">Balham</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Barbican">Barbican</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Battersea">Battersea</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Belgravia">Belgravia</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Bethnal-Green">Bethnal Green</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Brixton">Brixton</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Bromley">Bromley</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Camden">Camden</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Canary-Wharf">Canary Wharf</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Chelsea">Chelsea</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Chiswick">Chiswick</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Clapham">Clapham</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Crouch-End">Crouch End</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Croydon">Croydon</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Crystal-Palace">Crystal Palace</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Dalston">Dalston</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Dartford">Dartford</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Ealing">Ealing</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Earls-Court">Earls Court</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Elephant-and-Castle">Elephant and Castle</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Finchley">Finchley</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Finsbury-Park">Finsbury Park</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Fulham">Fulham</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Greenwich">Greenwich</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Hackney">Hackney</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Hammersmith">Hammersmith</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Hampstead">Hampstead</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Harrow">Harrow</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Illford">Illford</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Islington">Islington</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Kensington">Kensington</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Kilburn">Kilburn</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Liverpool-Street">Liverpool Street</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Maida-Vale">Maida Vale</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Mayfair">Mayfair</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Notting-Hill">Notting Hill </a><a href="https://www.serviceoctopus.com/carpet-cleaning/Paddington">Paddington</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Peckham">Peckham</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Putney">Putney</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Queens-Park">Queens Park</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Romford">Romford</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Shoreditch">Shoreditch</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Soho">Soho</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Southall">Southall </a><a href="https://www.serviceoctopus.com/carpet-cleaning/Southwark">Southwark</a><a href="https://www.serviceoctopus.com/carpet-cleaning/St-Johns-Wood">St Johns Wood</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Stoke-Newington">Stoke Newington</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Stratford">Stratford</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Streatham">Streatham</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Sutton">Sutton</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Tooting">Tooting</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Walthamstow">Walthamstow</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Wandsworth">Wandsworth</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Westminster">Westminster</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Wimbledon">Wimbledon</a></div>
</div>-->
                                    </div></div></div></div></section>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- FOOTER -->
<footer class="tt-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div id="text-2" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">COMPANY</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                            <li ><a href="/about-us">About us</a></li>
                            <li ><a href="/help">Help</a></li>
                            <li ><a href="/blog">Blog</a></li>
                            <li ><a href="/press">Press</a></li>
                        </ul>
                    </div>
                </div>                <div class="empty-space marg-xs-b30"></div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div id="text-3" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">LEGAL</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                            <li ><a href="/customer-terms-and-conditions">Terms and Conditions</a></li>
                            <li ><a href="/privacy-policy">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>                <div class="empty-space marg-sm-b30"></div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div id="text-4" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">COMPARE</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                            <li ><a href="/end-of-tenancy-cleaning/">End of Tenancy Cleaning</a></li>
                            <li ><a href="/carpet-cleaning">Carpet Cleaning</a></li>
                            <li ><a href="/deep-cleaning">Deep Cleaning</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="empty-space marg-lg-b80 marg-sm-b50 marg-xs-b30"></div>
    </div>
    <div class="tt-footer-copy">
        <div class="container">
            <div class="simple-text size-5 color-4">
                <p>Copyright &copy; 2017 Serviceoctopus.com Limited. All rights reserved.</p>

            </div>
        </div>
    </div>
</footer>



<script type='text/javascript' src='https://www.serviceoctopus.com/js/global.js'></script>


<script src="{{ elixir('js/app.js') }}"></script>


</body>
</html>
