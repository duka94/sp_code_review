@extends('reservation.layout.layout')

@section('content')
    <link rel='stylesheet' id='ytplayer-css-css'  href='/css/main.css' type='text/css' media='all' />
    <div id="content-wrapper" class="content no-margin">
        <div class="container">
            <div class="simple-text">
                <div class="row">
                    <div class="col-md-12">
                        <section class="intro">
                            <div class="container"><h2>Compare and Book Carpet Cleaners.</h2></div>

                            <div class="main-form">
                                <form action="{{ route('compare') }}" method="get">
                                    <div class="input column fourth services">
                                        <select name="serviceCategory" id="service-type">
                                            <option value="hide">Select Service</option>
                                            <option value="tenancy">End of Tenancy Cleaning</option>
                                            <option value="carpet" selected>Carpet Cleaning</option>
                                            <option value="deep">Deep Cleaning</option>
                                        </select>
                                    </div>
                                    <div class="input column fourth date">
                                        <input name="date" type="text" id="datetimepicker" placeholder="Select Date">
                                    </div>
                                    <div class="input column fourth time">
                                        <select name="time" id="service-time">
                                            <option value="8">08:00-09:00</option>
                                            <option value="9">09:00-10:00</option>
                                            <option value="10">10:00-11:00</option>
                                            <option value="11">11:00-12:00</option>
                                            <option value="12">12:00-13:00</option>
                                            <option value="13">13:00-14:00</option>
                                            <option value="14">14:00-15:00</option>
                                            <option value="15">15:00-16:00</option>
                                            <option value="16">16:00-17:00</option>
                                            <option value="17">17:00-18:00</option>
                                        </select>
                                    </div>
                                    <div class="input column fourth zipcode">
                                        <input name="postcode" type="text" placeholder="Enter Postcode" value="">
                                    </div>
                                    <div class="submit">
                                        <input type="submit" value="">
                                        <div class="submit-btn"><img src="/images/search.svg" alt="search"><span>Compare Services</span></div>
                                    </div>
                                </form>
                            </div>
                        </section><br />
                        <!--START PINK BOX HERE-->
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
        <section class="about">
            <div class="container pink">
                <h3>Compare. Book. Relax</h3>
                <div class="row2">
                    <div class="column third">
                        <div class="ico-holder"><img src="/img/icons/compare.png" alt=""></div>
                        <h4>Compare</h4>
                        <p>Simply pick the service you would like to compare, enter your date, time and location and click go.</p></div>
                    <div class="column third">
                        <div class="ico-holder"><img src="/img/icons/book.png" alt=""></div>
                        <h4>Book</h4>
                        <p>Select from 20+ companies to book the service that&#8217;s perfect for you; compare, book, save &#8211; just like that.</p></div>
                    <div class="column third">
                        <div class="ico-holder"><img src="/img/icons/relax.png" alt=""></div>
                        <h4>Relax</h4>
                        <p>Kick back and relax! With everything sorted in under 5 minutes, make yourself a cuppa &#8211; you deserve it.</p></div>
                </div>
            </div>
            <div class="blue-bg">
                <div class="container pink">
                    <h3>Why Service Octopus?</h3>
                    <div class="row2">
                        <div class="column third">
                            <div class="ico-holder"><img src="/img/icons/quick.png" alt=""></div>
                            <h4>Quick &amp; Convenient</h4>
                            <p>The days of having to call three different companies for a quote are long gone. Compare 20+ companies and book instantly.</p></div>
                        <div class="column third">
                            <div class="ico-holder"><img src="/img/icons/always-available.png" alt=""></div>
                            <h4>Always Available</h4>
                            <p>We&#8217;re open 24/7. Compare quotes and book your preferred company whenever it&#8217;s convenient for you.</p></div>
                        <div class="column third">
                            <div class="ico-holder"><img src="/img/icons/trustworthy.png" alt=""></div>
                            <h4>Trustworthy</h4>
                            <p>We use SSL encryption which protects your details sent over the internet from third-party access and manipulation.</p></div>
                    </div>
                </div>
            </div>
        </section>
        </div>
        <div id="content-wrapper" class="content no-margin"></div>
        <div class="container">
            <div class="simple-text">
                <div class="row">
                    <div class="col-md-12">
                        <!--END PINK BOX HERE--><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"><div  class="empty-space  marg-lg-b10 marg-sm-b15 marg-xs-b15"></div></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"><div class="text-block " ><div class="simple-text "><p></p>
                                        </div></div></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-2 have-padding"></div><div class="wpb_column col-md-8 have-padding"><div  class="empty-space  marg-lg-b50"></div><div  class="tt-title"><div class="tt-title-cat"></div><h2 class="c-h2"><small>Frequently asked questions, answered.</small></h2></div><div  class="empty-space  marg-lg-b30 marg-sm-b30 marg-xs-b30"></div><div class="tt-accordion"><div class="tt-accordion-panel"><div class="tt-accordion-title active ">Isn&#039;t it cheaper if I book direct?<div class="tt-accordion-icon"></div></div><div class="tt-accordion-body active "><div class="simple-text size-3"><div class="text-block " ><div class="simple-text "><p>Booking with Service Octopus should always be the same price (if not cheaper) than booking the service directly. If this is not the case <a href="https://www.serviceoctopus.com/help/email-us/">get in touch and let us know</a>. Our objective is to make sure you get the best deal possible.</p>
                                                        </div></div></div></div></div><div class="tt-accordion-panel"><div class="tt-accordion-title">How do you approve companies?<div class="tt-accordion-icon"></div></div><div class="tt-accordion-body"><div class="simple-text size-3"><div class="text-block " ><div class="simple-text "><p>We have a strict <b>manual</b> approval process which we apply before a company can provide a quote on our site. Find out <a href="https://www.serviceoctopus.com/approval-process/">more about our approval process</a>.</p>
                                                        </div></div></div></div></div><div class="tt-accordion-panel"><div class="tt-accordion-title">Is your site secure?<div class="tt-accordion-icon"></div></div><div class="tt-accordion-body"><div class="simple-text size-3"><div class="text-block " ><div class="simple-text "><p>We use SSL encryption which protects your details sent over the internet from third-party access and manipulation.</p>
                                                        </div></div></div></div></div></div><div  class="empty-space  marg-lg-b50 marg-md-b70 marg-sm-b50 marg-xs-b50"></div><div class="text-block " ><div class="simple-text "><p style="text-align: center">Question still not answered? Visit our <a href="https://www.serviceoctopus.com/help/">help section</a>.</p>
                                        </div></div><div  class="empty-space  marg-lg-b75 marg-md-b70 marg-sm-b90 marg-xs-b90"></div></div><div class="wpb_column col-md-2 have-padding"></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"></div></div></section><section  class="section stretch_row_only fullwidth  vc_custom_1482112013609"><div class="container"><div class="row"><div class="wpb_column col-md-12 have-padding"><section  class="section no"><div class="row"><div class="wpb_column col-md-2 have-padding"></div><div class="wpb_column col-md-8 have-padding"><div  class="empty-space  marg-lg-b40 marg-sm-b40 marg-xs-b40"></div><div  class="tt-title"><div class="tt-title-cat"></div><h2 class="c-h2"><small>Become a subscriber!</small></h2></div><div  class="empty-space  marg-lg-b20"></div><div class="text-block " ><div class="simple-text size-2"><p style="text-align: center">Service Octopus is shaking the cleaning industry up! Do you run a professional cleaning company? Would you like to join Service Octopus as a subscriber company? Find out more today.</p>
                                                        </div></div><div  class="empty-space  marg-lg-b20"></div><div class="text-center"><a  class="c-btn type-1 size-2 color-1" target="_self" title="button" href="https://www.serviceoctopus.com/subscribe/"><span>Learn More</span></a></div><div  class="empty-space  marg-lg-b100 marg-sm-b100 marg-xs-b100"></div></div><div class="wpb_column col-md-2 have-padding"></div></div></section></div></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-2 align-left have-padding"></div><div class="wpb_column col-md-8 have-padding"><div  class="empty-space  marg-lg-b60 marg-sm-b45 marg-xs-b45"></div><div  class="tt-title"><div class="tt-title-cat"></div><h2 class="c-h2"><small>More about Carpet Cleaning</small></h2></div><div  class="empty-space  marg-lg-b30 marg-sm-b30 marg-xs-b30"></div><div class="text-block " ><div class="simple-text "><p><img class="aligncenter wp-image-2947 size-full" title="Man Cleaning a Carpet - Compare carpet cleaning quote with Service Octopus" src="/img/compare-carpet-cleaning-quotes-service-octopus.jpg" alt="Man Cleaning a Carpet - Compare carpet cleaning quote with Service Octopus" width="700" height="217" /></p>
                                            <p>Service Octopus is a price comparison and booking platform which compares manually approved local cleaning companies, giving you the cheapest quote for <strong>Carpet Cleaning London</strong>.</p>
                                            <p>A professional carpet clean can be offered as a stand-alone service or an add-on to an end of tenancy clean or deep clean. There are two ways in which a carpet can be cleaned: steam cleaning (or water extraction) or shampooing. Both methods are commonly used in a professional carpet clean though it may have varying results. Although most companies will do their best to get stains and marks off your carpet, older stains are harder to remove. This is why you should always treat stains or marks on your carpets as soon as possible.</p>
                                            <p>As mentioned above, there are two ways to get your carpets professionally cleaned. Steam cleaning or hot water extraction is done via a machine. This machine blasts steam or hot water through the carpet to dislodge dirt. The machine then vacuums the dirty water removing marks and stains. With shampooing, a thick carpet cleaning foam is applied to the carpet and then manually scrubbed to get the dirt or stain off. A sticky residue is often the by-product left on the carpet so you will need to let it dry thoroughly to avoid getting any new dirt trapped on it.</p>
                                            <p>The price of carpet cleaning is dependent on the size of your carpet. You will also need to let the company know what kind of fabric or material your carpet is made of so they can use the correct chemicals to treat your carpet. As additional services you can add on, some companies offer carpet protection or pet/allergy treatments to your carpets.</p>
                                            <p>You should always aim to get your carpets cleaned at least once every 12 months. This is to ensure that you get germs and bacteria out of the carpet and to ensure that your indoor air quality is free of dusts. If you have allergies then a frequent carpet cleaning regime is suggested.</p>
                                        </div></div><div  class="empty-space  marg-lg-b60 marg-sm-b75 marg-xs-b75"></div></div><div class="wpb_column col-md-2 have-padding"></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"><div class="text-block " ><div class="simple-text "><!--<div class="dropdown">
<p><button class="dropbtn">Areas we cover</button></p>
<div id="myDropdown" class="dropdown-content"><a href="https://www.serviceoctopus.com/carpet-cleaning/Acton">Acton</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Balham">Balham</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Barbican">Barbican</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Battersea">Battersea</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Belgravia">Belgravia</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Bethnal-Green">Bethnal Green</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Brixton">Brixton</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Bromley">Bromley</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Camden">Camden</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Canary-Wharf">Canary Wharf</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Chelsea">Chelsea</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Chiswick">Chiswick</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Clapham">Clapham</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Crouch-End">Crouch End</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Croydon">Croydon</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Crystal-Palace">Crystal Palace</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Dalston">Dalston</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Dartford">Dartford</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Ealing">Ealing</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Earls-Court">Earls Court</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Elephant-and-Castle">Elephant and Castle</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Finchley">Finchley</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Finsbury-Park">Finsbury Park</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Fulham">Fulham</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Greenwich">Greenwich</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Hackney">Hackney</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Hammersmith">Hammersmith</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Hampstead">Hampstead</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Harrow">Harrow</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Illford">Illford</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Islington">Islington</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Kensington">Kensington</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Kilburn">Kilburn</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Liverpool-Street">Liverpool Street</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Maida-Vale">Maida Vale</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Mayfair">Mayfair</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Notting-Hill">Notting Hill </a><a href="https://www.serviceoctopus.com/carpet-cleaning/Paddington">Paddington</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Peckham">Peckham</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Putney">Putney</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Queens-Park">Queens Park</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Romford">Romford</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Shoreditch">Shoreditch</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Soho">Soho</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Southall">Southall </a><a href="https://www.serviceoctopus.com/carpet-cleaning/Southwark">Southwark</a><a href="https://www.serviceoctopus.com/carpet-cleaning/St-Johns-Wood">St Johns Wood</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Stoke-Newington">Stoke Newington</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Stratford">Stratford</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Streatham">Streatham</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Sutton">Sutton</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Tooting">Tooting</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Walthamstow">Walthamstow</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Wandsworth">Wandsworth</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Westminster">Westminster</a><a href="https://www.serviceoctopus.com/carpet-cleaning/Wimbledon">Wimbledon</a></div>
</div>-->
                                        </div></div></div></div></section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="/js/global.js"></script>
    <script>
        $(document).ready(function() {
            jQuery('#datetimepicker').datetimepicker({
                value:'{{ $today->format('d/m/Y') }}'
            });

            //<!-- CUSTOM SELECT BUTTON -->
             /*$('select').each(function(){
                 var $this = $(this), numberOfOptions = $(this).children('option').length;

                 $this.addClass('select-hidden');
                 $this.wrap('<div class="select"></div>');
                 $this.after('<div class="select-styled"></div>');

                 var $styledSelect = $this.next('div.select-styled');
                 //$styledSelect.text($this.children('option').eq(0).text());
                 $styledSelect.text($this.children('option:selected').eq(0).text());

                 var $list = $('<ul />', {
                     'class': 'select-options'
                 }).insertAfter($styledSelect);

                 for (var i = 0; i < numberOfOptions; i++) {
                     $('<li />', {
                         text: $this.children('option').eq(i).text(),
                         rel: $this.children('option').eq(i).val()
                     }).appendTo($list);
                 }

                 var $listItems = $list.children('li');

                 $styledSelect.click(function(e) {
                     e.stopPropagation();
                     $('div.select-styled.active').not(this).each(function(){
                         $(this).removeClass('active').next('ul.select-options').hide();
                     });
                     $(this).toggleClass('active').next('ul.select-options').toggle();
                 });

                 $listItems.click(function(e) {
                     //companyList.updateAdditionalServices()
                     e.stopPropagation();
                     $styledSelect.text($(this).text()).removeClass('active');
                     $this.val($(this).attr('rel'));
                     $list.hide();
                     //console.log($this.val());
                 });

                 $(document).click(function() {
                     $styledSelect.removeClass('active');
                     $list.hide();
                 });
             });*/
            <!-- /CUSTOM SELECT BUTTON -->
        });
    </script>
@endsection