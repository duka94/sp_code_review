<!doctype html>
<html class="no-js" lang="en-GB">

<head>
     <?php if (url('/') == 'https://www.serviceoctopus.com') { ?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PMCS6JH');</script>
<!-- End Google Tag Manager -->

    <?php  }?> 
    
    <meta name="geo.region" content="GB" />
    <meta name="geo.placename" content="London" />
    <meta name="geo.position" content="51.4976890;0.1014670" />
    <meta name="ICBM" content="51.4976890, 0.1014670" />
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->


    <meta property="og:url"                content="https://www.serviceoctopus.com/" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="Compare Cheap Cleaning Quotes - Book Online" />
    <meta property="og:description"        content="Compare quotes for end of tenancy cleaning, carpet cleaning and deep in London. Book an approved company online. Save time and money today." />
    <meta property="og:image"              content="https://www.serviceoctopus.com/logo.png" />
    <meta property="og:image:width"        content="500"/>
    <meta property="og:image:height"        content="218"/>


    <title>Compare Deep Cleaning London Quotes | Book Online | Service Octopus</title>


    <meta name="description" content="Compare deep cleaning London quotes with serviceoctopus.com online today. Save time, hassle and money when you book with serviceoctopus.com."/>
    <meta name="robots" content="noodp"/>
    <link rel="canonical" href="https://www..serviceoctopus.com/deep-cleaning/" />
    <link rel='stylesheet' id='ytplayer-css-css'  href='https://www.serviceoctopus.com/css/main.css' type='text/css' media='all' />
    <link rel='stylesheet' id='marketing-fonts-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A400%2C700%2C300&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
    <!--<link rel='stylesheet' id='js_composer_front-css'  href='https://www.serviceoctopus.com/wp-content/plugins/js_composer/assets/css/js_composer.min.css' type='text/css' media='all' />-->



    <script type='text/javascript' src='https://www.serviceoctopus.com/js/jquery.js'></script>
    <script type='text/javascript' src='https://www.serviceoctopus.com/js/jquery-migrate.min.js'></script>


    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://wordpress.serviceoctopus.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1482112013609{background-color: #f0f0f0 !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>


    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
    <style>
  
        
      
        .select-styled{
            position: relative;    border: 1px solid #E8E8E8;
        }
        
        section.intro .main-form input {border-color:#E8E8E8;}
        
       .select-options { 
        border-bottom: 1px solid #E8E8E8;;
    border-left: 1px solid #E8E8E8;;
    border-right: 1px solid #E8E8E8;}
        
        
  
        
    </style>


</head>
<body class="page-template-default page page-id-2135 page-parent logged-in  default default-layout theme-default wpb-js-composer js-comp-ver-5.0.1 vc_responsive">
<?php if (url('/') == 'https://www.serviceoctopus.com') { ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PMCS6JH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php  }?>


@include('layouts.inc.header')

{{--<div class="tt-header-margin "></div>--}}
<div id="app">
    <section class="intro">
        <div class="container" id="search">
            <h2>Compare Deep Cleaning London</h2>

            @if(Session::has('status'))
                <div class="alert alert-danger">
                    <strong>{{ Session::get('status') }}</strong>
                </div>
            @endif
            <search :session_category="{{json_encode($search_cat)}}"></search>

        </div>
    </section>
</div>
<div id="content-wrapper" class="content no-margin">
    {{--<div class="container">--}}
        {{--<div class="simple-text">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<section  class="section stretch_row_only fullwidth"><div class="container"><div class="row"><div class="wpb_column col-md-12 have-padding"><div  class="empty-space  marg-lg-b40 marg-md-b60 marg-sm-b60 marg-xs-b60"></div><div class="text-block " ><div class="simple-text "><h1>Compare and Book Carpet Cleaners.</h1>--}}
                                        {{--</div></div><div  class="empty-space  marg-lg-b30 marg-sm-b45 marg-xs-b45"></div><div class="text-block " ><div class="simple-text "><p style="text-align: center">&lt;&lt; HTML search box code goes here &gt;&gt;</p>--}}
                                        {{--</div></div><div  class="empty-space  marg-lg-b30 marg-sm-b45 marg-xs-b45"></div></div></div></div></section><br />--}}
                    {{--<!--START PINK BOX HERE-->--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <p><!-- TrustBox widget - Micro Review Count --></p>
    <div class="trustpilot-widget" data-locale="en-GB" data-template-id="5419b6a8b0d04a076446a9ad" data-businessunit-id="58d82de80000ff00059f420b" data-style-height="20px" data-style-width="100%" data-theme="light">
        <a href="https://uk.trustpilot.com/review/serviceoctopus.com" target="_blank">Trustpilot</a>
    </div>
    <p><!-- End TrustBox widget --><br />
    <div  class="empty-space  marg-lg-b10 marg-sm-b10 marg-xs-b10"></div>
    <section class="about">
        <div class="container pink">
            <h3>Compare. Book. Relax</h3>
            <div class="row2">
                <div class="column third">
                    <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/compare.png" alt=""></div>
                    <h4>Compare</h4>
                    <p>Simply pick the service you would like to compare, enter your date, time and location and click go.</p></div>
                <div class="column third">
                    <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/book.png" alt=""></div>
                    <h4>Book</h4>
                    <p>Select from 20+ companies to book the service that&#8217;s perfect for you; compare, book, save &#8211; just like that.</p></div>
                <div class="column third">
                    <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/relax.png" alt=""></div>
                    <h4>Relax</h4>
                    <p>Kick back and relax! With everything sorted in under 5 minutes, make yourself a cuppa &#8211; you deserve it.</p></div>
            </div>
        </div>
        <div class="blue-bg">
            <div class="container pink">
                <h3>Why Service Octopus?</h3>
                <div class="row2">
                    <div class="column third">
                        <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/quick.png" alt=""></div>
                        <h4>Quick &amp; Convenient</h4>
                        <p>The days of having to call three different companies for a quote are long gone. Compare 20+ companies and book instantly.</p></div>
                    <div class="column third">
                        <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/always-available.png" alt=""></div>
                        <h4>Always Available</h4>
                        <p>We&#8217;re open 24/7. Compare quotes and book your preferred company whenever it&#8217;s convenient for you.</p></div>
                    <div class="column third">
                        <div class="ico-holder"><img src="https://www.serviceoctopus.com/img/icons/trustworthy.png" alt=""></div>
                        <h4>Trustworthy</h4>
                        <p>We use SSL encryption which protects your details sent over the internet from third-party access and manipulation.</p></div>
                </div>
            </div>
        </div>
    </section>
    <div id="content-wrapper" class="content no-margin"></div>
    <div class="container">
        <div class="simple-text">
            <div class="row">
                <div class="col-md-12">
                    <!--END PINK BOX HERE--><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"><div  class="empty-space  marg-lg-b10 marg-sm-b15 marg-xs-b15"></div></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"><div class="text-block " ><div class="simple-text "><p></p>
                                    </div></div></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-2 have-padding"></div><div class="wpb_column col-md-8 have-padding"><div  class="empty-space  marg-lg-b50"></div><div  class="tt-title"><div class="tt-title-cat"></div><h2 class="c-h2"><small>Frequently asked questions, answered.</small></h2></div><div  class="empty-space  marg-lg-b30 marg-sm-b30 marg-xs-b30"></div><div class="tt-accordion"><div class="tt-accordion-panel"><div class="tt-accordion-title active ">Isn&#039;t it cheaper if I book direct?<div class="tt-accordion-icon"></div></div><div class="tt-accordion-body active "><div class="simple-text size-3"><div class="text-block " ><div class="simple-text "><p>Booking with Service Octopus should always be the same price (if not cheaper) than booking the service directly. If this is not the case <a href="https://www.serviceoctopus.com/help/email-us/">get in touch and let us know</a>. Our objective is to make sure you get the best deal possible.</p>
                                                    </div></div></div></div></div><div class="tt-accordion-panel"><div class="tt-accordion-title">What&#039;s included in end of tenancy cleaning?<div class="tt-accordion-icon"></div></div><div class="tt-accordion-body"><div class="simple-text size-3"><div class="text-block " ><div class="simple-text "><p>We advise cleaning companies to follow the following <a href="https://www.serviceoctopus.com/END-OF-TENANCY-CLEANING-CHECKLIST-1.pdf">end of tenancy cleaining checklist</a>. If you have any additional requests please mention this on the checkout page of the booking process.</p>
                                                    </div></div></div></div></div><div class="tt-accordion-panel"><div class="tt-accordion-title">How do you approve companies?<div class="tt-accordion-icon"></div></div><div class="tt-accordion-body"><div class="simple-text size-3"><div class="text-block " ><div class="simple-text "><p>We have a strict <b>manual</b> approval process which we apply before a company can provide a quote on our site. Find out <a href="https://www.serviceoctopus.com/approval-process/">more about our approval process</a>.</p>
                                                    </div></div></div></div></div><div class="tt-accordion-panel"><div class="tt-accordion-title">Is your site secure?<div class="tt-accordion-icon"></div></div><div class="tt-accordion-body"><div class="simple-text size-3"><div class="text-block " ><div class="simple-text "><p>We use SSL encryption which protects your details sent over the internet from third-party access and manipulation.</p>
                                                    </div></div></div></div></div></div><div  class="empty-space  marg-lg-b50 marg-md-b70 marg-sm-b50 marg-xs-b50"></div><div class="text-block " ><div class="simple-text "><p style="text-align: center">Question still not answered? Visit our <a href="https://www.serviceoctopus.com/help/">help section</a>.</p>
                                    </div></div><div  class="empty-space  marg-lg-b75 marg-md-b70 marg-sm-b90 marg-xs-b90"></div></div><div class="wpb_column col-md-2 have-padding"></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"></div></div></section><section  class="section stretch_row_only fullwidth  vc_custom_1482112013609"><div class="container"><div class="row"><div class="wpb_column col-md-12 have-padding"><section  class="section no"><div class="row"><div class="wpb_column col-md-2 have-padding"></div><div class="wpb_column col-md-8 have-padding"><div  class="empty-space  marg-lg-b40 marg-sm-b40 marg-xs-b40"></div><div  class="tt-title"><div class="tt-title-cat"></div><h2 class="c-h2"><small>Become a subscriber!</small></h2></div><div  class="empty-space  marg-lg-b20"></div><div class="text-block " ><div class="simple-text size-2"><p style="text-align: center">Service Octopus is shaking the cleaning industry up! Do you run a professional cleaning company? Would you like to join Service Octopus as a subscriber company? Find out more today.</p>
                                                    </div></div><div  class="empty-space  marg-lg-b20"></div><div class="text-center"><a  class="c-btn type-1 size-2 color-1" target="_self" title="button" href="https://www.serviceoctopus.com/subscribe/"><span>Learn More</span></a></div><div  class="empty-space  marg-lg-b100 marg-sm-b100 marg-xs-b100"></div></div><div class="wpb_column col-md-2 have-padding"></div></div></section></div></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-2 align-left have-padding"></div><div class="wpb_column col-md-8 have-padding"><div  class="empty-space  marg-lg-b60 marg-sm-b45 marg-xs-b45"></div><div  class="tt-title"><div class="tt-title-cat"></div><h2 class="c-h2"><small>More about Deep Cleaning</small></h2></div><div  class="empty-space  marg-lg-b30 marg-sm-b30 marg-xs-b30"></div><div class="text-block " ><div class="simple-text "><p>Deep cleaning is a one off service that some cleaning companies offer. It is different from a regular clean as the service focuses on getting deep grime and dirt out from your home. A deep clean typically includes scale removal from bathrooms, kitchen, taps and showerheads. Most cleaning companies will offer oven cleaning as part of the service but you will need to check with them. Some companies will even go as far as disinfecting certain areas for you. Other areas include inside and external windows, inside and outside cupboards and under the sink. </p>

                                        <p>As with all cleaning services, it is important to ask your cleaning company what is included in the deep clean and what is an additional cost. Carpets may not be included as well as any other upholstery like curtains and sofas. </p>

                                        <p>Deep clean should not be confused with an end of tenancy clean. An end of tenancy clean will normally follow a standard or checklist to ensure that you get your deposit back. With deep cleaning, no checklist is involved therefore you may lose part of your deposit if you choose this option. </p>

                                        <p>When choosing a company, it is important to check if they have insurance and that they offer a cleaning guarantee. Most companies will bring their own equipment and chemicals with them to your property, so you do not have to provide them. You should always check the end result of the cleaning whilst the cleaners is still there if you can. This is so you can quickly rectify any issues you have with the cleaning. In the event that this is not possible, you may be able to get a re-clean if the cleaning company offers a cleaning guarantee. However, you will need to check what their policy is. Most companies offer a re-clean within 48 hours. </p>
                                    </div></div><div  class="empty-space  marg-lg-b60 marg-sm-b75 marg-xs-b75"></div></div><div class="wpb_column col-md-2 have-padding"></div></div></section><section  class="section no"><div class="row"><div class="wpb_column col-md-12 have-padding"><div class="text-block " ><div class="simple-text "></div></div></div></div></section>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- FOOTER -->
<footer class="tt-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div id="text-2" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">COMPANY</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                            <li ><a href="/about-us">About us</a></li>
                            <li ><a href="/help">Help</a></li>
                            <li ><a href="/blog">Blog</a></li>
                            <li ><a href="/press">Press</a></li>
                        </ul>
                    </div>
                </div>                <div class="empty-space marg-xs-b30"></div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div id="text-3" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">LEGAL</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                            <li ><a href="/customer-terms-and-conditions">Terms and Conditions</a></li>
                            <li ><a href="/privacy-policy">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>                <div class="empty-space marg-sm-b30"></div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div id="text-4" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">COMPARE</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                            <li ><a href="/end-of-tenancy-cleaning/">End of Tenancy Cleaning</a></li>
                            <li ><a href="/carpet-cleaning">Carpet Cleaning</a></li>
                            <li ><a href="/deep-cleaning">Deep Cleaning</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="empty-space marg-lg-b80 marg-sm-b50 marg-xs-b30"></div>
    </div>
    <div class="tt-footer-copy">
        <div class="container">
            <div class="simple-text size-5 color-4">
                <p>Copyright &copy; 2017 Serviceoctopus.com Limited. All rights reserved.</p>

            </div>
        </div>
    </div>
</footer>

<script type='text/javascript' src='https://www.serviceoctopus.com/js/global.js'></script>

<script src="{{ elixir('js/app.js') }}"></script>

</body>
</html>
