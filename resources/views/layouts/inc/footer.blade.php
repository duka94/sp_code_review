@if(\Request::route()->getName() != 'checkout')
<footer class="tt-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div id="text-2" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">COMPANY</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                                <li ><a href="/about-us">About us</a></li>
                                <li ><a href="/help">Help</a></li>
                                <li ><a href="/blog">Blog</a></li>
                                <li ><a href="/press">Press</a></li>
                            </ul>
                        </div>
                    </div>                <div class="empty-space marg-xs-b30"></div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div id="text-3" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">LEGAL</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                                <li ><a href="/customer-terms-and-conditions">Terms and Conditions</a></li>
                                <li ><a href="/privacy-policy">Privacy Policy</a></li>
 				                <li ><a href="/website-terms-of-use">Website Terms of Use</a></li>
                            </ul>
                        </div>
                    </div>                <div class="empty-space marg-sm-b30"></div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div id="text-4" class="widget tt-footer-list footer_widget widget_text"><h5 class="c-h5 color-3">COMPARE</h5><div class="empty-space marg-lg-b10"></div>			<div class="textwidget"><ul style="list-style: none;padding-left:0px;">
                                <li ><a href="/end-of-tenancy-cleaning/">End of Tenancy Cleaning</a></li>
                                <li ><a href="/carpet-cleaning">Carpet Cleaning</a></li>
                                <li ><a href="/deep-cleaning">Deep Cleaning</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="empty-space marg-lg-b80 marg-sm-b50 marg-xs-b30"></div>
        </div>

    <div class="tt-footer-copy">
        <div class="container">
            <div class="simple-text size-5 color-4">
                <p>Copyright &copy; 2017. All rights reserved. Serviceoctopus.com Limited, 75 Park Lane, Croydon, CR9 1XS / Company Number: 10350926 </p>

            </div>
        </div>
    </div>
</footer>
@else
    <footer style="padding:20px;">
        <div class="container">
            <p>Copyright &copy; 2017. All rights reserved. Serviceoctopus.com Limited, 75 Park Lane, Croydon, CR9 1XS / Company Number: 10350926 </p>
        </div>
    </footer>

@endif
