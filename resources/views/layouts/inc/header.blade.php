<header class="tt-header">
    <div class="container">
        <div class="top-inner clearfix">
            <div class="top-inner-container">
                <a href="/"><img src="/logo.svg" alt="Service Octopus Logo" style="width:160px;height:67px;"></a>
                <button class="cmn-toggle-switch"><span></span></button>
            </div>
        </div>
        <div class="toggle-block">
            <div class="toggle-block-container">
                @if(\Request::route()->getName() != 'checkout' && \Request::route()->getName() !='confirm')
                    <nav class="main-nav clearfix">
                        <ul id="nav" class="menu-main">
                            <li id="menu-item-2159" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2159"><a href="/about-us" class=" ">About</a></li>
                            <li id="menu-item-2045" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2045"><a href="/help" class=" ">Help</a></li>
                            <li id="menu-item-2159" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2159"><a href="/price-promise" class=" ">Price Promise</a></li>
                            {{--<li id="menu-item-2129" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2129"><a href="/subscribe" class=" ">Join</a></li>--}}
                            @if(\Request::route()->getName() != 'reservation')
                            <li><a href="tel:02038807700">020 3880 7700</a></li>
                            @endif
                        </ul>
                    </nav>
                  {{--  @if(\Request::route()->getName() == 'reservation' )
                        <div class="nav-more">
                            <a class="c-btn type-2" href="/login"><span>Login</span></a>
                        </div>
                    @endif--}}
                @endif
            </div>
        </div>
    </div>
</header>
