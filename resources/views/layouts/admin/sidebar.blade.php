<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
    {{--<div class="user-panel">--}}
    {{--<div class="pull-left image">--}}

    {{--<img src="img/user2-160x160.jpg" class="img-circle" alt="User Image">--}}
    {{--</div>--}}
    {{--<div class="pull-left info">--}}
    {{--<p>Alexander Pierce</p>--}}
    {{--<!-- Status -->--}}
    {{--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
    {{--</div>--}}
    {{--</div>--}}

    <!-- search form (Optional) -->
    {{--<form action="#" method="get" class="sidebar-form">--}}
    {{--<div class="input-group">--}}
    {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
    {{--<span class="input-group-btn">--}}
    {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
    {{--</button>--}}
    {{--</span>--}}
    {{--</div>--}}
    {{--</form>--}}
    <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">ADMIN MENU</li> 
            <li><a href="/admin"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
            
            <li><a href="/admin/bookings"><i class="fa fa-shopping-cart"></i> <span> Bookings</span></a></li>
            
            <li><a href="/admin/schedule"><i class="fa fa-calendar"></i> <span> Availability</span></a></li>
            
            <li><a href="/admin/services"><i class="fa fa-gbp"></i> <span> Prices</span></a></li>
            
            <?php if(!isset($data['is_approved']) || $data['is_approved']): ?>
            <li class="treeview {{ Request::segment(2) === 'company' ? 'active' : null }}">
                <a href="#"><i class="fa fa-link"></i> <span>Company Info</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/company/general">General Details</a></li>
                    <li><a href="/admin/company/additional">Additional Details</a></li>
                    <li><a href="/admin/company/password">Change password</a></li>
                    <li><a href="/admin/user"><span>Users</span></a></li>
                </ul>
            </li>

            <li><a href="/admin/rating"><i class="fa fa-comments"></i> <span>Reviews</span></a></li>

            <li><a href="/admin/postcodes"><i class="fa fa-map-marker"></i> <span> Postcodes</span></a></li>

            <li><a href="/admin/subscription"><i class="fa fa-user"></i> <span> Subscription</span></a></li>

            <li><a href="/admin/help"><i class="fa fa-life-ring"></i> <span> Help</span></a></li>
            <?php endif; ?>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>