<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MASTER ADMIN MENU</li>


            <li><a href="/master"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>
            <li><a href="/master/bookings"><i class="fa fa-gbp"></i> <span>Bookings</span></a></li>
            <li><a href="/master/companies"><i class="fa fa-building-o"></i> <span>Companies</span></a></li>
            <li><a href="/master/cities"><i class="fa fa-globe"></i> <span>Cities</span></a></li>
            <li><a href="/master/settings"><i class="fa fa-cogs"></i> <span>Settings</span></a></li>
            
            
            <li><a href="/master/companies/activity"><i class="fa fa-building-o"></i> <span>Companies Activity</span></a></li>
            <li class="treeview" id="tooltip_dd">
                <a href="#"><i class="fa fa-list-alt"></i> <span>Tooltip</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/master/tooltip/end-of-tenancy"><span>End of Tennancy</span></a></li>
                    <li><a href="/master/tooltip/carpet-cleaning"> <span>Carpet Cleaning</span></a></li>
                    <li><a href="/master/tooltip/deep-cleaning"><span>Deep Cleaning</span></a></li>
                </ul>
            </li>
            <li class="treeview {{ 
                        Request::segment(2) === 'total' 
                        || Request::segment(2) === 'topcompanies'
                        || Request::segment(2) === 'successfulbookings' 
                        ? 'active' : null }}"
                        >
                <a href="#"><i class="fa fa-link"></i> <span>Reporting</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/master/total"><span>Total number of bookings</span></a></li>
                    <li><a href="/master/topcompanies"> <span>Top 5 companies</span></a></li>
                    <li><a href="/master/successfulbookings"><span>Successful Booking rate</span></a></li>
                </ul>
            </li>
            
            
            <!--<li><a href="/master/revenue_vat"><i class="fa fa-cogs"></i> <span>Revenue - VAT</span></a></li>
            <li><a href="/master/revenue_withoutvat"><i class="fa fa-cogs"></i> <span>Revenue - without VAT</span></a></li>
            -->
            
            

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>