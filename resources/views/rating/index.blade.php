
@extends('reservation.layout.layout')

@section('content')

<div class="container" id="general_app" v-cloak>

    @if ($data['isRated'])

        <div class="row">
            <div class="col-xs-12">

                <h2>You have already rated this order!</h2>

            </div>
        </div>

    @else

        <h2>Leave a review for {{@$company_name}}</h2>

        <div class="alert alert-success" role="alert" v-if="sent && success">
            Thank you, your rating has been saved.
        </div>

        <div class="alert alert-danger" role="alert" v-if="sent && error">
            Error when saving your rating. Please try again later!
        </div>

    <div class="row">
        <div class="col-xs-12">
            <rating :items="items" :value="value" v-on:change="update" kind="growRotate"></rating> ( @{{ value }} )

        </div>



        <vf-form ajax
                 action="/rating/store"
                 method="POST"
                 v-ref:ratingForm
        >
            <vf-status-bar></vf-status-bar>
            <div class="row">

                <div class="col-xs-12 col-md-6">

                    <vf-textarea
                            label="Leave your review"
                            name="comment"
                            v-ref:comment
                            :disabled="sent"
                            :rules="{comment:true, min:4}"
                    >
                    </vf-textarea>

                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <vf-submit v-if="!sent" class="pull-left"></vf-submit>
                </div>

            </div>

        </vf-form>
    </div>

    @endif

</div>

<script>
    var uuid = <?= json_encode($data['uuid']);?>
</script>
<script src="/js/rating.js"></script>

@endsection
{{--</body>--}}

