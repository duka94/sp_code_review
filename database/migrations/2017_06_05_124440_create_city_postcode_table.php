<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityPostcodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_postcode', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id');
            $table->integer('post_code_id');
            $table->index('city_id');
            $table->index('post_code_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::drop('city_postcode');
    }
}
