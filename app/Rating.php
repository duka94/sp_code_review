<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

    protected $visible=['id','email','rating','comment','answer','completed','uuid','name'];
    protected $fillable = ['email','rating','comment','answer','completed','uuid','name'];

    protected $casts = ['completed'=>'boolean','id'=>'string'];

    public function human_time($time)
    {

        $time = time() - $time; // to get the time since that moment
        $time = ($time < 1) ? 1 : $time;
        $tokens = array(
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) {
                continue;
            }
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's ago' : 'ago');
        }

    }

    public function getCreatedAtAttribute($timestamp)
    {
        return $this->human_time(strtotime($timestamp));
//        return Carbon::parse($timestamp)->format('d/m/Y');
    }

//    public function getDateCreatedAttribute()
//    {
//        return Carbon::parse($this->created_at)->format('Y/m/d');
//    }

    public function getNameAttribute()
    {
        $order = TempData::findByUuid($this->uuid);
        if($order)
        {
            $data = $order->getData();

            return $data->client->firstname.' '.$data->client->lastname;
        }

        return "No Name";
    }
}
