<?php

namespace App\Mail;

use App\ServiceCategories;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerQuoteReserved extends Mailable
{
    use Queueable, SerializesModels;


    public $link;
    public $customer_name;
    public $category_name;

    const MAIL_NAME = "CustomerQuoteReserved";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($uuid, $customer)
    {
        $this->customer_name = $customer->firstname . ' ' . $customer->lastname;
        $this->link = config('app.restore_order_link') . $uuid;
        $this->category_name = ServiceCategories::where('id', $customer->services)->pluck('name')->first();
        // TODO: Lowets price in the subject
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.customer-quote-reserved')->from(env('EMAIL_NOREPLAY'),env('SITE_NAME'))->subject($this->customer_name.', our best price for '.$this->category_name.'');
    }
}
