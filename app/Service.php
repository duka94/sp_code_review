<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    protected $fillable = ['id','name','limit','tooltip_text'];
//    protected $visible = ['id','name','limit','created_at'];
    //
    public function categories()
    {
       return $this->belongsTo(ServiceCategories::class,'service_category_id');
    }


    public function getMainServices()
    {
       return $this->where('parent_id',0);
    }

    public function parent()
    {
        return $this->belongsTo(Service::class,'parent_id','id');
    }

    public function subs()
    {
        return $this->hasMany(Service::class,'parent_id');
    }

    public function related() {
        return $this->hasMany(Service::class,'related_id');
    }

    public function related_main()
    {
        return $this->belongsTo(Service::class,'related_id','id');
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class,'companies_services')->withPivot('price');
    }

	public function extended() {
		return $this->hasMany(ServicesExtended::class, 'id', 'service_id');
    }

    public static $mainCategories = [
        'tenancy' => 1,
        'carpet' => 4,
        'deep' => 17,
    ];

    public static function getMainAdditionalServices($mainCategory)
    {
        $mainCategory = ServiceCategories::find(self::$mainCategories[$mainCategory]);
        $childCategories = $mainCategory->childs()->get()->pluck('id')->all();
        if ($mainCategory->id == 4) {
            $carpetServices = [3, 1, 4, 2, 5, 6, 67, 69, 70, 71, 15, 9, 10, 11, 45, 46, 47, 48, 49, 50, 51, 72, 73, 74, 75, 76, 77, 78, 79];

            $services = Service::whereRaw('id IN (' . implode(',', $carpetServices) . ')')->get()->toArray();
        } else {
            $services = Service::whereRaw('service_category_id IN (' . implode(',', $childCategories) . ')')->get()->toArray();
        }

        if($mainCategory->id == 17){
            $new_services = Service::whereIN('id', [13, 14, 109, 110])->get()->toArray();

            $services = array_merge($services, $new_services);
        }

//        $excludeServices = [13, 14, 27, 68, 92, 99, 100, 98, 107, 108];

//        foreach ($services as $service) {
//            if (!in_array($service['id'], $excludeServices)) {
//                $result[] = $service;
//            }
//        }

        return $services;

    }
    public static function getAdditionalService($additionalServices, $mainCategory, $registeredServices)
    {
        $get_additional_service = $additionalServices;

        $mainCategory = ServiceCategories::find(self::$mainCategories[$mainCategory]);

        $childCategories = $mainCategory->childs()->get()->pluck('id')->all();

        // This will need serious refactor
        if ($mainCategory->id == 4) {
            $carpetServices = [3, 1, 4, 2, 5, 6, 67, 68, 69, 70, 71, 15, 9, 10, 11, 45, 46, 47, 48, 49, 50, 51, 72, 73, 74, 75, 76, 77, 78, 79];
            //$services = Service::where('service_category_id', '=', 5)->get()->toArray();
            $services = Service::whereRaw('id IN (' . implode(',', $carpetServices) . ')')->get()->toArray();
        } else {
            $services = Service::whereRaw('service_category_id IN (' . implode(',', $childCategories) . ')')->get()->toArray();
        }

        $excludeServices = [13, 14, 27, 92, 99, 100, 98, 107, 108];
        $result = [];

        if( !empty($get_additional_service) )
        {

            $get_additional_service = self::getServicesId($get_additional_service, $childCategories);
        }

        foreach ($services as $service) {
            if (!in_array($service['id'], $excludeServices)) {
                if( empty($get_additional_service) )
                {
                    $result[$service['id']] = [
                        'id' => $service['id'],
                        'name' => trim($service['name']),
                        'icon' => $service['icon'],
                        'quantity' => 0,
                        'tooltip_active' => $service['tooltip_active'],
                        'tooltip_text' => $service['tooltip_text']
                    ];

                } else {

                    $result[$service['id']] = [
                        'id' => $service['id'],
                        'name' => trim($service['name']),
                        'icon' => $service['icon'],
                        'quantity' => isset($get_additional_service[$service['id']]) ? $get_additional_service[$service['id']] : 0,
                        'tooltip_active' => $service['tooltip_active'],
                        'tooltip_text' => $service['tooltip_text']
                    ];
                    isset($get_additional_service[$service['id']]) ? $registeredServices[$service['id']] =  (string)$get_additional_service[$service['id']]  : '';
                }
            }
        }
        if( empty($get_additional_service) )
        {
            return $result;
        } else {
            $result['registeredServices'] = $registeredServices;
            return $result;
        }
    }

    public static function getServicesId($services, $child_categories)
    {
        $result = [];

        foreach ($services as $service => $value) {
            $service = Service::where('slug', $service)->whereIn('service_category_id', $child_categories)->first();
            $service->qty = $value;
           $result[$service->id] = (int)$service->qty;
        }

        return $result;
    }
}
