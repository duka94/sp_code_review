<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Illuminate\Http\Request;
use Laravel\Cashier\Subscription;

class Company extends Authenticatable
{
    use Notifiable;
    use Billable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'city', 'postcode', 'logo', 'liability_img', 'max_jobs', 'approved'
    ];

//    protected $visible = [
//        'id','email','name','created_at','pivot'
//    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function data()
    {
        return $this->hasOne(CompanyData::class);
    }

    public function associations()
    {
        return $this->hasManyThrough(Association::class, 'company_associations');
    }


    /**
     *
     * SERVICES
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'companies_services')->withPivot(['price', 'prices']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function serviceTypes()
    {
        return $this->belongsToMany(ServiceCategories::class, 'companies_service_categories');
    }

    /**
     * @return mixed
     */
    public function getCompanySelectedServices()
    {
        return $this->services()->with('categories')->with(['subs', 'parent.categories'])->get();
    }

    public function isFirstLogin()
    {
        return (bool)$this->data_collected;
    }

    public function isMainAccount()
    {
        return !(bool)$this->parent_id;
    }

    public function toggleApproved()
    {
        $this->approved = !$this->approved;
        return $this;
    }

    public function parent()
    {
        return $this->belongsTo(Company::class, 'parent_id', 'id');
    }

    public function subs()
    {
        return $this->hasMany(Company::class, 'parent_id');
    }

    public function createSubUser($email, $password)
    {
        $newUser = $this->replicate();
        $newUser->email = $email;
        $newUser->password = $password;
        $newUser->parent_id = $this->id;
        $newUser->remember_token = null;
        $newUser->save();

        return $newUser;
    }

    /**
     * @param Company|null $company
     * @return Company|null
     */
    public static function getMainAccount(Company $company = null)
    {
        if (!$company) {
            $company = \Auth::user();
        }

        if ($company->isMainAccount()) {
            return $company;
        }
        return $company->parent()->first();
    }


    public function timeslots()
    {
        return $this->hasMany(TimeSlot::class);
    }


    public function setPasswordAttribute($password)
    {
        $pass_alg = password_get_info($password);
        if(!$pass_alg['algo']) {
            $this->attributes['password'] = bcrypt($password);
        } else {
            $this->attributes['password'] = $password;
        }
    }

    public function regions()
    {
        return $this->hasMany(CompanyRegions::class);
    }

    public function orders()
    {
        return $this->hasMany(TempData::class);
    }

    public function todayOrders()
    {
        return $this->orders()->where('created_at', '>=', Carbon::today()->toDateString())->get();
    }
    public function orderOnDate($date)
    {  
       // return $this->orders()->where('created_at', '>=', Carbon::parse($date)->toDateString() . ' 00:00:01')->where('created_at', '<=', Carbon::parse($date)->toDateString() . '23:59:59')->get();
        $orders = $this->orders()->get();
        $i = 0;
    	$orders->filter(function ($order) use ($date,&$i){
              
                $order_data = $order->getData();
                $order_date = Carbon::parse($order_data->service_date);
                
                if(Carbon::parse($date)->toDateString() == Carbon::parse($order_data->service_date)->toDateString()){
                    $i++;
                }
                
        });
        
        return $i;
    }

    public function ratingsWithUuid($id)
    {
        return $this->hasMany(Rating::class)->where('uuid', $id);
    }
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    /**
     *
     * POSTCODES
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function postcodes()
    {
        return $this->belongsToMany(Postcode::class)->withPivot('price');
    }


    public function cities()
    {
        return $this->belongsToMany(City::class);
    }
    /**
     * @param Request $request
     *
     * @return array|\Exception
     */
    public static function syncPostcodes(Request $request)
    {
        $data = [];

        foreach ($request->all() as $postcode_id => $price) {
            $data[$postcode_id] = ['price' => $price];
        }

        try {
            return self::getMainAccount()->postcodes()->sync($data);
        } catch (\Exception $e) {
            return $e;
        }

    }

    /**
     * Въведените пощенски кодове
     *
     * @return \Illuminate\Support\Collection
     */
    public function getCompanyPostcodes()
    {
        return $this->postcodes()->get()->pluck('id');
    }


    /**
     * WORKDAYS
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function workdays()
    {
        return $this->hasOne(CompanyWorkdays::class, 'company_id', 'id');
    }

    /**
     * @param bool $json
     *
     * @return mixed
     */
    public function getCompanyWorkdays($json = false)
    {

        $workdays = $this->workdays()->get();

        $workdays = $workdays->isEmpty() ? '' : $workdays->pluck('workdays')->first();

        return (!!$json) ? json_decode($workdays) : $workdays;
    }


    /**
     * RESTDAYS/HOLIDAYS connection
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function restdays()
    {
        return $this->belongsToMany(Holidays::class, 'company_restdays');
    }

    /**
     * Company restdays
     * @return \Illuminate\Support\Collection
     */
    public function getCompanyRestdays()
    {
        return $this->restdays()->get()->pluck('id');
    }

    /**
     * SUBSCRIPTIONS
     *
     * @return mixed
     */
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class)->orderBy('created_at', 'desc');
    }

    public function getCompanySubscriptionPlan()
    {
        return SubscriptionPlan::where('stripe_plan', $this->subscriptions->pluck('name')->first())->first();
    }

    public function getSubscriptionLimit()
    {
        return $this->getCompanySubscriptionPlan()->jobs_limit;
    }

    public function getTodayBookings()
    {
        return $this->orders()->where('created_at', '>=', Carbon::now()->toDateString() . ' 00:00:01')->where('created_at', '<=', Carbon::now()->toDateString() . '23:59:59');
    }

    public function getThisMonthBookings()
    {
        return $this->orders()->where('created_at', '>=', Carbon::now()->startOfMonth())->where('created_at', '<=', Carbon::now()->endOfMonth());
        //TODO: Tuk trqbva da bade stripe datata ot stripe.
    }

    public function emails()
    {
        return $this->hasMany(Emails::class, 'company_id');
    }


    public static function getAllMainActive()
    {
        return (new static())->all()->where('parent_id', 0)->where('data_collected', 1);
    }

    public function isSendEmail($name)
    {
        $id = $this->id;
        $ret = false;
        $this->emails()->each(function (Emails $email) use ($name, $id,&$ret) {
            if ($email->isSend($name,$id)) {
                $ret = true;
            } else {
                $ret = false;
            }
        });
        return $ret;
    }

}
