<?php

namespace App\Console\Commands;

use App\Mail\CustomerFeedback;
use App\TempData;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class EmailCustomersFeedback extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:email-customers-feedback';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send review email to customers who booked service for yesterday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $check_day = date("Y-m-d", strtotime("-1 days"));

        $orders = TempData::getServicedOrders($check_day);

        foreach ($orders as $order) {

            $rating = $order->company()->first()->ratingsWithUuid($order->uuid)->get();
            if($rating->isEmpty())
            {
                $company = $order->company()->first();
                $order_data = $order->getData();

                try {
                    Mail::to($order_data->client->email)->send(new CustomerFeedback($company, $order->uuid));
                } catch (Exception $e) {
                    echo($e->getMessage());
                }
            }

        }

        $msg = count($orders);
        if( env('APP_ENV', 'local') == 'live' )
        {
            mail("duka@outlook.com","Check cron LIVE",$msg);

        } else {

            mail("duka@outlook.com","Check cron dev",$msg);
        }
    }
}
