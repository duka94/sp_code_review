<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'created_at', 'updated_at'];

    public function postcodes()
    {
        return $this->belongsToMany(Postcode::class);
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }
}
