<?php

namespace App\Repository;

use App\Service;

class ServiceRepository
{
    public function findByCategories(array $categories = [])
    {
        return Service::whereIn('services.service_category_id', $categories)->get()->toArray();
    }
}
