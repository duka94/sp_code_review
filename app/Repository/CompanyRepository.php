<?php

namespace App\Repository;

use App\Postcode;
use App\Company;
use Illuminate\Support\Facades\DB;

class CompanyRepository
{
    public function findApprovedCompaniesByServicesAndPostcode(
        array $services,
        Postcode $postcode = null
    ) {
        $results = [];

        foreach ($services as $service) {
            $qb = DB::table('companies_services')
                ->select('companies_services.company_id')->distinct()
                ->leftJoin('companies', 'companies_services.company_id', '=', 'companies.id')
                ->where('approved', '=', 1);

            if ($postcode) {
                $qb->leftJoin('company_postcode', 'companies_services.company_id', '=', 'company_postcode.company_id')
                    ->where('company_postcode.postcode_id', '=', $postcode->id);
            }

            $qb->where('service_id', '=', $service);
            $results[] = $qb->get()->toArray();
        }

        $companyIds = [];
        foreach ($results as $result) {
            $companyIds[] = array_map(function ($data) {
                return $data->company_id;
            }, $result);
        }

        if (count($companyIds) > 1) {
            return call_user_func_array('array_intersect', $companyIds);
        } else {
            return isset($companyIds[0]) ? $companyIds[0] : [];
        }
    }

    public function idsToCompanies(array $companyIds)
    {
        return Company::whereRaw('id IN ('.implode(',', $companyIds).')')->where('approved', 1)->get();
    }

    private function convertToCompanyObject($companyIds)
    {
        $ids = array_map(function ($companyId) {
            return $companyId->company_id;
        }, $companyIds);

        return DB::table('companies')->whereRaw('id IN ('.implode(',', $ids).')')->get()->toArray();
    }
}
