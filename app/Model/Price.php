<?php

namespace App\Model;

class Price
{
    private $cleaningPrice;
    private $bookingFee;

    public function __construct($cleaningPrice, $bookingFee)
    {
        $this->cleaningPrice = $cleaningPrice;
        $this->bookingFee = $bookingFee;
    }

    /**
     * @return mixed
     */
    public function getCleaningPrice()
    {
        return $this->cleaningPrice;
    }

    /**
     * @return mixed
     */
    public function getBookingFee()
    {
        return $this->bookingFee;
    }

    public function subtractAmount($amount)
    {

        $discount = $this->bookingFee - $amount < 1 ? ($this->bookingFee - 1) : $amount;

        return new static(
            max(1, $this->cleaningPrice - $discount),
            $this->bookingFee - $discount
        );
    }
}
