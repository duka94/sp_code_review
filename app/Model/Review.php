<?php

namespace App\Model;

use App\TempData;

class Review
{
    private $rating;
    protected $table = 'ratings';

    public function __construct($rating)
    {
        $this->rating = $rating;
    }

    public function getComment()
    {
        return $this->rating->comment;
    }

    public function getCreatedDate()
    {
        return $this->rating->created_at;
    }

    public function getName()
    {
        $order = TempData::findByUuid($this->rating->uuid);
        if($order)
        {
            $data = $order->getData();

            return $data->client->firstname.' '.$data->client->lastname;
        }

        return "No Name";

    }
}