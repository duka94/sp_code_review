<?php

namespace App\Model;

use App\Company;
use App\Rating;
use App\CompanyData;
use JMS\Serializer\Annotation\AccessType;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\ReadOnly;
use JMS\Serializer\Annotation\Exclude;
use Illuminate\Support\Facades\DB;

/**
 * @ReadOnly
 * @AccessType("public_method")
 */
class CompanyModel
{
    /**
     * @Exclude
     */
    private $company;

    /**
     * @Exclude
     */
    private $companyData;

    /**
     * @Type("string")
     */
    private $image;

    /**
     * @Type("string")
     */
    private $special_offer;
    /**
     * @Type("string")
     */
    private $name;

    /**
     * @Type("integer")
     */
    private $id;

    /**
     * @Type("integer")
     */
    private $numberOfReviews;

    /**
     * @Type("integer")
     */
    private $reviewRate;

    /**
     * @Type("integer")
     */
    private $isHalfRate;

    /**
     * @Type("integer")
     */
    private $liabilityInsurance;

    /**
     * @Type("string")
     */
    private $cleaningGuarantee;

    /**
     * @Type("string")
     */
    private $servicingSince;

    /**
     * @Type("integer")
     */
    private $totalPrice;

    /**
     * @Type("integer")
     */
    private $bookingFee;

    public function __construct(Company $company, Price $price)
    {
        $this->company = $company;
        $this->companyData = CompanyData::where('company_id', $this->company->id)->get()->first();
        $this->totalPrice = $price->getCleaningPrice();
        $this->bookingFee = $price->getBookingFee();
    }

    public function getSpecial_offer()
    {
        return $this->company->special_offer;
    }

    public function getImage()
    {
        $this->image = '/img/logos/'.$this->company->logo;

        return $this->image;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        $this->name = $this->company->name;

        return $this->name;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        $this->id = $this->company->id;

        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNumberOfReviews()
    {
        $this->numberOfReviews = Rating::where('company_id', $this->company->id)->count();

        return (int) $this->numberOfReviews;
    }

    /**
     * @return mixed
     */
    public function getReviewRate()
    {
        $averageRating = DB::select(DB::raw("select AVG(rating) as rating from ratings where company_id= ".$this->company->id));
        $averageRating = $averageRating[0]->rating;
        $averageRating = floor($averageRating * 2) / 2;

        return $averageRating;
    }

    public function getIsHalfRate()
    {
        $averageRating = $this->getReviewRate();
        $whole = floor($averageRating);

        return $averageRating - $whole > 0 ? 1 : 0;
    }

    /**
     * @return mixed
     */
    public function getLiabilityInsurance()
    {
        $this->liabilityInsurance = $this->companyData->liability_amount;

        return $this->liabilityInsurance;
    }

    /**
     * @return mixed
     */
    public function getCleaningGuarantee()
    {
        $this->cleaningGuarantee = $this->companyData->complaints;

        return $this->cleaningGuarantee;
    }

    /**
     * @return mixed
     */
    public function getServicingSince()
    {
        $this->servicingSince = $this->companyData->date_established;

        return $this->servicingSince;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @return mixed
     */
    public function getBookingFee()
    {
        return $this->bookingFee;
    }
}