<?php

namespace App\Model;

use App\Company;
use App\ServiceCategories;
use DateTime;

class Order
{
    private $company;
    private $postcode;
    private $mainServiceCategory;
    private $serviceDate;
    private $serviceTime;
    private $services;

    public function __construct(
        Company $company,
        ServiceCategories $serviceCategory,
        DateTime $serviceDate,
        $services,
        $serviceTime,
        $postcode
    ) {
        $this->company = $company;
        $this->postcode = $postcode;
        $this->mainServiceCategory = $serviceCategory;
        $this->serviceDate = $serviceDate;
        $this->serviceTime = $serviceTime;
        $this->services = $services;
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function getPostcode()
    {
        return $this->postcode;
    }

    public function getMainServiceCategory()
    {
        return $this->mainServiceCategory;
    }

    public function getServiceDate()
    {
        return $this->serviceDate;
    }

    public function getServiceTime()
    {
        return $this->serviceTime;
    }

    public function getServices()
    {
        return $this->services;
    }
}