<?php

namespace App\Http\Controllers;

use DateTime;

class PageController
{
    public function carpet()
    {
        return view('page.carpet', [
            'today' => new DateTime(),
        ]);
    }

    public function endOfTenancyCleaning()
    {
        $search = 'tenancy';
        return view('page.endOfTenancyCleaning', [
            'today' => new DateTime(),
            'search_cat' => $search
        ]);
    }
    public function carpetCleaning()
    {
        $search = 'carpet';
        return view('page.carpetCleaning', [
            'today' => new DateTime(),
            'search_cat' => $search
        ]);
    }
    public function deepCleaning()
    {
        $search = 'deep';

        return view('page.deepCleaning', [
            'today' => new DateTime(),
            'search_cat' => $search
        ]);
    }
}