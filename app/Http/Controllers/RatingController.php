<?php

namespace App\Http\Controllers;

use App\Company;
use App\Rating;
use App\TempData;
use App\Mail\SubscriberReceivedFeedback;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Requests;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($uuid)
    {
        $rated = (bool)Rating::where('uuid', $uuid)->first();
        $order = TempData::findByUuid($uuid);
        $order_data = $order->getData();
//        $ratings = Rating::where('email', $order_data->client->email);

        $data = ['uuid' => $uuid, 'isRated' => $rated];
        $page_title = "Leave a review for {$order_data->company->name} | Service Octopus";
        $company_name = $order_data->company->name;

        return view('rating.index', compact('data', 'page_title', 'company_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();
//        dd( $data );
//        return new JsonResponse(['error' => 'Error'], 422);
        $order = TempData::findByUuid($data['uuid']);
        $order_data = $order->getData();

        if($data['rating'] == 1 && $data['comment'] == '')
        {
            return new JsonResponse('It looks like you\'re leaving a 1 star review. Please leave some text below to explain this.', 422);
        } else if($data['rating'] == 0) {
            return new JsonResponse('Please leave a review.', 422);
        }
        else if($data['comment'] == '') {
            $data['comment'] = 'This customer left a '. $data['rating'] .' star review with no comments';
        }

        $exist = Company::find($order->company_id)->ratings()->where(['email' => $order_data->client->email, 'uuid' => $data['uuid']])->first();

        //Can be added only one by user
        if ($exist == null) {
            if ((int)$request->get('rating') > 0) {
                $add = Company::find($order->company_id)->ratings()->create([
                    'email' => $order_data->client->email,
                    'uuid' => $data['uuid'],
                    'rating' => $data['rating'],
                    'comment' => $data['comment'],
                    'answer' => ''
                ]);
                if ($add) {
                    $company = Company::find($order->company_id);
                    $rating = [];
                    $rating['rating'] = $data['rating'];
                    $rating['name'] = $order_data->firstname;
                    \Mail::to($company->email)->send(new SubscriberReceivedFeedback($company, $rating));

                    return json_encode(['status' => 'ok']);
                }
            }
        }
        return new JsonResponse(['error' => 'Error'], 422);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
