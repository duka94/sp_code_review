<?php

namespace app\Http\Controllers\Callbacks;

use App\ServiceCategories;
use App\Postcode;
use App\Service;
use App\TempData;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Service\CompanyFilter;
use App\Service\CouponHandler;

class ReservationCallbackController
{
    public function homeData()
    {
        return new JsonResponse([
            'serviceCategories' => ServiceCategories::getRootCategoriesSelect(),
        ]);
    }

    public function getCompanies(Request $request)
    {
        $companyFilter = new CompanyFilter();

        return new JsonResponse($companyFilter->filter($request));
    }

    public function validatePostcode(Request $request)
    {
        if (!$postcode = $request->request->get('postcode')) {
            return new JsonResponse([], 400);
        }

        $findPostcode = Postcode::whereRaw("INSTR('{$postcode}',`name`) = 1 ")->get();

        if (count($findPostcode) === 0) {
            return new JsonResponse([], 400);
        }

        return new JsonResponse();
    }

    public function getServicesByCategory(Request $request)
    {
        $categoryId = $request->query->get('categoryId');

        $services = Service::query()->select(['id', 'name'])
            ->where('service_category_id', '=', $categoryId)->get()->toArray();

        return new JsonResponse($services);
    }

    public function setSelectedServices(Request $request)
    {
        $services = $request->request->get('services');
        $order = $request->request->get('order');
        $registerData = $request->request->get('registerData');

        if (!$services || !$order) {
            return new JsonResponse('Invalid parameters', 400);
        }

        $order = TempData::find($order);

        if (!$order) {
            return new JsonResponse('Invalid order', 400);
        }

        $data = json_decode($order->data, true);

        $data['selected_services'] = $services;

        if ($registerData) {
            $data['register_data'] = $registerData;
        }

        $order->data = json_encode($data, JSON_UNESCAPED_SLASHES);
        $order->save();

        return new JsonResponse();
    }

    public function buildSelectedText(Request $request)
    {
        $services = $request->request->get('services');
        $result = [];
//        if ($service->name == "Bedroom" || $service->name == "bathrooms") {
//            $result[] = '<span>' . $quantity . ' ' . $service->name . '</span>';
//        }
        foreach ($services as $serviceId => $quantity) {
            $service = Service::find($serviceId);
            if (in_array($serviceId, [97, 107, 108])) {
                $result[] = '<span>'.$service->name.'</span>';
            } else {
                $result[] = '<span>' . $quantity . ' ' . $service->name . '</span>';
            }
        }

        return new JsonResponse(['text' => implode(',', $result)]);
    }

    public function applyCoupon(Request $request)
    {
        $coupon = $request->request->get('coupon');
        $couponHandler = new CouponHandler();

        if ($couponHandler->isValid($coupon)) {
            $coupon = $couponHandler->getCouponByName($coupon);
            $value = $coupon ?: 0;

            return new JsonResponse(['discount' => $value]);
        }

        return new JsonResponse([], 400);
    }
}
