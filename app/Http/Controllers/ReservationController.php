<?php

namespace App\Http\Controllers;

use App\City;
use App\Company;
use App\CompanyData;
use App\Factory\TempDataFactory;
use App\Mail\ClientBookingEmail;
use App\Mail\CompanyBookingEmail;
use App\Mail\CountLimit;
use App\Mail\LimitExpireEmail;
use App\Model\Review;
use App\Postcode;
use App\Rating;
use App\Service;
use App\Service\BuildCompanyResponseModel;
use App\Service\CompanyFilter;
use App\Service\CompanyServiceChecker;
use App\Service\CouponHandler;
use App\Service\OrderHelper;
use App\Service\PriceCalculator;
use App\Service\ServiceDateValidator;
use App\ServiceCategories;
use App\TempData;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use JMS\Serializer\SerializerBuilder;
use Stripe\Stripe;
use Stripe\Token;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Validator;

class ReservationController extends Controller
{
    public static $mainCategories = [
        'tenancy' => 1,
        'carpet' => 4,
        'deep' => 17,
    ];

    public function index(Request $request)
    {
        $postCode = $request->request->get('postCode');
        $serviceCategory = $request->request->get('serviceCategory');
        $serviceDate = $request->request->get('serviceDate');
        $serviceTime = $request->request->get('serviceTime');
        $serviceDate = $serviceDate ?
            (isset($serviceDate['date']) ? new DateTime($serviceDate['date']) : DateTime::createFromFormat('d/m/Y', $serviceDate)) :
            new DateTime();
        $page_title = 'Compare Cleaning and Removals Quotes | serviceoctopus.com';

        if( !empty( Session::get('serviceCategory') ) )
        {
            return view('reservation.index', [
                'serviceCategories' => ServiceCategories::getRootCategoriesSelect(),
                'serviceDate' => $serviceDate,
                'serviceTime' => $serviceTime,
                'serviceCategory' => $serviceCategory,
                'postCode' => $postCode,
                'page_title' => $page_title,
                'session_category' => Session::get('serviceCategory'),
                'session_date' => Session::get('date'),
                'session_postcode' => Session::get('postcode'),
                'session_time' => Session::get('time'),
            ]);
        }

        return view('reservation.index', [
            'serviceCategories' => ServiceCategories::getRootCategoriesSelect(),
            'serviceDate' => $serviceDate,
            'serviceTime' => $serviceTime,
            'serviceCategory' => $serviceCategory,
            'postCode' => $postCode,
            'page_title' => $page_title
        ]);


    }

    public function compareServices(Request $request)
    {
        if( @Session::get('succesfully_bought') == 'success' )
        {
            Session::forget('succesfully_bought');
        }

        $serviceCategoryId = $request->query->get('serviceCategory');
        $serviceCategory = $findPostcode = null;
        $serviceDate = Carbon::parse($request->query->get('date'));
        $serviceTime = $request->query->get('time');
        $postCode = $request->query->get('postcode');
        Session::put([
            "serviceCategory" => $serviceCategoryId,
            "date" => $request->query->get('date'),
            "postcode" => $postCode,
            "time" => $serviceTime
        ]);

        if( $this->checkTime($serviceDate) === false) {
           Session::flash('status', 'Sorry, we don\'t allow bookings for the next day past 5pm. Please try another day.');

            return redirect()->route("reservation", [
                "serviceCategory" => $serviceCategoryId,
                "serviceDate" => $serviceDate,
                "serviceTime" => $serviceTime,
                "postCode" => $postCode
            ]);

        }

        if (!$serviceCategoryId || !$serviceDate || !$postCode) {
            Session::flash('status', 'Please complete all the search fields below.');

            return redirect()->route('reservation', [
                "serviceCategory" => $serviceCategoryId,
                "serviceDate" => $serviceDate,
                "serviceTime" => $serviceTime,
                "postCode" => $postCode
            ]);
        }

        if ($serviceDate) {
            $valid = $this->validateServiceDate($serviceDate);
            if (!$valid) {
                return redirect()->route('reservation', [
                    "serviceCategory" => $serviceCategoryId,
                    "serviceDate" => $serviceDate->format("Y-m-d"),
                    "serviceTime" => $serviceTime,
                    "postCode" => $postCode
                ]);
            }
        }

        if ($serviceCategoryId && !$serviceCategory = $this->validateMainServiceCategory($serviceCategoryId)) {
            if (!$serviceCategory) {
                Session::flash('status', 'Please select a service below.');

                return redirect()->route('reservation', [
                    "serviceCategory" => $serviceCategoryId,
                    "serviceDate" => $serviceDate->format("Y-m-d"),
                    "serviceTime" => $serviceTime,
                    "postCode" => $postCode
                ]);
            }
        }
        if ($postCode && !$findPostcode = $this->validatePostcode($postCode)) {
            return redirect()->route('reservation', [
                "serviceCategory" => $serviceCategoryId,
                "serviceDate" => $serviceDate->format("Y-m-d"),
                "serviceTime" => $serviceTime,
                "postCode" => $postCode,
            ]);
        }

        if ($serviceTime && !in_array($serviceTime, range(8, 17))) {
            Session::flash('status', 'Invalid service time provided!');

            return redirect()->route('reservation', [
                "serviceCategory" => $serviceCategoryId,
                "serviceDate" => $serviceDate->format("Y-m-d"),
                "serviceTime" => $serviceTime,
                "postCode" => $postCode
            ]);
        }

        $services = $request->except(["serviceCategory", "date", "postcode", "time"]);
        $additionalServices = $services;
        unset($additionalServices['bathrooms'], $additionalServices['bedrooms'], $additionalServices['propertyType']);

        $getBedrooms = empty($services['bedrooms']) ? 1 : $services['bedrooms'];
        $getBathrooms = empty($services['bathrooms']) ? 1 : $services['bathrooms'];

        if ($serviceCategoryId == 'tenancy')
        {
            $registeredServices = [13 => $getBedrooms, 14 => $getBathrooms];
        } elseif ($serviceCategoryId == 'deep') {
            $registeredServices = [13 => $getBedrooms, 14 => $getBathrooms];
        } elseif ($serviceCategoryId == 'carpet') {
            $registeredServices = [3 => $getBathrooms, 1 => $getBedrooms];
        }
        $serviceCategoryName = $serviceCategory->name;

        if( empty($additionalServices) )
        {
            $defaultAdditionalService = Service::getAdditionalService(null, $serviceCategoryId, null );

        } else {

            $defaultAdditionalService = Service::getAdditionalService($additionalServices, $serviceCategoryId, $registeredServices );
            $registeredServices = $defaultAdditionalService['registeredServices'];
            unset($defaultAdditionalService['registeredServices']);
        }

        $bedroomTooltip = Service::find(13);
        $bathroomTooltip = Service::find(14);
        $flatTooltip = Service::find(109);
        $houseTooltip = Service::find(110);
        $keyCollection = Service::find(98);

        return view('reservation.compare_services', compact('serviceDate',
                                                            'serviceTime',
                                                            'serviceCategory',
                                                            'serviceCategoryId',
                                                            'serviceCategoryName',
                                                            'postCode',
                                                            'services',
                                                            'additionalServices',
                                                            'registeredServices',
                                                            'defaultAdditionalService',
                                                            'bedroomTooltip',
                                                            'bathroomTooltip',
                                                            'flatTooltip',
                                                            'houseTooltip',
                                                            'keyCollection'
        ));
    }



    public function checkTime($serviceDate) {

         $now =  Carbon::now('Europe/London');

         if ($serviceDate->isTomorrow() and $now->hour >= 17) {

          return false;

         }
    }

    public function getCompanies(Request $request)
    {
        $data = $request->request->get('data');
        $order = $request->request->get('order');

        $data['serviceCategoryId']  != 'carpet' ? $property_type = $data['propertyType'] : $property_type = null;
        $serviceCategoryId = $data['serviceCategoryId'];
        $serviceDate = $data['date'];
        $serviceTime = $data['time'];
        $postCode = $data['postCode'];

        // Assoc array, quantity, serviceId, {33: 1, 21: 2}
        $services = $data['services'];

        if (!$serviceCategoryId || !$serviceDate || !$postCode) {
            return new JsonResponse(['error' => 'Please complete all the search fields below.'], 400);
        }

        if ($serviceDate) {
            $serviceDate = $this->validateServiceDate($serviceDate);
            if (!$serviceDate) {
                return new JsonResponse(['error' => 'Invalid service date!'], 400);
            }
        }

        $serviceCategory = $this->validateMainServiceCategory($serviceCategoryId);
        if ($serviceCategoryId && !$serviceCategory) {
            if (!$serviceCategory) {
                return new JsonResponse(['error' => 'Please select a service below.'], 400);
            }
        }


        if ($postCode && !$findPostcode = $this->validatePostcode($postCode)) {
            return new JsonResponse(['error' => 'Invalid postcode provided!'], 400);
        }


        if ($serviceTime && !in_array($serviceTime, range(8, 17))) {
            return new JsonResponse(['error' => 'Invalid service time provided!'], 400);
        }


        $additionalServices = array_keys($services);
        $companies = (new CompanyFilter())->mainCompareFilter($serviceCategory, $serviceDate, $findPostcode, $additionalServices);

        //$childCategories = $serviceCategory->childs()->get()->pluck('id')->all();
        //$services = Service::whereRaw('service_category_id IN ('.implode(',', $childCategories).')')->get();

        //$defaultPrices = (new PriceCalculator())->getDefaultPrices($companies->toArray(), $serviceCategory);
        //$price = (new PriceCalculator())->getPrice($company, [11 => 1, 27 => 2]);
        //return response()->json($services);

        $companies = (new BuildCompanyResponseModel())->build($companies, $services, $property_type);
        $companies = (new CompanyFilter())->orderCompanies($companies, $order);

        $serializer = SerializerBuilder::create()->build();

        return response()->json([
            'serviceCategoryId' => $serviceCategoryId,
            'serviceDate' => $serviceDate,
            'serviceTime' => $serviceTime,
            'serviceText' => $this->getServiceText($services),
            'postCode' => $postCode,
            'companies' => $serializer->serialize($companies, 'json'),
        ]);
    }

    private function getBedroomBathroomServiceId($serviceCategoryId)
    {

    }

    private function getServiceText($services)
    {
        $textArray = [];
        foreach ($services as $serviceId => $quantity) {
            $service = Service::find($serviceId);
            if (in_array($serviceId, [97, 107, 108])) {
                $textArray[] = '<span>' . $service->name . '</span>';
            } else {
                $textArray[] = '<span>' . $quantity . ' ' . $service->name . '</span>';
            }
        }

        return implode(',', $textArray);
    }

    private function validatePostcode($postCode)
    {
        if (!in_array(mb_strlen($postCode), [5, 6, 7, 8])) {
            Session::flash('status', 'Invalid postcode length!');

            return false;
        }
        $findPostcode = Postcode::whereRaw("INSTR('{$postCode}',`name`) = 1 ")->with('cities')->get();
        if(!$findPostcode->isEmpty())
            $city = $findPostcode[0]->cities;

        if (count($findPostcode) === 0 || $city->isEmpty()) {
            Session::flash('status', 'Sorry, your postcode area is not covered at present.');

            return false;
        }

        return $findPostcode[0];
    }

    public function ajaxReviews($company_id)
    {
        $ratings = Rating::where('company_id', $company_id)->orderBy('created_at', 'desc')->paginate(5);

        return View::make('reservation.ajax_reviews')->with('reviews', $ratings)->render();
    }

    public function companyReviews($company_id)
    {
        $company = Company::where("id", $company_id)->first();
        $response["name"] = $company->name;
        $response["logo"] = $company->logo;
        $response["city"] = $company->cities[0]->name;
        $reviewsCount = DB::table('ratings')->where('company_id', '=', $company_id)->count();
        $avg = DB::table('ratings')->where('company_id', '=', $company_id)->avg("rating");
        $ratings = Rating::where('company_id', $company->id)->orderBy('created_at', 'desc')->paginate(5);
        $response["count"] = $reviewsCount;
        $response["avg"] = round($avg, 1);
        $response["paginate"] = $ratings;

        return json_encode($response);
    }

    public function companyDetails(Request $request, Company $company, TempData $order)
    {
        $request['serviceCategoryId'] != 'carpet' ? $property_type = $request['propertyType'] : $property_type = null;

        $companyData = CompanyData::where('company_id', '=', $company->id)->get()->first();
        if (!empty($companyData->youtube)) {
            $companyData->youtube = str_replace(['.com/watch?v='], ['.com/embed/'], $companyData->youtube );
            strpos($companyData->youtube, '&') != false ? $companyData->youtube =  substr_replace($companyData->youtube, '', strpos($companyData->youtube, '&') ) : '';
        }
        $page_title = 'About '. $companyData->name .' | Service Octopus';

        $reviews = Rating::where('company_id', $company->id)->orderBy('created_at', 'desc')->paginate(5);

        $reviewsCount = DB::table('ratings')->where('company_id', '=', $company->id)->count();

        $mainServiceCategories = ServiceCategories::getRootCategoriesSelect();
        $allServiceCategories = DB::table('companies_service_categories')->select('service_categories_id')->where('company_id', '=', $company->id)->get()->toArray();
        $categoryIds = array_map(function ($serviceCategory) {
            return $serviceCategory->service_categories_id;
        }, $allServiceCategories);

        $mainServiceCategories = array_map(function ($serviceCategory) use ($categoryIds) {
            if (in_array($serviceCategory['id'], $categoryIds)) {
                return $serviceCategory;
            }
        }, $mainServiceCategories);

        $mainServices = DB::select(DB::raw("select distinct service_categories_id from companies_service_categories where service_categories_id in (1,4,17) and company_id= $company->id"));
        $mainServices = array_map(function ($service) {
            return $service->service_categories_id;
        }, $mainServices);

        $averageRating = DB::select(DB::raw("select AVG(rating) as rating from ratings where company_id= $company->id"));
        $averageRating = $averageRating[0]->rating;
        $averageRating = floor($averageRating * 2) / 2;

//dd( $reviews );
       // $orderData = json_decode($order->data, true);
        $selectedServices = $request->get('services');
        $services = array_map(function ($serviceId, $quantity) {
            return Service::find($serviceId);
        }, array_keys($selectedServices), array_values($selectedServices));
        $price = (new PriceCalculator())->getPrice($company, $selectedServices, $property_type);

        return view('reservation.company_details', [
            'companyData' => $companyData,
            'company' => $company,
            'reviews' => $reviews,
            'services' => $services,
            'selectedServices' => $selectedServices,
            'order' => $order,
            'reviewsCount' => $reviewsCount,
            'serviceCategories' => $mainServiceCategories,
            'mainServices' => $mainServices,
            'averageRating' => round($averageRating, 1),
            'price' => $price,
            'duePrice' => $price->getCleaningPrice() - $price->getBookingFee(),
            'page_title' => $page_title
        ]);
    }



    public function checkout(Request $request)
    {
        if(@Session::get('succesfully_bought') == 'success')
            return redirect('/');

        $request['serviceCategoryId']  != 'carpet' ? $property_type = $request['propertyType'] : $property_type = null;
        // TODO: fix time mess on view
//        var_dump($_SERVER['HTTP_REFERER']);

        $company = Company::find($request->get('companyId'));
        if (!$company) {
            Session::flash('status', 'Invalid company provided!');

            return redirect()->route('reservation');
        }

        $page_title = 'Book with '. $company->name .' | Service Octopus';

        $selectedServices = $request->get('services');
        $services = array_map(function ($serviceId, $quantity) {
            return Service::find($serviceId);
        }, array_keys($selectedServices), array_values($selectedServices));


        if (!(new CompanyServiceChecker())->canProvideServices($company, $services)) {
            Session::flash('status', 'This company does not provide selected services!');

            return redirect()->route('reservation');
        }

        $price = (new PriceCalculator())->getPrice($company, $selectedServices, $property_type);

        // TODO: Split in two separate methods
        if (!$request->isMethod(Request::METHOD_POST)) {
            $stripe_public_key = env('STRIPE_PUBLIC');

            return view('reservation.checkout', [
                'company' => $company,
                'services' => $services,
                'selectedServices' => $selectedServices,
                'price' => $price,
                'duePrice' => $price->getCleaningPrice() - $price->getBookingFee(),
                'fields' => $request->request->all(),
                'stripePublicKey' => env('STRIPE_KEY'),
                'page_title' => $page_title,
                'stripe_pk_key' => $stripe_public_key
            ]);
        }


        // When user data is actually submitted

        $request['card_number'] = str_replace(' ', '', $request->get('card_number'));

        $validator = Validator::make($request->all(), [
            'house_number' => 'bail|required',
            'address_line_1' => 'bail|required',
            'city_town' => 'bail|required',
            'email' => 'bail|required|email',
            'email_confirm' => 'bail|required|same:email',
            'mobile' => 'bail|required',
            'first_name' => 'bail|required',
            'last_name' => 'bail|required',
//            'card_number' => 'bail|required|numeric|min:13',
//            'expiration_month' => 'bail|required|numeric|max:12',
//            'expiration_year' => 'bail|required|numeric',
//            'cvc' => 'bail|required',
            'terms' => 'bail|required',
            // And actual order data
            'date' => 'required',
            'time' => 'required',
            'services' => 'required',
            'postCode' => 'required',
            'serviceCategoryId' => 'required'

        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $serviceCategory = $this->validateMainServiceCategory($request->get('serviceCategoryId'));
        $city = Postcode::whereRaw("INSTR('{$request->get('postCode')}',`name`) = 1 ")->first()->cities[0]->name;

        $order = TempDataFactory::create(
            $serviceCategory,
            Carbon::parse($request->get('date')),
            $request->get('time'),
            $request->get('postCode'),
            $request->get('propertyType'),
            $city
        );

        $couponHandler = new CouponHandler();
        $coupon = $request->request->get('coupon');
        $couponValid = false;

        if ($coupon && $couponHandler->isValid($coupon)) {

            $price = $couponHandler->applyCoupon($coupon, $price);

            if (!$price) {
                Session::flash('status', 'Invalid coupon provided!');

                return redirect()->route('reservation');
            }

            else {

                $couponValid = true;
            }
        }

        $servicesTmpData = array_map(function ($serviceId, $quantity) {
            return ['service_id' => $serviceId, 'count' => $quantity];
        }, array_keys($selectedServices), array_values($selectedServices));

        $token = $request->stripeToken;

        Stripe::setApiKey(env('STRIPE_SECRET'));
        try {
//            $token = $this->generateToken($request);

            $charge = \Stripe\Charge::create([
                "amount" => $price->getBookingFee() * 100, // CHECK, Amount in cents
                "currency" => "gbp",
                "source" => $token,
                "description" => "Booking fee for request " . $order->uuid. "-" .$request->request->get('first_name')."-".$request->request->get('last_name'),
            ]);
        } catch (\Stripe\Error\Card $e) {
            return back()->withInput()->withErrors([
                'card' => 'Card Error: ' .$e->getMessage()
            ]);
        }

//        dd( $price->getCleaningPrice() );
        $orderHelper = new OrderHelper();
        $companyOrderData = $orderHelper->getCompanyData($company, $servicesTmpData);
        $clientData = $orderHelper->getClientData($request);
        try{
            $order->updateData([
                'company' => $companyOrderData,
                'client' => $clientData,
                'site_charge' => $charge->amount,
                'order_total' => $price->getCleaningPrice(),
                'firstname' => $request->request->get('first_name'),
                'lastname' => $request->request->get('last_name')
            ]);
        } catch(Exception $ex){
            mail("duka@outlook.com","Error",$ex);
            if(env('APP_ENV', 'local') == 'live')
            {
                mail("alex@serviceoctopus.com","Error",$ex);
                mail("roberto@serviceoctopus.com","Error",$ex);
                mail("balsabozovic84@gmail.com","Error",$ex);
            }
            $ex->getMessage();
        }

        if ( $couponValid === true ) {

            $order->updateData([
            'coupon' => $coupon,
            ]);

        }

        /**
         * $order->update(['company_id', $company_id]) не действа !§€@§€
         */
        $order->company_id = $company->id;
        $order->save();

        $orderData = $order->getData();
        \Mail::to($orderData->client->email)->send(new ClientBookingEmail($order, $orderData, $price->getCleaningPrice(), $price->getBookingFee(), $company));
        \Mail::to($company->email)->send(new CompanyBookingEmail($order, $orderData, $price->getCleaningPrice(), $price->getBookingFee()));

        $limit = $company->getSubscriptionLimit();
        $todayOrders = $company->getThisMonthBookings()->count();

        if ($limit - $todayOrders <= 0) {
            \Mail::to($company->email)->send(new LimitExpireEmail($company));
        }

        if ($limit - $todayOrders == (int)$limit / 2) {
            \Mail::to($company->email)->send(new CountLimit((int)$limit - $todayOrders));
        }

        Session::put('succesfully_bought', 'success');
        // Redirect to checkout success
        return redirect()->route('confirm', ['order' => $order->id]);


    }

    private function generateToken(Request $request)
    {
        $token = Token::create([
            "card" => [
                //"name" => $request->request->get(''),
                "number" => $request->request->get('card_number'),
                "exp_month" => $request->request->get('expiration_month'),
                "exp_year" => $request->request->get('expiration_year'),
                "cvc" => $request->request->get('cvc')
            ]
        ]);
        dd( $token );
        return $token->id;
    }

    /*public function processCheckout(Request $request)
    {
        $order = $request->request->get('order');
        $order = TempData::find($order);
        if (!$order) {
            Session::flash('status', 'Invalid request!');

            return redirect('/reservation');
        }

        $company = $request->request->get('company');
        $company = Company::find($company);
        if (!$company) {
            Session::flash('status', 'Invalid request!');

            return redirect('/reservation');
        }

        $validator = Validator::make($request->all(), [
            'house_number' => 'bail|required',
            'address_line_1' => 'bail|required',
        ]);

        if ($validator->fails()) {
            //return redirect()->route('checkout', ['order' => $order->id, 'company' => $company])->withErrors($validator)->withInput();
            return redirect()->back()->withErrors($validator)->withInput();
        }

        return view('reservation.checkout_confirmation');
    }*/

    public function checkoutConfirm(TempData $order)
    {
        $data = $order->getData();
        $company = $order->company()->first();
        if (!$company) {
            Session::flash('status', 'Order is not completed!');

            return redirect()->route('reservation');
        }

        if (!$email = $data->client->email) {
            Session::flash('status', 'Order is not completed!');

            return redirect()->route('reservation');
        }

        return view('reservation.checkout_confirmation', [
            'email' => $email,
            'company' => $company
        ]);
    }

    public function getAdditionalServices(Request $request)
    {
        $mainCategory = $request->request->get('data');

        if (!isset(self::$mainCategories[$mainCategory])) {
            return new JsonResponse('Invalid category provided.');
        }

        $mainCategory = ServiceCategories::find(self::$mainCategories[$mainCategory]);

        $childCategories = $mainCategory->childs()->get()->pluck('id')->all();

        // This will need serious refactor
        if ($mainCategory->id == 4) {
            $carpetServices = [3, 1, 4, 2, 5, 6, 67, 68, 69, 70, 71, 15, 9, 10, 11, 45, 46, 47, 48, 49, 50, 51, 72, 73, 74, 75, 76, 77, 78, 79];
            //$services = Service::where('service_category_id', '=', 5)->get()->toArray();
            $services = Service::whereRaw('id IN (' . implode(',', $carpetServices) . ')')->get()->toArray();
        } else {
            $services = Service::whereRaw('service_category_id IN (' . implode(',', $childCategories) . ')')->get()->toArray();
        }
        $excludeServices = [13, 14, 27, 92, 99, 100, 98, 107, 108];
        $result = [];

        foreach ($services as $service) {
            if (!in_array($service['id'], $excludeServices)) {
                $result[$service['id']] = ['id' => $service['id'], 'name' => $service['name'], 'icon' => $service['icon'], 'quantity' => 0];
            }
        }

        return new JsonResponse($result);
    }

    private function validateServiceDate($serviceDate)
    {
        try {
            $serviceDate = Carbon::parse($serviceDate);
        } catch (\Exception $e) {
            Session::flash('status', 'Invalid date provided!');

            return false;
        }

        $serviceDateValidator = new ServiceDateValidator();

        try {
            $serviceDateValidator->validate($serviceDate);
        } catch (\Exception $e) {
            Session::flash('status', $e->getMessage());

            return false;
        }

        return $serviceDate;
    }

    private function validateMainServiceCategory($serviceCategoryId)
    {
        if (!isset(self::$mainCategories[$serviceCategoryId])) {
            return false;
        }

        return ServiceCategories::find(self::$mainCategories[$serviceCategoryId]);
    }
}
