<?php

namespace App\Service;

use App\Company;
use App\Repository\CompanyRepository;
use App\Repository\ServiceRepository;
use Symfony\Component\HttpFoundation\Request;
use DateTime;
use Carbon\Carbon;
use App\ServiceCategories;
use App\Postcode;
use App\Holidays;
use Illuminate\Support\Facades\DB;
use App\Service\CompanyServiceChecker;

class CompanyFilter
{
    private $companyRepository;
    private $serviceRepository;
    private $companyServiceChecker;

    public function __construct()
    {
        $this->companyRepository = new CompanyRepository();
        $this->serviceRepository = new ServiceRepository();
        $this->companyServiceChecker = new CompanyServiceChecker();
    }

    public function mainCompareFilter(
        ServiceCategories $serviceCategory,
        DateTime $serviceDate,
        Postcode $postCode,
        $additionalServices = []
    ) {
        //$services = $serviceCategory ? $this->getServicesId($serviceCategory) : [];
        /*if (!empty($additionalServices)) {
            $services = array_unique(array_merge($services, $additionalServices));
        }*/
        // dd($additionalServices);

        $companyIds = $this->companyRepository->findApprovedCompaniesByServicesAndPostcode($additionalServices, $postCode);
        if (empty($companyIds)) {
            return [];
        }

        if ($serviceDate) {
            $dayOfTheWeek = Carbon::parse($serviceDate->format('Y-m-d'))->dayOfWeek;
            $companyWorkdays = DB::table('company_workdays')
                ->whereRaw('company_id IN ('.implode(',', $companyIds).')')->get()->toArray();

            $companyIds = array_map(function ($companyWorkday) use ($dayOfTheWeek, $companyIds) {
                $workdays = json_decode($companyWorkday->workdays);
                if (in_array($dayOfTheWeek, $workdays)) {
                    return $companyWorkday->company_id;
                }
            }, $companyWorkdays);

            $companyIds = array_values(array_filter($companyIds, function($companyId) {
                return !is_null($companyId);
            }));

            if ($holiday = Holidays::where('date', '=', $serviceDate->format('Y-m-d'))->first()) {
                $companiesToExclude = DB::table('company_restdays')->where('holidays_id', '=', $holiday->id)->get()->toArray();
                $companiesToExcludeIds = array_map(function ($companyToExclude) {
                    return $companyToExclude->company_id;
                }, $companiesToExclude);

                $companyIds = array_diff($companyIds, $companiesToExcludeIds);
            }
        }

        $companies = Company::whereIn('id', $companyIds)
            ->whereApproved(true)
            ->whereType(0)
            ->get();

        $companyServiceChecker = new CompanyServiceChecker();

        // @todo Check counting order, currently using TempData
        $companies = $companies->reject(function ($company) use ($companyServiceChecker) {
            return $companyServiceChecker->checkOrderSubscriptionLimit($company) === false;
        })->map(function ($company) {
            return $company;
        });

        // Also number of orders are from TempData
        $companies = $companies->reject(function ($company) use ($companyServiceChecker, $serviceDate) {
            return $companyServiceChecker->checkOrdersPerDayLimit($company, $serviceDate) === false;
        })->map(function ($company) {
            return $company;
        });

        return $companies;
    }

    public function filter(Request $request)
    {
        $postcode = $request->query->get('postcode', '');
        $services = $request->query->get('services');

        $companies = $this->companyRepository->findApprovedCompaniesByServicesAndPostcode([13,17], $postcode);

        $results = [];
        $serviceDate = new DateTime();
        $needed = [];

        foreach($companies as $company) {
            if ($company->services->isEmpty()) {
                continue;
            }

            if($company->postcodes->isEmpty()) {
                return;
            }

            if (!$this->companyServiceChecker->canProvideServices($company, $services)) {
                return;
            }

            if (!$this->companyServiceChecker->checkWorkDays($company, $serviceDate)) {
                return;
            }

            if (!$this->companyServiceChecker->checkRestDays($company, $serviceDate)) {
                return;
            }

            if (!$this->companyServiceChecker->checkOrderSubscriptionLimit($company)) {
                return;
            }

            if (!$this->companyServiceChecker->checkOrdersPerDayLimit($company, $serviceDate)) {
                return;
            }

            $results[] = $company;
        }

        return $results;
    }

    public function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }

    public function orderCompanies($companies, $order = 'price')
    {
        if ($order == 'price') {

            usort($companies,function($a,$b){
//                $c = $b->getSpecial_offer() - $a->getSpecial_offer();
                $c = $b->getTotalPrice() < $a->getTotalPrice();
                return $c;
            });

        } elseif ($order == 'rating') {

            usort($companies,function($a,$b){
//                $c = $b->getSpecial_offer() - $a->getSpecial_offer();
                $c = $a->getReviewRate() < $b->getReviewRate();
                return $c;
            });

        } elseif ($order == 'reviews') {

            usort($companies,function($a,$b){
//                $c = $b->getSpecial_offer() - $a->getSpecial_offer();
                $c = $a->getNumberOfReviews() < $b->getNumberOfReviews();
                return $c;
            });
        }

        return $companies;
    }

    private function getChildCategories(ServiceCategories $serviceCategory)
    {
        $childs = $serviceCategory->childs()->get()->toArray();

        return array_map(function ($serviceCategory) {
            return $serviceCategory['id'];
        }, $childs);
    }

    private function getServicesId(ServiceCategories $serviceCategory)
    {
        $childCategories = $this->getChildCategories($serviceCategory);
        $services = $this->serviceRepository->findByCategories($childCategories);

        return array_map(function ($service) {
            return $service['id'];
        }, $services);
    }

    private function convertToCompanyId($companies)
    {
        return array_map(function ($companyId) {
            return $companyId->company_id;
        }, $companies);
    }
}
