<?php

namespace App\Service;

use DateTime;
use Carbon\Carbon;
use Exception;

class ServiceDateValidator
{
    const HOUR_LIMIT = 17;

    public function validate(DateTime $serviceDate)
    {
        $now = new DateTime('now');

        if (Carbon::parse($serviceDate->format('Y-m-d'))->isToday()) {
            throw new Exception('You can not pick today date for service.');
        }

        if ($serviceDate < $now) {
            throw new Exception('You can not pick date in the past.');
        }

        $tomorrow = new DateTime('tomorrow');

        if ($serviceDate == $tomorrow && (int) Carbon::parse($now->format('Y-m-d H:i:s'))->hour > 17) {
            throw new Exception('You can not pick tomorrow date since it is past 17am.');
        }
    }
}