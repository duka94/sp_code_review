<?php

namespace App\Service;

use App\Company;
use App\Model\Price;
use App\ServiceCategories;
use App\MasterSettigns;
use Illuminate\Support\Facades\DB;

class PriceCalculator
{
    const CARPET_CLEANING_MAIN_CATEGORY = 4;

    public static $bedroomServices = [
        // End of tenancy cleaning
        1 => [
            'house' => 27,
            'flat' => 13
        ],
        // Deep cleaning
        17 => [
            'house' => 100,
            'flat' => 99
        ],
    ];

    // Not depending on Flat/House, based from Optional Extras
    public static $bathroomServices = [
        // End of tenancy cleaning
        1 => 14,
        // Deep cleaning
        17 => 92,
    ];

    public function getPrice(Company $company, array $services = [], $property_type)
    {
        $totalPrice = 0;
        $break_const = false;
        foreach ($services as $serviceId => $quantity) {
            if (!$break_const) {
//            if $property_type == house => $serviceId is 27 -> look into db 'companies_services' table
                if ($serviceId == 13 && $property_type == 'house') {
                    $serviceId = 27;
                }

                $price = DB::table('companies_services')->select('price')->where('company_id', '=', $company->id)
                    ->where('service_id', '=', $serviceId)->get()->first();

                if ($quantity > 1) {
                    $prices = DB::table('companies_services')->select('prices')->where('company_id', '=', $company->id)
                        ->where('service_id', '=', $serviceId)->get()->first();

                    if ($prices == null && $serviceId == 27) {
                        $break_const = true;
                        $prices = "";

                    } else {
                        $prices = json_decode($prices->prices, true);

                    }

                    if ($prices != 0 && isset($prices[$quantity])) {
                        $totalPrice += $prices[$quantity];
                    } else {
 
                        if (isset($price->price)) {
                            if ($serviceId !== 14 && isset($price->price)) {

                                $totalPrice += $price->price * $quantity;

                            } else {

                                $totalPrice += $price->price * ($quantity - 1);
                            }
                        }
                    }
                } else {

                    if ($serviceId !== 14 and isset($price->price)) {
                        $totalPrice += $price->price;
                    }
                }
            }
        }

        return new Price($totalPrice, $this->calculateFee($totalPrice));
    }

    public function getDefaultPrices(array $companies, ServiceCategories $serviceCategory)
    {
        $prices = [];
        $isCarpetCategory = $serviceCategory->id === self::CARPET_CLEANING_MAIN_CATEGORY;

        foreach ($companies as $company) {
            if (!$isCarpetCategory) {
                $bathroomPrice = DB::table('companies_services')->select('price')->where('company_id', '=', $company['id'])
                    ->where('service_id', '=', self::$bathroomServices[$serviceCategory->id])->get()->first();

                $bedroomServiceId = self::$bedroomServices[$serviceCategory->id]['flat'];

                $bedroomPrice = DB::table('companies_services')->select('price')->where('company_id', '=', $company['id'])
                    ->where('service_id', '=', $bedroomServiceId)->get()->first();

                if ($bathroomPrice && $bedroomPrice) {
                    $price = (int)$bathroomPrice->price + (int)$bedroomPrice->price;
                    $prices[$company['id']]['price'] = $price;
                    $prices[$company['id']]['fee'] = $this->calculateFee($price);
                } else {
                    print 'Debug error: cid:'.$company['id'].' '.gettype($bathroomPrice).' '.gettype($bedroomPrice);exit;
                }
            }
        }

        return $prices;
    }

    private function calculateFee($price)
    {
        // Currently not used
        $vat = MasterSettigns::where('setting_name', '=', 'vat')->select('value')->get()->first()->value;
        $booking = MasterSettigns::where('setting_name', '=', 'booking')->select('value')->get()->first()->value;

        return round($price * ((int)$booking / 100));
    }

    private function getFlatCategory(ServiceCategories $serviceCategory)
    {
        return ServiceCategories::where('parent_id', '=', $serviceCategory->id)
            ->where('name', '=', 'Flat')->first();
    }
}