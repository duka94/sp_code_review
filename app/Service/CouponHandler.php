<?php


namespace App\Service;

use App\Model\Price;

class CouponHandler
{
    public static $coupons = [
        [
            'name' => 'NEWBOOKING5',
            'value' => 5,
        ],
        [
            'name' => 'NEWBOOKING10',
            'value' => 10,
        ]
    ];

    public function isValid($providedCoupon)
    {
        $couponNames = array_map(function ($coupon) {
            return $coupon['name'];
        }, self::$coupons);

        return in_array($providedCoupon, $couponNames);
    }

    public function applyCoupon($coupon, Price $price)
    {
        if (!$this->isValid($coupon)) {
            return false;
        }

        return $price->subtractAmount($this->getCouponByName($coupon));
    }

    public function getCouponByName($coupon)
    {
        $result = (int) array_search($coupon, array_column(self::$coupons, 'name'));

        if ($result !== false) {
            return self::$coupons[$result]['value'];
        }

        return false;
    }
}
