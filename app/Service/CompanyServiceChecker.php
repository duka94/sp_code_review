<?php

namespace App\Service;

use App\Company;
use DateTime;
use Carbon\Carbon;

class CompanyServiceChecker
{
    public function canProvideServices(Company $company, array $services)
    {
        $companyServices = $company->services()->get()->toArray();
        $companyServicesIds = array_map(function ($companyService) {
            return $companyService['id'];
        }, $companyServices);

        $services = array_map(function ($service) {
            return $service->id;
        }, $services);

        foreach ($services as $service) {
            if (!in_array($service, $companyServicesIds)) {
                return false;
            }
        }

        return true;
    }

    public function checkWorkDays(Company $company, DateTime $serviceDate)
    {
        if (!isset($company->workdays->workdays)) {
            return false;
        }

        if (!in_array(Carbon::parse($serviceDate->format('Y-m-d'))->dayOfWeek, json_decode($company->workdays->workdays))) {
            return false;
        }

        return true;
    }

    public function checkRestDays(Company $company, DateTime $serviceDate)
    {
        $restdays = $company->restdays()->get()->pluck('date')->toArray();

        if (in_array(Carbon::parse($serviceDate->format('Y-m-d')), $restdays)) {
            return false;
        }

        return true;
    }

    public function checkOrderSubscriptionLimit(Company $company)
    {
        $startOfMonth = Carbon::now()->startOfMonth();
        $endOfMonth = Carbon::now()->endOfMonth();

        $orders = collect($company->orders)->filter(function ($order) use ($startOfMonth, $endOfMonth) {
            return Carbon::parse(json_decode($order->data)->service_date)->between($startOfMonth, $endOfMonth);
        })->count();

        if ((int)$orders >= (int)$company->getSubscriptionLimit()) {
            return false;
        }

        return true;
    }

    public function checkOrdersPerDayLimit(Company $company, DateTime $serviceDate)
    {
        $dayOrders = $company->orderOnDate($serviceDate->format('Y-m-d'));

        if ($dayOrders >= (int)$company->max_jobs) {
            return false;
        }

        return true;
    }
}