<?php

namespace App\Service;

use App\Model\CompanyModel;
use App\Model\Price;
use App\Service\PriceCalculator;

class BuildCompanyResponseModel
{
    public function build($companies, array $services, $property_type )
    {
        $response = [];
        $priceCalculator = new PriceCalculator();

        foreach ($companies as $company) {
            $price = $priceCalculator->getPrice($company, $services, $property_type);
            //temporary fix for flat/house
            if( $price->getCleaningPrice() != 0 && $price->getBookingFee() != 0)
            {
                $model = new CompanyModel($company, $price);
                $response[] = $model;
            }

        }

        return $response;
    }
}