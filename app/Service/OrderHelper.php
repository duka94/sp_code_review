<?php

namespace App\Service;

use App\Company;
use App\CompanyServices;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Request;

class OrderHelper
{
    public function getCompanyData(Company $company, array $services)
    {
        $cleaning_quarantee = [
            '1' => '24 hours',
            '2' => '48 hours',
            '3' => '1 week',
            '4' => '2 weeks',
            '5' => '3 weeks',
            '6' => '4 weeks',
            '7' => 'No guarantee'
        ];

        $tmp = [
            'id' => (string)$company->id,
            'name' => $company->name,
            'logo' => $company->logo,
            'year' => $company->data->date_established,
            'region' => $company->regions,
            'liability' => ( $company->data->liability == 1 && Carbon::createFromFormat('Y-m-d', $company->data->liability_expires)->gt(Carbon::now()) ),
            'cover' => '1 million',
            'rating' => (int)collect($company->ratings)->avg('rating'),
            'total_reviews' => collect($company->ratings)->count(),
            'ratings' => $company->ratings,
            'quarentee' => $company->data->complaints == 7 ? false : $cleaning_quarantee[$company->data->complaints]
        ];

        $must_filter = false;

        $serviceIds = array_map(function ($service) {
            return $service['service_id'];
        }, $services);
        $totalPrice = 0;

        $company->services->each(function ($service, $k) use (&$tmp, $company, $services, &$must_filter, $serviceIds, &$totalPrice) {
            if (!in_array($service->id, $serviceIds)) {
                return;
            }

            $count = collect($services)->where('service_id', $service->id)->first()['count'];
            $price = $service->pivot->price * $count;

            if ($service->prices_number > 1 && $count > 1) {
                $a_prices = json_decode($service->pivot->prices, true);

                if ( isset($a_prices[$count]) ) {
                    $price = $a_prices[$count];
                } else {
                    $must_filter = true;
                    return;
                }
            }

            if($price === 0) {
                $must_filter = true;
                return;
            }

            $srv_tmp = [
                'id' => (string)$service->id,
                'price' => $price,
                'name' =>  $service->name,
                'count' => $count,
                'sum_price' => $price * $count
            ];
            $totalPrice += $srv_tmp['sum_price'];

            $subs = Company::find($company->id)->services()->whereIn('services.id', $service->subs->pluck('id'))->get();
            $additional = [];

            $subs->each(function ($sub) use (&$additional, $services, &$totalPrice) {
                if((double) $sub->pivot->price > 0){
                    $additional[] = [
                        'id' => (string)$sub->id,
                        'name' => $sub->name,
                        'price' => $sub->pivot->price,
                    ];
                    $totalPrice += $sub->pivot->price;
                }
            });

            $srv_tmp['additional'] = $additional;
            $tmp['services'][$service->id] = $srv_tmp;
        });

        $tmp['price'] = $totalPrice;

        return $tmp;
    }

    public function getClientData(Request $request)
    {
        return [
            'firstname' => $request->request->get('first_name'),
            'lastname' => $request->request->get('last_name'),
            'email' => $request->request->get('email'),
            'flat' => $request->request->get('house_number'),
            'address' => $request->request->get('address_line_1'),
            'address_2' => $request->request->get('address_line_2'),
            'town' => $request->request->get('city_town'),
            'city' => $request->request->get('state_country'),
            'clientphone' => $request->request->get('mobile'),
            'instructions'=>  $request->request->get('instructions')
        ];
    }
}