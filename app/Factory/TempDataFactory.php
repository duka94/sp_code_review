<?php

namespace App\Factory;

use App\ServiceCategories;
use App\TempData;
use DateTime;

class TempDataFactory
{
    public static function create(
        ServiceCategories $serviceCategory,
        DateTime $serviceDate,
        $serviceTime,
        $postcode,
        $propertyType,
        $city
    ) {
        $data = [];
        $data['cleaning_type'] = $serviceCategory->name;
        $data['services'] = $serviceCategory->id;
        $jsonData = json_encode($data, JSON_UNESCAPED_SLASHES);
        $uuid = TempData::add($jsonData);
        $order = TempData::where('uuid', '=', $uuid)->get()->first();

        $data['uuid'] = $order->uuid;
        $data['service_date'] = $serviceDate->format('Y-m-d H:i:s');

        $serviceDateEnd = clone $serviceDate;
        $startTime = $serviceDate->setTime($serviceTime, 0);
        $endTime = $serviceDateEnd->setTime($serviceTime + 1, 0);
        $data['timeslots'] = $startTime->format('Y-m-d H:i:s').'|'.$endTime->format('Y-m-d H:i:s');

        $data['postcode'] = $postcode;
        $data['propertyType'] = ucfirst($propertyType);
        $data['city'] = $city;

        $order->data = json_encode($data, JSON_UNESCAPED_SLASHES);
        $order->save();

        return $order;
    }
}