<?php

namespace App\Factory;

use App\TempData;
use App\Model\Order;
use App\Company;
use App\ServiceCategories;
use DateTime;

class OrderFactory
{
    public static function createFromTempData(TempData $tempData)
    {
        $data = json_decode($tempData->data, true);
        $companyData = $data['company'];
        $postcode = $data['postcode'];
        $company = Company::where('id', '=', $companyData['id'])->get()->first();
        $mainServiceCategory = ServiceCategories::where('id', '=', $data['services'])->get()->first();
        $serviceDate = new DateTime($data['service_date']);
        $serviceTime = $data['timeslots'];
        $services = $data['company']['services'];

        $order = new Order($company, $mainServiceCategory, $serviceDate, $services, $serviceTime, $postcode);

        return $order;
    }
}